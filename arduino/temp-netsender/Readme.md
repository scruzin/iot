# Readme

temp-netsender is a NetSender client to send temperature sensor data to NetReceiver,
using the DHT, OneWire and DallasTemperature libraries.

The "external" pins are mapped as follows:

* X50 = DHT temperature
* X51 = DHT humidity
* X60 = Dallas temperature (DS18B20, etc.)

Temperatures are reported in degrees Kelvin times 10.
Humidity is reported as a percentage times 10, i.e., 482 for 48.2%
A value of -1 is returned upon failure.

# See Also

* [NetReceiver Help](http://netreceiver.appspot.com/help)

# License

temp-netsender is Copyright (C) 2017-2018 Alan Noble.

It is free software: you can redistribute it and/or modify them
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

It is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License
along with NetReceiver in gpl.txt.  If not, see [GNU licenses](http://www.gnu.org/licenses).

The DHT11 library is Copyright (C) AdaFruit Industries under an MIT License.

