/*
  Name:
    temp-netsender - a NetSender client to send temperature sensor data to NetReceiver.

 License:
    Copyright (C) 2017-2019 Alan Noble.

    This file is part of NetSender. NetSender is free software: you can
    redistribute it and/or modify it under the terms of the GNU
    General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.

    NetSender is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with NetSender in gpl.txt.  If not, see
    <http://www.gnu.org/licenses/>.
*/

#include "NetSender.h"
#include "DHT.h"
#include <OneWire.h>
#include <DallasTemperature.h>


#define DHTPIN 12
#define DTPIN  13

#define DHTTYPE DHT22 // external device #5 
DHT dht(DHTPIN, DHTTYPE);
OneWire onewire(DTPIN);
DallasTemperature dt(&onewire); // external device #6

int varsum = 0;

// tempReader is the pin reader that polls either the DHT or Dallas Temperature device
int tempReader(NetSender::Pin *pin) {
  pin->value = -1;
  if (pin->name[0] != 'X') {
    return -1;
  }
  int pn = atoi(pin->name + 1);
  float ff;
  switch(pn) {
  case 50: // DHT air temperature
    ff = dht.readTemperature();
    if (isnan(ff)) {
      return -1;
    } else {
      pin->value = 10 * (ff + 273.15);
      break;
    }
  case 51: // DHT humidity
    ff = dht.readHumidity();
    if (isnan(ff)) {
      return -1;
    } else {
      pin->value = 10 * ff;
      break;
    }
  case 60: // Dallas temperature
    dt.requestTemperatures();
    ff = dt.getTempCByIndex(0);
    if (isnan(ff)) {
      return -1;
    } else {
      pin->value = 10 * (ff + 273.15);
      break;
    }
  default:
    return -1; 
  }
  return pin->value;
}

// required Arduino routines
// NB: setup runs everytime ESP8266 comes out of a deep sleep
void setup() {
  dht.begin();
  dt.begin();
  NetSender::ExternalReader = &tempReader;
  NetSender::init();
  loop();
}

void loop() {
  while (!NetSender::run(&varsum)) {
    ;
  }
}
