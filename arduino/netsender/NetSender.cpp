/*
  Name:
    NetSender - an Arduino library for sending measured values to the cloud and writing values from the cloud.
      
  Description:
    see http://netreceiver.appspot.com/help.

  License:
    Copyright (C) 2017-2018 Alan Noble.

    This file is part of NetSender. NetSender is free software: you can
    redistribute it and/or modify it under the terms of the GNU
    General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.

    NetSender is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with NetSender in gpl.txt.  If not, see
    <http://www.gnu.org/licenses/>.
*/

#include <stdlib.h>
#include <limits.h>
#include <string.h>
#include <ctype.h>
#include <ESP8266WiFi.h>
#include <EEPROM.h>
#include "NetSender.h"

namespace NetSender {

// delays in milliseconds
#define ADC_DELAY  5   // time between consecutive ADC reads
#define SENSE_DELAY 10 // time between SensePin going high and reading
#define WIFI_DELAY 100 // time between consecutive WiFi attempts
#define HTTP_DELAY 500 // time between consecutive HTTP attempts

// constants
enum bootReason {
  bootNormal = 0x00,
  bootWiFi   = 0x01,
  bootAlarm  = 0x02,
  bootMask   = 0x03
};

// NetReceiver reconfig codes
enum rcCode {
  rcOK           = 0,
  rcConfigure    = 1,
  rcReboot       = 2,
  rcDebug        = 3,
  rcUpgrade      = 4
};

// persistent variables (stored in EEPROM as part of configuration)
enum pvIndex {
  pvPulses,
  pvPulsePeriod,
  pvPulseDutyCycle,
  pvPulseCycle,
  pvAlarmRestart,
  pvAlarmPin,
  pvAlarmLevel,
  pvAlarmPeriod,
  pvAlarmNetwork,
  pvAlarmVoltage,
  pvAlarmRecoveryVoltage,
  pvSensePin
};

// NB: keep in sync with above
const char* PvNames[] = {
  "Pulses", "PulsePeriod", "PulseDutyCycle", "PulseCycle", "AlarmRestart",
  "AlarmPin", "AlarmLevel", "AlarmPeriod", "AlarmNetwork", "AlarmVoltage",
  "AlarmRecoveryVoltage","SensePin"
};

// X pins (offset by 10)
enum xIndex {
  xA0       = 0, // X10
  xAlarmed  = 1, // X11
  xAlarms   = 2, // X12
  xBoot     = 3, // X13
  xSuppress = 4, // X14
  xMax
};

// exported globals
Configuration Config;
ReaderFunc ExternalReader = NULL;
ReaderFunc BinaryReader = NULL;
int VarSum = 0;
bool Debug = false;

// other globals
static int XPin[xMax] = {-1, 0, 0, 0, 0};
static bool Configured = false;
static char MacAddress[MAC_SIZE];
static unsigned long Time = 0;
static unsigned long AlarmedTime = 0;
static int ConnFailures = 0;

// forward declarations
void restart(bootReason reason);

// utilities
// copy a string, padding with null characters
void padCopy(char * dst, const char * src, size_t size) {
  int ii = 0;
  for (; ii < size - 1 && ii < strlen(src); ii++) {
    dst[ii] = src[ii];
  }
  for (; ii < size; ii++) {
    dst[ii]  = '\0';
  }
}

// format MAC address.
// Prior to version 140 we formatted it back to front.
char * fmtMacAddress(byte mac[6], char str[MAC_SIZE]) {
  const char* hexDigits = "0123456789ABCDEF";
  char * strp = str;
  for (int ii = 0; ii < 6; ii++) {
    *strp++ = hexDigits[(mac[ii] & 0x00F0) >> 4];
    *strp++ = hexDigits[mac[ii] & 0x000F];
    if (ii < 5) *strp++ = ':';
  }
  *strp = '\0';
  return str;
}

// get a JSON string or integer value
bool extractJson(String json, const char * name, String& value) {
  int finish, start = json.indexOf("\"" + String(name) + "\"");
  if (start == -1) return false;
  start += strlen(name) + 3; // skip quotes and colon
  while (json.charAt(start) == ' ') {
    start++;
  }
  switch (json.charAt(start)) {
  case '-': case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9':
    finish = json.indexOf(',', start);
    break;
  case '"':
    finish = json.indexOf('"', ++start);
    break;
  default:
    return false;
  }
  if (finish == -1) finish = json.length();
  value = json.substring(start, finish);
  return true;
}

// initializing and reading/writing pins

// setPins sets pin names in the Pin array from comma-separated names and clears unused pins.
int setPins(const char * names, Pin * pins) {
  char *start = (char *)names;
  int ii = 0;
  for (; ii < MAX_PINS; ii++) {
    char * finish = strchr(start, ',');
    if (finish == NULL) {
      strcpy(pins[ii].name, start);
      ++ii;
      break;
    }
    strncpy(pins[ii].name, start, finish-start);
    pins[ii].name[finish-start] = '\0';
    start = finish + 1;
  }
  int sz = ii;
  for (; ii < MAX_PINS; ii++) {
    pins[ii].name[0] = '\0';
  }
  return sz;
}

// initPins initializes digital pins.
void initPins() {
  Pin pins[MAX_PINS];

  int sz = setPins(Config.inputs, pins);
  for (int ii = 0; ii < sz; ii++) {
    if (pins[ii].name[0] == 'D') {
      int pn = atoi(pins[ii].name + 1);
      pinMode(pn, INPUT);
    }
  }
  sz = setPins(Config.outputs, pins);
  for (int ii = 0; ii < sz; ii++) {
    if (pins[ii].name[0] == 'D') {
      int pn = atoi(pins[ii].name + 1);
      pinMode(pn, OUTPUT);
    }
  }
}

// read a pin value and return it, or -1.
int readPin(Pin * pin) {
  int pn = atoi(pin->name + 1);
  int sum;
  switch (pin->name[0]) {
  case 'A':
    // return average of 10 rapid measurements
    for (int jj = 0, sum = 0; jj < 10; jj++, sum += analogRead(pn)) {
      delay(ADC_DELAY);
    }
    pin->value = sum/10;
    return pin->value;
  case 'B':
    if (BinaryReader != NULL) {
      return (*BinaryReader)(pin);
    }
    break;
  case 'D':
    pin->value = digitalRead(pn);
    return pin->value;
  case 'X':
    if (pn >= 10 && pn < 10+xMax) {
      pin->value = XPin[pn-10];
      return pin->value;
    }
    if (ExternalReader != NULL) {
      return (*ExternalReader)(pin);
    }
    break;
  }
  return -1;
}

void writePin(Pin * pin) {
  int pn = atoi(pin->name + 1);
  if (Debug) Serial.print(F("Write ")), Serial.print(pin->name), Serial.print(F("=")), Serial.println(pin->value);
  switch (pin->name[0]) {
  case 'A':
    analogWrite(pn, pin->value);
    break;
  case 'D':
    digitalWrite(pn, pin->value);
    break;
  case 'X':
    if (pn == 10+xSuppress && pin->value == 1) {
      XPin[xSuppress] = 1;
    }
    break;
  default:
    if (Debug) Serial.println(F("Invalid write"));
  }
}

// pulsePin generates pulses on the given pin, with each pulse having the given period and duty cycle,
// with the latter defaulting to 50.
// When the dutyCycle is greater than 100, we subtract 100 and pulse from HIGH to LOW instead of LOW to HIGH.
// When X14 is true, the equivalent delay is produced without actual pulses being generated.
void pulsePin(int pin, int pulses, int period, int dutyCycle=50) {
  int level = LOW;
  if (pulses <= 0) return;
  if (period <= 0 || pulses * period > Config.monPeriod * 1000) return;
  if (dutyCycle < 0 || dutyCycle > 200 ) return;
  if (Debug) {
    if (XPin[xSuppress]) {
      Serial.print(F("Pulse suppressed: ")), Serial.print(pulses * period), Serial.println(F("ms"));
    } else {
      Serial.print(F("Pulsing ")), Serial.print(pulses), Serial.print(F(",")), Serial.print(period), Serial.print(F(",")), Serial.println(dutyCycle);
    }
  }
  if (dutyCycle == 0) {
    dutyCycle = 50;
  }
  if (dutyCycle > 100) {
    dutyCycle = dutyCycle - 100;
    level = HIGH;
  }
  int active = period * dutyCycle / 100; // in milliseconds
  int timing[2] = {active, period - active};
  for (int ii = 0; ii < pulses * 2; ii++) {
    if (!XPin[xSuppress]) {
      digitalWrite(pin, ii % 2 ? level : !level);
    }
    delay(timing[ii % 2]);
  }
}

void cyclePin(int pin, int cycles, bool force = false) {
  // cycle a digital pin on and off, unless in pulse mode
  if (!force && Config.vars[pvPulses] != 0) return;
  pulsePin(pin, cycles, 500, 150);
}

// EEPROM utilities
void readConfig(Configuration* config) {
  unsigned char *bytep = (unsigned char *)config;
  EEPROM.begin(sizeof(Configuration));
  for (int ii = 0; ii < sizeof(Configuration); ii++) {
    unsigned char ch = EEPROM.read(ii);
    if (ch == 255) {
      *bytep++ = '\0';
    } else {
      *bytep++ = ch;
    }
  }
  if (config->version/10 != VERSION/10) {
    if (Debug) Serial.print(F("Clearing config with version ")), Serial.println(config->version);
    memset((unsigned char *)config, 0, sizeof(Configuration));
    config->version = VERSION;
  }
  if (config->monPeriod == 0) {
    config->monPeriod = RETRY_PERIOD;
  }
}

void writeConfig(Configuration* config) {
  unsigned char *bytep = (unsigned char *)config;
  if (Debug) Serial.println(F("Writing config"));
  EEPROM.begin(sizeof(Configuration));
  for (int ii = 0; ii < sizeof(Configuration); ii++) {
    EEPROM.write(ii, *bytep++);
  }
  EEPROM.commit();
}

// restart device, saving the reason
void restart(bootReason reason) {
  if (Debug) Serial.print(F("Restarting (")), Serial.print(reason), Serial.println(F(")"));
  if (reason != Config.boot) {
    Config.boot = ((int)Debug << 2) | reason;
    writeConfig(&Config);
  }
  cyclePin(LED_PIN, 6, true);
  ESP.restart();
}

// Write the alarm pin specified by the AlarmPin variable.
// The period specifies the alarm duration in milliseconds.
// If period is 0 then the alarm is continous.
// Side effects:
//   XPin[xAlarmed] is true if the alarm is set, false otherwise.
//   XPin[xAlarms] is the alarm count, which is incremented each time the alarm is set.
//   AlarmedTime is the time the alarm was set.
void writeAlarm(bool alarm, int period) {
  int pin = Config.vars[pvAlarmPin];
  int level = Config.vars[pvAlarmLevel];
  if (Config.vars[pvAlarmNetwork] == 0 && Config.vars[pvAlarmVoltage] == 0) {
    return;
  }
  pinMode(pin, OUTPUT);
  if (!alarm) {
    if (Debug) Serial.print(F("Write alarm: D")), Serial.print(pin), Serial.print(F("=")), Serial.println(!level);
    digitalWrite(pin, !level);
    XPin[xAlarmed] = false;
    AlarmedTime = 0;
    return;
  }
  if (Debug) Serial.print(F("Write alarm: D")), Serial.print(pin), Serial.print(F("=")), Serial.println(level);
  digitalWrite(pin, level);
  XPin[xAlarms]++;
  if (AlarmedTime == 0) {
    AlarmedTime = millis();
  }
  if (period == 0) {
    XPin[xAlarmed] = true;
    return;
  }
  delay(period);
  if (Debug) Serial.print(F("Write alarm: D")), Serial.print(pin), Serial.print(F("=")), Serial.println(!level);
  digitalWrite(pin, !level);
  XPin[xAlarmed] = false;
  // NB: don't reset AlarmedTime
}

// Write the pin specified by the SensePin variable, to turn sensors on or off.
void senseControl(bool on) {
  if (Config.vars[pvSensePin] == 0) {
    return;
  }
  pinMode(Config.vars[pvSensePin], OUTPUT);
  if (on) {
    if (Debug) Serial.println(F("Sense on"));
    digitalWrite(Config.vars[pvSensePin], HIGH);
    delay(SENSE_DELAY);
  } else {
    if (Debug) Serial.println(F("Sense off"));
    digitalWrite(Config.vars[pvSensePin], LOW);
  }
}

// communications
// control WiFi radio, or restart the device if WiFi is misbehaving.
void wifiControl(bool on) {
  if (on) {
    if (WiFi.status() == WL_CONNECTED) {
      return;
    }
    WiFi.forceSleepWake();
    delay(WIFI_DELAY);
    for (int attempts = 0; !WiFi.mode(WIFI_STA) && attempts < WIFI_ATTEMPTS; attempts++) {
      delay(WIFI_DELAY);
    }
    delay(WIFI_DELAY);
    if (!WiFi.mode(WIFI_STA)) {
      if (Debug) Serial.println(F("WiFi mode failure"));
      restart(bootWiFi);
    }
    if (Debug) Serial.println(F("WiFi on"));
  } else {
    if (WiFi.status() == WL_CONNECTED) {
      WiFi.disconnect();
      delay(WIFI_DELAY);
    }
    for (int attempts = 0; WiFi.status() == WL_CONNECTED && attempts < WIFI_ATTEMPTS; attempts++) {
      delay(WIFI_DELAY);
    }
    if (WiFi.status() == WL_CONNECTED) {
      if (Debug) Serial.println(F("WiFi not disconnecting"));
      restart(bootWiFi);
    }
    WiFi.mode(WIFI_OFF);
    delay(WIFI_DELAY);
    WiFi.forceSleepBegin();
    delay(WIFI_DELAY);
    if (Debug) Serial.println(F("WiFi off"));
  }
}

// connect to WiFi (after calling wifiControl)
bool wifiConnect(const char * wifi) {
  // wifi is network info as CSV "ssid,key" (ssid must not contain a comma!)
  // NB: only works for WPA/WPA2 network
  char ssid[WIFI_SIZE], *key;
  if (wifi[0] == '\0') {
    return false; // though not a connection failure
  }
  strcpy(ssid, wifi);
  key = strchr(ssid, ',');
  if (key == NULL) {
    key = ssid + strlen(ssid);
  } else {
    *key++ = '\0';
  }
  if (WiFi.status() == WL_CONNECTED) {
    if (strcmp(WiFi.SSID().c_str(), ssid) == 0) {
      return true;
    }
    WiFi.disconnect();
  }

  if (Debug) Serial.print(F("Requesting DHCP from ")), Serial.println(wifi);
  WiFi.begin(ssid, key);
  delay(WIFI_DELAY);
 
  // WiFi.waitForConnectResult can end up in an infinite loop, so don't use it!
  // NB: connecting can take several seconds, so ensure WIFI_ATTEMPTS x WIFI_DELAY is at least 5000ms.
  for (int attempts = 0; WiFi.status() != WL_CONNECTED && attempts < WIFI_ATTEMPTS; attempts++) {
    delay(WIFI_DELAY);
  }
  if (WiFi.status() != WL_CONNECTED) {
    ConnFailures++;
    if (Debug) Serial.print(F("WiFi connection failed (")),Serial.print(ConnFailures),Serial.println(F(")"));
    return false;
  }
 
  if (Debug) Serial.print(F("Obtained DHCP IP address ")), Serial.println(WiFi.localIP());
  return true;
}

// httpRequests issues an HTTP request. When body is non-null, the method is POST, otherwise it is GET.
bool httpRequest(const char* hostname, int port, const char* path, Pin * body, String& reply) {
  WiFiClient www;
  reply = "";
  if (Debug) Serial.print(F("Connecting to ")), Serial.print(hostname), Serial.print(F(":")), Serial.println(port);
  // NB: connect takes 5s to time out, so don't go overboard retrying
  for (int attempts = 0; !www.connect(hostname, port) && attempts < HTTP_CONNECT_ATTEMPTS; attempts++) {
    delay(HTTP_DELAY);
  }
  if (!www.connected()) {
    ConnFailures++;
    if (Debug) Serial.print(F("HTTP connection failed (")),Serial.print(ConnFailures),Serial.println(F(")"));
    return false;
  }

  if (Debug) Serial.println(F("Connected"));
  if (XPin[xAlarmed]) {
    writeAlarm(false, 0); // reset alarm
  }
  ConnFailures = 0;

  if (body == NULL) {
    www.print(String("GET ") + path + " HTTP/1.1\r\nHost: " + hostname + "\r\nConnection: close\r\n\r\n");
    if (Debug) Serial.print(F("GET ")), Serial.println(path);
  } else {
    // calculate content length
    int sz = 0;
    for (int ii = 0; ii < MAX_PINS && body[ii].name[0] != '\0'; ii++) {
      if (body[ii].name[0] == 'B' && body[ii].value >= 0) {
        sz += body[ii].value;
      }
    }
    // write header
    www.print(String("POST ") + path + " HTTP/1.1\r\nHost: " + hostname +
	      "\r\nContent-Length: " + sz + "\r\nContent-Type: application/octet-stream\r\n\r\n");
    // write body
    for (int ii = 0; ii < MAX_PINS && body[ii].name[0] != '\0'; ii++) {
      if (body[ii].name[0] == 'B' && body[ii].value >= 0) {
	www.write(body[ii].data, body[ii].value);
      }
    }
    if (Debug) Serial.print(F("POST ")), Serial.println(path), Serial.print(F("Content-Length: ")), Serial.println(sz);
  }

  // wait for the server to become available
  for (int wait = HTTP_SERVER_WAIT; !www.available() && wait > 0; wait -= 200) {
    delay(HTTP_DELAY);
  }
 
  // NB: we only care about the first and last lines of the response
  String status = "";
  while (www.available()) {
    reply = www.readStringUntil('\n');
    if (status == "") {
      status = reply;
    }
  }
  if (Debug) Serial.print(F("Status: ")), Serial.println(status), Serial.print(F("Body: ")), Serial.println(reply);
  if (status.startsWith("HTTP/1.1 200 OK")) {
    return true;
  } else {
    ConnFailures++;
    return false;
  }
}

// Issue a single request, writing polled values to 'inputs' and actuated values to 'outputs'.
// Sets 'reconfig' true if reconfiguration is required, false otherwise.
// Side effects: 
//   Updates, enters debug mode or reboots according to the reconfig code ("rc").
bool request(RequestType req, Pin * inputs, Pin * outputs, bool * reconfig, String& reply) {
  char path[MAX_PATH];
  String param;
  IPAddress la;
  *reconfig = false;
  Pin * body = NULL;

  switch (req) {
  case RequestConfig:
    la = WiFi.localIP();
    sprintf(path, "/config?vn=%d&ma=%s&dk=%s&la=%d.%d.%d.%d&ut=%ld", VERSION, MacAddress, Config.dkey,
            la[0], la[1], la[2], la[3], (unsigned long)millis()/1000);
    break;
  case RequestPoll:
    sprintf(path, "/poll?vn=%d&ma=%s&dk=%s", VERSION, MacAddress, Config.dkey);
    for (int ii = 0; ii < MAX_PINS && inputs[ii].name[0] != '\0'; ii++) {
      if (inputs[ii].name[0] == 'B' && inputs[ii].value < 0) {
	// omit missing/partial binary data
	continue;
      }
      sprintf(path + strlen(path), "&%s=%d", inputs[ii].name, inputs[ii].value);
      if (body == NULL && inputs[ii].name[0] == 'B') {
        body = inputs; // binary data is sent in the POST body
      }
    }
    break;
  case RequestAct:
    sprintf(path, "/act?vn=%d&ma=%s&dk=%s", VERSION, MacAddress, Config.dkey); 
    break;
  case RequestVars:
    sprintf(path, "/vars?vn=%d&ma=%s&dk=%s", VERSION, MacAddress, Config.dkey); 
    break;
  }

  if (!httpRequest(SERVICE_HOST, SERVICE_PORT, path, body, reply)) {
    if (Config.vars[pvAlarmNetwork] > 0 && ConnFailures >= Config.vars[pvAlarmNetwork]) {
      // too many network failures; raise the alarm!
      writeAlarm(true, Config.vars[pvAlarmPeriod]);
      ConnFailures = 0;
    }
    return false;
  }

  if (!reply.startsWith("{")) {
    if (Debug) Serial.println(F("Malformed response"));
    return false;
  }

  // since version 138 poll requests also return output values
  if (req == RequestPoll || req == RequestAct) {
    for (int ii = 0; ii < MAX_PINS && outputs[ii].name[0] != '\0'; ii++) {
      if (extractJson(reply, outputs[ii].name, param)) {
        outputs[ii].value = param.toInt();
        writePin(&outputs[ii]);
      } else {
        outputs[ii].value = -1;
        if (Debug) Serial.print(F("Missing value for output pin ")), Serial.println(outputs[ii].name);
      }
    }
  }

  if (extractJson(reply, "rc", param)) {
    switch (param.toInt()) {
    case rcOK:
      break;
    case rcConfigure:
      if (Debug) {
        Serial.println(F("Received config request."));
        Serial.println(F("Debug mode off."));
        Debug = false;
      }
      *reconfig = true;
      Configured = false;
      break;
    case rcReboot:
      Serial.println(F("Received reboot request."));
      if (Configured) {
        restart(bootNormal);
      } // else ignore reboot request unless configured
      break;
    case rcDebug:
      if (!Debug) {
        Serial.println(F("Debug mode on."));
        Debug = true;
      }
      break;
    case rcUpgrade:
    default:
      break;
    }
  }

  if (extractJson(reply, "vs", param)) {
    int vs = param.toInt();
    if (vs != VarSum) {
      if (Debug) Serial.println(F("Varsum changed"));
    }
    VarSum = vs;
  }

  if (extractJson(reply, "er", param)) {
    // we let the caller deal with errors
    if (Debug) Serial.print(F("Error: ")), Serial.println(param);
  }

  return true;
}

// request config, and return true upon success, or false otherwise
// Side effects:
//   Sets Configured to true upon success.
bool config() {
  String reply, error, param;
  bool reconfig = false;
  bool changed = false;

  if (!request(RequestConfig, NULL, NULL, &reconfig, reply) || extractJson(reply, "er", param)) {
    cyclePin(LED_PIN, 2);
    return false;
  } 

  if (extractJson(reply, "mp", param) && param.toInt() != Config.monPeriod) {
    Config.monPeriod = param.toInt();
    if (Debug) Serial.print(F("Mon. period changed: ")), Serial.println(Config.monPeriod);
    changed = true;
  }
  if (extractJson(reply, "ap", param) && param.toInt() != Config.actPeriod) {
    Config.actPeriod = param.toInt();
    if (Debug) Serial.print(F("Act. period changed: ")), Serial.println(Config.actPeriod);
    changed = true;
  }
  if (extractJson(reply, "wi", param) && param != Config.wifi) {
    padCopy(Config.wifi, param.c_str(), WIFI_SIZE);
    if (Debug) Serial.print(F("Wifi changed: ")), Serial.println(Config.wifi);
    changed = true;
  }
  if (extractJson(reply, "dk", param) && param != Config.dkey) {
    padCopy(Config.dkey, param.c_str(), DKEY_SIZE);
    if (Debug) Serial.print(F("Dkey changed: ")), Serial.println(Config.dkey);
    changed = true;
  }
  if (extractJson(reply, "ip", param) && param != Config.inputs) {
    padCopy(Config.inputs, param.c_str(), IO_SIZE);
    if (Debug) Serial.print(F("Inputs changed: ")), Serial.println(Config.inputs);
    changed = true;
  }
  if (extractJson(reply, "op", param) && param != Config.outputs) {
    padCopy(Config.outputs, param.c_str(), IO_SIZE);
    if (Debug) Serial.print(F("Outputs changed: ")), Serial.println(Config.outputs);
    changed = true;
  }

  if (changed) {
    writeConfig(&Config);
    initPins();
    cyclePin(LED_PIN, 4);
  }
  Configured = true;
  return true;
}

// Retrieve NetReceiver vars, return the persistent vars and
// set changed to true if a persistent var has changed.
// Transient vars, such as "id" are not saved.
// Missing persistent vars default to 0.
bool getVars(int vars[VAR_SIZE], bool* changed) {
  String reply, error, id, param;
  bool reconfig;
  *changed = false;

  if (!request(RequestVars, NULL, NULL, &reconfig, reply) || extractJson(reply, "er", param)) {
    return false;
  }
  bool hasId = extractJson(reply, "id", id);
  if (hasId && Debug) Serial.print(F("id=")), Serial.println(id);

  for  (int ii = 0; ii < VAR_SIZE; ii++) {
    int val = 0;
    if (hasId) {
      String var = id + '.' + String(PvNames[ii]);
      if (extractJson(reply, var.c_str(), param)) {
        val = param.toInt();
      }
    } else {
      if (extractJson(reply, PvNames[ii], param)) {
        val = param.toInt();
      }
    }
    vars[ii] = val;
    if (Config.vars[ii] != val) {
      *changed = true;
    }
    if (Debug) Serial.print(PvNames[ii]), Serial.print(F("=")), Serial.println(vars[ii]);
  }
  return true;
}

// write vars
void writeVars(int vars[VAR_SIZE]) {
  if (Debug) Serial.println(F("Writing vars"));
  memcpy(Config.vars, vars, sizeof(Config.vars));
  writeConfig(&Config);
  writeAlarm(false, 0);
}

// init should be called from setup once
void init(void) {
  // disable WiFi persistence in flash memory
  WiFi.persistent(false);
  WiFi.mode(WIFI_STA);
  Serial.begin(115200);
  pinMode(LED_PIN, OUTPUT);
  digitalWrite(LED_PIN, HIGH);
  delay(2000);

  // get config
  readConfig(&Config);
  // did we boot normally?
  if (Config.boot != bootNormal) {
    Serial.print(F("boot: ")), Serial.println(Config.boot);
    if ((Config.boot & bootMask) == bootAlarm) {
      // NB: reboot in Configured state, otherwise voltage check will be disabled.
      Configured = true;
      Debug = (bool)(Config.boot >> 2);
    }
    XPin[xBoot] = Config.boot;
    Config.boot = bootNormal;
    writeConfig(&Config);
  }
  // initialize GPIO pins
  initPins();
  // reset alarm
  writeAlarm(false, 0);
  // save the formatted MAC address
  byte mac[6];
  WiFi.macAddress(mac);
  fmtMacAddress(mac, MacAddress);

  if (Debug) {
    Serial.println(F("Config"));
    Serial.print(F("ma: ")), Serial.println(MacAddress);
    Serial.print(F("wi: ")), Serial.println(Config.wifi);
    Serial.print(F("dk: ")), Serial.println(Config.dkey);
    Serial.print(F("ip: ")), Serial.println(Config.inputs);
    Serial.print(F("op: ")), Serial.println(Config.outputs);
    Serial.print(F("mp: ")), Serial.println(Config.monPeriod);
    Serial.print(F("ap: ")), Serial.println(Config.actPeriod);
  }
}

// Pause to maintain timing accuracy, adjusting the timing lag in the process.
// Pulsed is how long we've pulsed in milliseconds, or the equivalent delay if suppressing pulses.
// If we're here because of an error and we're not pulsing, we just pause long enough to retry,
// since timing accuracy is moot. If pulsing, we pause for the active time remaining this cycle,
// unless we're out of time.
bool pause(bool err, unsigned long pulsed, long * lag) {
  if (err && pulsed == 0) {
    delay(RETRY_PERIOD * 1000L);
    return err;
  }

  unsigned long now = millis();
  long remaining = Config.actPeriod * 1000L - pulsed;
  *lag += (now - Time - pulsed);

  if (Debug) {
    Serial.print(F("Pulsed time: ")), Serial.print(pulsed), Serial.println(F("ms"));
    Serial.print(F("Total lag: ")), Serial.print(*lag), Serial.println(F("ms"));
    Serial.print(F("Run time: ")), Serial.print(now - Time), Serial.println(F("ms"));
  }

  if (remaining > *lag) {
    remaining -= *lag;
    if (Debug) Serial.print(F("Pausing for ")), Serial.print(remaining), Serial.println(F("ms"));
    delay(remaining);
    *lag = 0;
  } else {
    if (Debug) Serial.println(F("Skipped pause"));
  }
  return err;
}

// run should be called from loop until it returns true, e.g., 
//  while (!run(&varsum)) {
//    ;
//  }
// May return false while either connected to WiFi or not.
// NB: pulse suppression must be re-enabled each cycle via the X14 pin.
bool run(int* varsum) {
  Pin inputs[MAX_PINS], outputs[MAX_PINS];
  String reply;
  bool reconfig = false;
  unsigned long pulsed = 0;
  long lag = 0;
  unsigned long now = millis();
  
  // measure lag to maintain accuracy between cycles
  if (Time > 0 && now > Time) {
    lag = (long)(now - Time) - (Config.monPeriod * 1000L);
    if (Debug) Serial.print(F("Initial lag: ")), Serial.print(lag), Serial.println(F("ms"));
    if (lag < 0) {
      lag = 0;
    }
  }
  Time = now; // record the start of each cycle

  // restart if the alarm has gone for on too long
  if (Config.vars[pvAlarmRestart] > 0 && AlarmedTime > 0) {
    int alarmed; // alarmed duration in minutes
    if (now > AlarmedTime) {
      alarmed = (now - AlarmedTime)/60000;
    } else { // rolled over
      alarmed = ((0xffffffff - AlarmedTime) + now)/60000;
    }
    if (Debug) Serial.print(F("Alarm duration: ")), Serial.println(alarmed);
    if (alarmed >= Config.vars[pvAlarmRestart]) {
      restart(bootAlarm);
    }
  }

  // if we're not configured, tell the user our MAC address 
  if (!Configured || Config.dkey == 0) {
    Serial.print(F("NetSender v")), Serial.println(VERSION);
    Serial.print(F("My MAC address: ")), Serial.println(MacAddress);
  }

  // pulsing happens before anything else, regardless of network connectivity
  if (Config.vars[pvPulses] != 0 && Config.vars[pvPulsePeriod] != 0) {
    pulsePin(LED_PIN, Config.vars[pvPulses], Config.vars[pvPulsePeriod], Config.vars[pvPulseDutyCycle]);
    pulsed = (unsigned long)Config.vars[pvPulses] * Config.vars[pvPulsePeriod];
    long gap = (Config.vars[pvPulseCycle] * 1000L) - ((long)Config.vars[pvPulses] * Config.vars[pvPulsePeriod]);
    if (gap > 0) {
      for (int spanned = 0; spanned < Config.monPeriod - Config.vars[pvPulseCycle]; spanned += Config.vars[pvPulseCycle]) {
        if (Debug) Serial.print(F("Pulse group gap: ")), Serial.print(gap), Serial.println(F("ms"));
        delay(gap);
        pulsePin(LED_PIN, Config.vars[pvPulses], Config.vars[pvPulsePeriod], Config.vars[pvPulseDutyCycle]);
        pulsed += (gap + ((unsigned long)Config.vars[pvPulses] * Config.vars[pvPulsePeriod]));
      }
    }
  }
  XPin[xSuppress] = 0;

  // Check voltage unless we're not configured
  // This ensures we do not raise the alarm until we've communicated with NetReceiver at least once.
  if (Configured && Config.vars[pvAlarmVoltage] > 0) {
    Pin pin = { "A0" };
    senseControl(true);
    XPin[xA0] = readPin(&pin);
    senseControl(false);
    if (Debug) Serial.print(F("A0=")), Serial.println(XPin[xA0]);
    if (XPin[xA0] < Config.vars[pvAlarmVoltage]) {
      if (!XPin[xAlarmed]) {
        // low voltage; raise the alarm and turn off WiFi!
        if (Debug) Serial.println(F("Low voltage alarm!"));
        cyclePin(LED_PIN, 5, true);
        writeAlarm(true, 0);
        wifiControl(false);
      }
      return pause(false, pulsed, &lag);
    }
    if (XPin[xAlarmed]) {
      if (XPin[xA0] < Config.vars[pvAlarmRecoveryVoltage]) {
        return pause(false, pulsed, &lag);
      }
      if (Debug) Serial.println(F("Low voltage alarm cleared"));
      writeAlarm(false, 0);
    }
  } else if (Debug) {
    Serial.println(F("Skipped voltage check"));
    XPin[xA0] = -1;
  }

  wifiControl(true);
  bool connected = wifiConnect(Config.wifi);
  if (!connected && strcmp(Config.wifi, DEFAULT_WIFI) != 0) {
    delay(WIFI_DELAY);
    connected = wifiConnect(DEFAULT_WIFI);
  }
  if (!connected) {
    if (Config.vars[pvAlarmNetwork] > 0 && ConnFailures >= Config.vars[pvAlarmNetwork]) {
      // too many network failures; raise the alarm!
      writeAlarm(true, Config.vars[pvAlarmPeriod]);
      ConnFailures = 0;
    } else {
      cyclePin(LED_PIN, 3);
    }
    wifiControl(false);
    return pause(false, pulsed, &lag);
  }
  
  // Attempt configuration whenever:
  //   (1) there are no inputs and no outputs, or 
  //   (2) we receive a rcConfigure code
  if (Config.inputs[0] == '\0' && Config.outputs[0] == '\0') {
    if (!config()) {
      return pause(false, pulsed, &lag);
    }
  }

  // Since version 138 the poll method returns outputs as well as inputs,
  if (Config.inputs[0] != '\0') {
    int sz = setPins(Config.inputs, inputs);
    senseControl(true);
    for (int ii = 0; ii < sz; ii++) {
      readPin(&inputs[ii]);
    }
    senseControl(false);
    setPins(Config.outputs, outputs);
    if (!request(RequestPoll, inputs, outputs, &reconfig, reply)) {
      return pause(false, pulsed, &lag);
    }
  }

  // so we only need to call the act method if there are no inputs.
  if (Config.inputs[0] == '\0' && Config.outputs[0] != '\0') {
    setPins(Config.outputs, outputs);
    if (!request(RequestAct, NULL, outputs, &reconfig, reply)) {
      return pause(false, pulsed, &lag);
    }
  }

  if (reconfig && !config()) {
    return pause(false, pulsed, &lag);
  }

  if (*varsum != VarSum) {
    int vars[VAR_SIZE];
    bool changed;
    if (!getVars(vars, &changed)) {
      return pause(false, pulsed, &lag);
    }
    if (changed) {
      if (Debug) Serial.println(F("Persistent variable(s) changed"));
      writeVars(vars);
    }
    *varsum = VarSum;
  }

  wifiControl(false);

  // adjust for pulse timing inaccuracy and network time
  pause(true, pulsed, &lag);
  cyclePin(LED_PIN, 1);

  if (Config.monPeriod == Config.actPeriod) {
    return true;
  }
  
  long remaining;
  if (Config.actPeriod * 1000L > pulsed) {
    remaining = (Config.monPeriod - Config.actPeriod) * 1000L;
  } else {
    remaining = Config.monPeriod * 1000L - pulsed;
  }
  if (remaining > lag) {
    remaining -= lag;
    if (Debug) Serial.print(F("Deep sleeping for ")), Serial.print(remaining), Serial.println(F("ms"));
    ESP.deepSleep(remaining * 1000L);
  }
  return true;
}

} // end namespace
