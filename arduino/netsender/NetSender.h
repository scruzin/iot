 /*
  Name:
    NetSender - an Arduino library for sending measured values to the cloud and writing values from the cloud.

  Description:
    see http://netreceiver.appspot.com/help.

  License:
    Copyright (C) 2017-2018 Alan Noble.

    This file is part of NetSender. NetSender is free software: you can
    redistribute it and/or modify it under the terms of the GNU
    General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.

    NetSender is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with NetSender in gpl.txt.  If not, see
    <http://www.gnu.org/licenses/>.
*/

#ifndef NetSender_H
#define NetSender_H

namespace NetSender {

#define VERSION                141
#define SERVICE_HOST           "netreceiver.appspot.com"
#define SERVICE_PORT           80
#define DEFAULT_WIFI           "netreceiver,netsender"
#define LED_PIN                2
#define WIFI_SIZE              80
#define DKEY_SIZE              20
#define MAX_PINS               10
#define PIN_SIZE               4
#define IO_SIZE                (MAX_PINS * PIN_SIZE)
#define VAR_SIZE               12
#define RESERVED_SIZE          44
#define MAC_SIZE               18
#define MAX_PATH               256
#define RETRY_PERIOD           5   // seconds
#define WIFI_ATTEMPTS          100
#define HTTP_CONNECT_ATTEMPTS  3
#define HTTP_SERVER_WAIT       2000

typedef enum {
  RequestConfig = 0,
  RequestPoll = 1,
  RequestAct = 2,
  RequestVars = 3,
} RequestType;

typedef char PinName[PIN_SIZE];

// Configuration parameters are saved to the first 256 bytes of EEPROM as follows:
//   Version        (length 2)
//   Mon. period    (length 2)
//   Act. period    (length 2)
//   Boot           (length 2)
//   WiFi ssid,key  (length 80)
//   Device key     (length 20)
//   Inputs         (length 40)
//   Outputs        (length 40)
//   Vars           (length 24)
//   Reserved       (length 44)
typedef struct {
  int version;
  int monPeriod;
  int actPeriod;
  int boot;
  char wifi[WIFI_SIZE];
  char dkey[DKEY_SIZE];
  char inputs[IO_SIZE];
  char outputs[IO_SIZE];
  int  vars[VAR_SIZE];
  char reserved[RESERVED_SIZE];
} Configuration;

// Pin represents a pin name and value and optional POST data.
typedef struct {
  char name [PIN_SIZE];
  int value;
  byte * data;
} Pin;

// ReaderFunc represents a pin reading function.
typedef int (*ReaderFunc)(Pin *);

// exported globals
extern Configuration Config;
extern ReaderFunc ExternalReader;
extern ReaderFunc BinaryReader;
extern int VarSum;
extern bool Debug;

// init should be called from setup once.
// run should be called from loop until it returns true, e.g., 
//  while (!run(&vs)) {
//    delay(RETRY_PERIOD * (long)1000);
//  }
extern void init();
extern bool run(int*);

// low-level functions for clients wishing to re-implement run()
extern bool connect(const char*);
extern bool request(RequestType, Pin*, Pin*, bool*, String&);
extern bool config();
extern bool vars();

} // end namespace
#endif
