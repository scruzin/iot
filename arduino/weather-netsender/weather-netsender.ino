 /*
  Name:
    Weather-NetSender - an Arduino program to sender SDL Weather Rack data to the cloud using NetSender.
    See the NetSender library for more details.

  License:
    Copyright (C) 2017 Alan Noble.

    This file is part of NetSender. NetSender is free software: you can
    redistribute it and/or modify it under the terms of the GNU
    General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.

    NetSender is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with NetSender in gpl.txt.  If not, see
    <http://www.gnu.org/licenses/>.
*/

#include "NetSender.h"
#include "SDLWeather.h"

SDLWeather WeatherStation(4, 5, A0);
float RainTotal = 0.0;
int varsum;

int weatherReader(NetSender::Pin *pin) {
  pin->value = -1;
  if (pin->name[0] != 'X') {
    return -1;
  }
  int pn = atoi(pin->name + 1);
  switch(pn) {
  case 0: // TWS
    pin->value = 10 * WeatherStation.getWindSpeed();
    break;
  case 1: // TWG
    pin->value = 10 * WeatherStation.getWindGust();
    break;
  case 2: // TWA
    pin->value = 10 * WeatherStation.getWindDirection();
    break;
  case 3: // PPT
    RainTotal += WeatherStation.getCurrentRainTotal();
    pin->value = 10 * RainTotal;
    break;
  default:
    return -1;
  }
  return pin->value;
}

// required Arduino routines
// NB: setup runs everytime ESP8266 comes out of a deep sleep
void setup(void) {
  NetSender::ExternalReader = &weatherReader;
  NetSender::init();
  loop();
}

void loop() {
  while (!NetSender::run(&varsum)) {
    delay(RETRY_PERIOD * (long)1000);
  }
}
