# Readme

Weather-netsender is a netsender client to send SwitchDocs Lab (SDL) Weather Rack data to the cloud.

SDLWeather is a version of the SwitchDocs Labs SDL_Weather_80422 library modified for the ESP8266.
See SDLWeather.cpp for a description of the changes.

The Weather Rack is configured via the SDLWeather constructor, as follows:

  SDLWeather WeatherStation(4, 5, A0);

This specifies that the WeatherPi Arduino Weather Board is interfaced to the ESP8266 as follows:

* Anemometer is connected to GPIO 4
* Rain bucket is connected to GPIO 5
* Wind vane is connected to A0 (via a step-down voltage divider)

Note that the first two arguments can be any GPIO pins capable of servicing interrupts.

Weather-netsender uses "external" pins to collect Weather Rack Data, mapped as follows:

* X0 = True Wind Speed (TWS)
* X1 = True Wind Gust (TWG)
* X2 = True Wind Angle (TWA)
* X3 = Total rainfall (Precipitation) (PPT)

Note that the total rainfall is reset to zero upon startup. 
See the WeatherReader function for details.

# See Also

* [WeatherPi Arduino Weather Board](http://www.switchdoc.com/wp-content/uploads/2016/02/WeatherPiArdunio_Current.pdf)
* [NetReceiver Help](http://netreceiver.appspot.com/help)
* [SDL_Weather_80422 GitHub repository](https://github.com/switchdoclabs/SDL_Weather_80422)
* [Weather Rack product](https://www.amazon.com/dp/B00QURVHN6/)

# License

Weather-netsender is Copyright (C) 2017 Alan Noble.

It is free software: you can redistribute it and/or modify them
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

It is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License
along with NetReceiver in gpl.txt.  If not, see [GNU licenses](http://www.gnu.org/licenses).

