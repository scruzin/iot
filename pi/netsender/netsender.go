/*
NAME
  netsender - package for netsender clients implementing basic pin reading/actuating and communication to the server.

DESCRIPTION
  See Readme.md

AUTHOR
  Jack Richardson <richardson.jack@outlook.com>
  Alan Noble <anoble@gmail.com>

LICENSE
  netsender is Copyright (C) 2017-2018 the Australian Ocean Lab (AusOcean).

  It is free software: you can redistribute it and/or modify them
  under the terms of the GNU General Public License as published by the
  Free Software Foundation, either version 3 of the License, or (at your
  option) any later version.

  It is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
  for more details.

  You should have received a copy of the GNU General Public License
  along with netsender in gpl.txt.  If not, see [GNU licenses](http://www.gnu.org/licenses).
*/

package netsender

import (
	"bufio"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net"
	"net/http"
	"os"
	"runtime"
	"strconv"
	"strings"
	"sync"
	"syscall"
	"time"

	"bitbucket.org/ausocean/utils/smartLogger"

	"github.com/kidoman/embd"
	"github.com/kidoman/embd/convertors/mcp3008"
	_ "github.com/kidoman/embd/host/all"
)

// defaults and networking consts
const (
	version         int    = 135
	serviceHost     string = "netreceiver.appspot.com"
	servicePort     int    = 80
	defaultWIFI     string = "netreceiver,netsender"
	configFile      string = "/etc/netsender.conf"
	retryPeriod     int    = 30 // seconds
	ledPin          int    = 16
	defaultLogLevel        = 3
)

// NetReceiver reconfig codes and related consts
const (
	rcOK           = 0
	rcConfigure    = 1
	rcReboot       = 2
	rcDebug        = 3
	xVirtualMem    = 22
	stackTraceSize = 1 << 16
)

var (
	config         map[string]string
	configured     bool = false
	configMutex         = &sync.Mutex{}
	adc            *mcp3008.MCP3008
	lastActuation  string
	ExternalReader func(int) (int, error) = nil
	logger                                = smartLogger.New(defaultLogLevel, smartLogger.Blue) //Logger to stderr
	varSum         int                    = 0
)

//Pin type checks
func isDigital(pin string) bool {
	return pin[0] == 'D'
}
func isAnalog(pin string) bool {
	return pin[0] == 'A'
}
func isExternal(pin string) bool {
	return pin[0] == 'X'
}

//HTTP request types
const (
	RequestConfig = 0
	RequestPoll   = 1
	RequestAct    = 2
	RequestVars   = 3
)

func GetLogger() smartLogger.LogInstance {
	return logger
}

func getPinNumber(pinName string) (int, error) {
	if pinName[len(pinName)-1] == '*' {
		return strconv.Atoi(string(pinName[:len(pinName)-1]))
	}
	return strconv.Atoi(pinName)
}

func GetConfigParam(param string) string {
	configMutex.Lock()
	defer configMutex.Unlock()
	return config[param]
}

// extractJsonInt extracts the integer value of a given key from a JSON string
func extractJsonInt(JSON string, name string) (int, error) {
	data := make(map[string]interface{})
	err := json.Unmarshal([]byte(JSON), &data)
	if err != nil {
		return -1, err
	}

	if data[name] != nil {
		return int(data[name].(float64)), nil
	}
	return -1, errors.New(name + " not found in JSON")
}

// extractJson extracts the string value of a given key from a JSON string
func extractJsonString(JSON string, name string) (string, error) {
	data := make(map[string]interface{})
	err := json.Unmarshal([]byte(JSON), &data)
	if err != nil {
		return "", err
	}

	if data[name] != nil {
		return data[name].(string), nil
	}
	return "", errors.New(name + " not found in JSON")
}

func SplitCSV(str string) []string {
	if str == "" {
		return []string{}
	} else {
		return strings.Split(str, ",")
	}
}

//initPins intializes all required GPIO pins and performs direction setup on digital pins
func initPins() {
	for _, pinName := range SplitCSV(config["inputs"]) {
		if pin, err := getPinNumber(pinName[1:]); err != nil {
			logger.Log("Error", "Pin name not a number")
		} else {
			if isDigital(pinName) {
				embd.SetDirection(pin, embd.In)
			}
		}
	}

	for _, pinName := range SplitCSV(config["outputs"]) {
		if pin, err := getPinNumber(pinName[1:]); err != nil {
			logger.Log("Error", "Pin name not a number")
		} else {
			if isDigital(pinName) {
				embd.SetDirection(pin, embd.Out)
			}
		}
	}
}

//readPin returns the integer value that is read from the specified pin
func readPin(pinName string) (int, error) {
	pin, err := getPinNumber(pinName[1:])
	if err != nil {
		logger.Log("Error", "Pin name not a number")
		return -1, err
	}

	switch {
	case isDigital(pinName):
		if val, err := embd.DigitalRead(pin); err != nil {
			logger.Log("Error", "Couldnt read from digital pin")
			return -1, err
		} else {
			return val, nil
		}

	case isAnalog(pinName): //Analog reading is only supported through the MCP3008 ADC chip
		return adc.AnalogValueAt(pin)

	case isExternal(pinName):
		// check for built-in "external" pins first
		if pin == xVirtualMem {
			var ms runtime.MemStats
			runtime.ReadMemStats(&ms)
			return int(ms.Sys / 1024), nil
		}

		if ExternalReader != nil {
			return ExternalReader(pin)
		} else {
			logger.Log("Error", "External pin not defined")
			return -1, errors.New("External pin not defined")
		}

	default:
		logger.Log("Error", "Read performed on invalid pin "+pinName)
		return -1, errors.New("Read performed on invalid pin")
	}
}

//cyclePin cycles the specified digital pin a given number of times
func cyclePin(pin int, cycles int) {
	for i := (cycles * 2) - 1; i >= 0; i-- {
		if i%2 == 1 {
			embd.DigitalWrite(pin, embd.High)
		} else {
			embd.DigitalWrite(pin, embd.Low)
		}
		time.Sleep(250 * time.Millisecond)
	}
	logger.Log("Detail", "Pin "+strconv.Itoa(pin)+" cycled "+strconv.Itoa(cycles)+" times")
}

//writePin writes an integer value to the specified pin
func writePin(pinName string, value int) error {
	pin, err := getPinNumber(pinName[1:])
	if err != nil {
		logger.Log("Error", "Pin name not a number")
		return err
	}
	if isDigital(pinName) {
		if value < 0 {
			cyclePin(pin, -value)
		} else {
			switch {
			case value == 0:
				embd.DigitalWrite(pin, embd.Low)
			default:
				embd.DigitalWrite(pin, embd.High)
			}
		}
	} else {
		logger.Log("Warning", "Unable to write to non-digital pin")
	}
	return nil
}

//writeConfig writes the configuration information from the configuration map and into the file located at /etc/netsender.conf
func writeConfig() {
	var configText string

	logger.Log("Debug", "Writing config")
	configText += "mac " + config["mac"] + "\n"
	configText += "monPeriod " + config["monPeriod"] + "\n"
	configText += "actPeriod " + config["actPeriod"] + "\n"
	configText += "wifi " + config["wifi"] + "\n"
	configText += "dKey " + config["dKey"] + "\n"
	configText += "inputs " + config["inputs"] + "\n"
	configText += "outputs " + config["outputs"]

	err := ioutil.WriteFile(configFile, []byte(configText), 0666)
	if err != nil {
		logger.Log("Error", "Error writing to config file: "+err.Error())
	}
}

//readConfig reads the configuration information stored at /etc/netsender.conf and saves it to the configuration map
func readConfig() error {
	file, err := os.Open(configFile)
	if err != nil {
		logger.Log("Error", "Error opening file for read: "+err.Error())
		return err
	}
	defer file.Close()
	configMutex.Lock()
	defer configMutex.Unlock()

	configNumbers := []string{"monPeriod", "actPeriod"}
	configStrings := []string{"wifi", "dKey", "inputs", "outputs", "mac"}

	for i := 0; i < len(configNumbers); i++ {
		name := configNumbers[i]
		if val, err := GetConfigValueFromFile(name, file); err != nil {
			logger.Log("Warning", "Cant find "+name+" in config file")
			config[name] = ""
		} else {
			if _, err := strconv.Atoi(val); err != nil {
				logger.Log("Warning", "Error reading config for "+name+" Expected an int but didnt find one")
				config[name] = ""
			} else {
				config[name] = val
			}
		}
	}

	for i := 0; i < len(configStrings); i++ {
		name := configStrings[i]
		if val, err := GetConfigValueFromFile(name, file); err != nil {
			logger.Log("Warning", "Cant find "+name+" in config file")
			config[name] = ""
		} else {
			config[name] = val
		}
	}
	return nil
}

//send builds the URL for the specified requestTyp to be used in a HTTP get request
func Send(requestType int, pins []string) (string, bool, error) {
	path := ""
	reconfig := false
	var reply string
	var err error

	switch requestType {
	case RequestConfig:
		path = fmt.Sprintf("/config?vn=%d&ma=%s&dk=%s&la=%s&ut=%f", version, config["mac"], config["dKey"], getLocalAddr(), float64(time.Millisecond)/1000)

	case RequestPoll:
		path = fmt.Sprintf("/poll?vn=%d&ma=%s&dk=%s", version, config["mac"], config["dKey"])
		for _, pinName := range pins {
			var pinVal int
			var err error
			if pinVal, err = readPin(pinName); err != nil {
				logger.Log("Error", err.Error())
			}
			path += "&" + pinName + "=" + strconv.Itoa(pinVal)
			time.Sleep(10 * time.Millisecond)
		}

	case RequestAct:
		path = fmt.Sprintf("/act?vn=%d&ma=%s&dk=%s", version, config["mac"], config["dKey"])

	case RequestVars:
		path = fmt.Sprintf("/vars?vn=%d&ma=%s&dk=%s", version, config["mac"], config["dKey"])

	default:
		logger.Log("Warning", "Request type not recognized")
	}
	logger.Log("Detail", "Path: "+path)

	if reply, err = httpGet(serviceHost, servicePort, path); err != nil {
		return reply, false, err
	}

	logger.Log("Debug", "Reply: "+reply)
	if !strings.HasPrefix(reply, "{") {
		return "", false, errors.New("JSON format error")
	}
	var rc, vs int
	if er, err := extractJsonString(reply, "er"); err == nil {
		logger.Log("Warning", "Error in config response: "+er)
	}
	if rc, err = extractJsonInt(reply, "rc"); err == nil {
		switch rc {
		case rcConfigure:
			reconfig = true
			configured = false
		case rcReboot:
			logger.Log("Info", "Received reboot request. Rebooting...")
			syscall.Reboot(syscall.LINUX_REBOOT_CMD_RESTART)
		case rcDebug:
			logger.Log("Info", "Received debug request")
			if !configured {
				logger.Log("Info", "Reconfig required")
				break
			}
			buf := make([]byte, stackTraceSize)
			size := runtime.Stack(buf, true)
			logger.Log("Info", "Stack trace:\n"+string(buf[:size]))
			reconfig = true
			configured = false
		default:
		}
	}
	if vs, err = extractJsonInt(reply, "vs"); err == nil {
		if vs != varSum {
			logger.Log("Debug", "Varsum changed")
		}
		varSum = vs
	}
	return reply, reconfig, nil
}

// get preferred local IP address as a string
// NB: dialing a UDP connection does not actually create a connection
func getLocalAddr() string {
	if conn, err := net.Dial("udp", "8.8.8.8:80"); err == nil {
		defer conn.Close()
		str := conn.LocalAddr().String()
		if port := strings.Index(str, ":"); port > 0 {
			return str[:port] // strip port number
		}
		return str
	}
	return ""
}

//httpGet makes a get request to the net receiver server
func httpGet(hostname string, port int, path string) (string, error) {
	client := &http.Client{Timeout: 10 * time.Second, Transport: http.DefaultTransport}
	address := hostname + ":" + strconv.Itoa(port)
	logger.Log("Detail", "Connecting to: "+address)

	attempts := 0
	for _, err := net.DialTimeout("tcp", address, 10*time.Second); err != nil && attempts < 10; {
		time.Sleep(500 * time.Millisecond)
		attempts++
		_, err = net.DialTimeout("tcp", address, 10*time.Second)
	}
	if attempts == 10 {
		logger.Log("Warning", "Unable to connect to address: "+address)
		return "", errors.New("Can't connect to network")
	}
	logger.Log("Detail", "Connected to "+address)

	resp, err := client.Get("http://" + address + path)
	if err != nil {
		logger.Log("Error", "Error sending GET request to "+address+":"+err.Error())
		return "", err
	}

	logger.Log("Debug", "GET request to "+address+path+" was successful")
	defer resp.Body.Close()
	var responseString []byte

	if responseString, err = ioutil.ReadAll(resp.Body); err != nil {
		logger.Log("Error", err.Error())
	}
	bodyLines := strings.Split(string(responseString), "\n")

	if resp.Status == "200 OK" {
		return bodyLines[len(bodyLines)-1], nil
	} else {
		return "", errors.New("Response was not 200 OK")
	}
}

// returns true if (1) NetSender has been configured the first time or
// (2) configured since the most recent reconfig request
func IsConfigured() bool {
	return configured
}

//getConfig requests configuration information from the net receiver server and determines if a local update is required
func GetConfig() error {
	changed := false
	var reply string
	var err error

	if reply, _, err = Send(RequestConfig, nil); err != nil {
		cyclePin(ledPin, 2)
		return err
	}

	logger.Log("Info", "Received config data")
	logger.Log("Info", reply)

	configMutex.Lock()
	defer configMutex.Unlock()

	if param, err := extractJsonInt(reply, "mp"); err == nil {
		val := strconv.Itoa(param)
		if val != config["monPeriod"] {
			config["monPeriod"] = val
			logger.Log("Info", "MonPeriod changed: "+val)
			changed = true
		}
	} else {
		logger.Log("Warning", "Could not extract mp from JSON response")
	}
	if param, err := extractJsonInt(reply, "ap"); err == nil {
		val := strconv.Itoa(param)
		if val != config["actPeriod"] {
			config["actPeriod"] = val
			logger.Log("Info", "ActPeriod changed: "+val)
			changed = true
		}
	} else {
		logger.Log("Warning", "Could not extract ap from JSON response")
	}
	if param, err := extractJsonString(reply, "wi"); err == nil {
		if param != config["wifi"] {
			config["wifi"] = param
			logger.Log("Info", "WIFI changed: "+param)
			changed = true
		}
	} else {
		logger.Log("Warning", "Could not extract wifi from JSON response")
	}
	if param, err := extractJsonInt(reply, "dk"); err == nil {
		val := strconv.Itoa(param)
		if val != config["dKey"] {
			config["dKey"] = val
			logger.Log("Info", "Device key changed: "+val)
			changed = true
		}
	}
	if param, err := extractJsonString(reply, "ip"); err == nil {
		if param != config["inputs"] {
			config["inputs"] = param
			logger.Log("Info", "Inputs changed: "+param)
			changed = true
		}
	} else {
		logger.Log("Warning", "Could not extract ip from JSON response")
	}
	if param, err := extractJsonString(reply, "op"); err == nil {
		if param != config["outputs"] {
			config["outputs"] = param
			logger.Log("Info", "Outputs changed: "+param)
			changed = true
		}
	} else {
		logger.Log("Warning", "Could not extract op from JSON response")
	}

	if changed {
		writeConfig()
		initPins()
		cyclePin(ledPin, 4)
	}

	configured = true
	return nil
}

func GetConfigValueFromFile(key string, file *os.File) (string, error) {
	file.Seek(0, 0)
	scanner := bufio.NewScanner(file)

	for scanner.Scan() {
		text := scanner.Text()
		tokens := strings.Split(text, " ")

		if tokens[0][0] == '#' {
			continue
		}

		if tokens[0] == key {
			return tokens[1], nil
		}
	}
	return "", errors.New("Key not found in file")
}

func getLastActuation() (string, error) {
	if lastActuation == "" {
		return "", errors.New("No previous action to perform")
	} else {
		return lastActuation, nil
	}
}

func getDefaultBehaviour() (string, error) {
	file, err := os.Open(configFile)
	if err != nil {
		logger.Log("Error", "Error opening file for read: "+err.Error())
		return "", errors.New("Unable to get default behaviour")
	}

	defer file.Close()
	response, _ := GetConfigValueFromFile("defaultBehaviour", file)
	if response == "" {
		return "{}", nil
	}
	return response, nil
}

// get the current varSum (cached)
func GetVarSum() int {
	return varSum
}

// get the current vars
// special vars:
//   id: the ID assigned to this device by NetReceiver (always present)
//   mode: device-specific operating mode of this device, defaults to "Normal"
//   logging: the log level, one of "Error", "Warning", "Info" or "Debug"
func GetVars() (map[string]string, error) {
	var reply string
	var err error

	if reply, _, err = Send(RequestVars, nil); err != nil {
		return nil, err
	}
	logger.Log("Info", "Received vars")
	logger.Log("Info", reply)

	decoder := json.NewDecoder(strings.NewReader(reply))
	var vars map[string]string
	if err := decoder.Decode(&vars); err != nil {
		return nil, err
	}

	er, hasError := vars["er"]
	if hasError {
		return nil, errors.New(er)
	}

	id, hasID := vars["id"]
	if !hasID {
		return nil, errors.New("No ID in response")
	}

	for key, value := range vars {
		if strings.HasPrefix(key, id+".") {
			vars[key[len(id)+1:]] = value
			delete(vars, key)
		}
	}

	// check for special vars
	logging, haslogging := vars["logging"]
	if haslogging {
		logger.SetLevel(logging)
		logger.Log("Debug", "Set log level: "+logging)
	}
	_, hasMode := vars["mode"]
	if !hasMode {
		vars["mode"] = "Normal"
	}

	return vars, nil
}

//initalize is to be called on execution once and initializes GPIO pins and proccesses configuration data
func Init(hardware bool) {
	config = make(map[string]string)

	readConfig()
	logger.Log("Info", "MAC: "+config["mac"])
	logger.Log("Info", "WIFI: "+config["wifi"])
	logger.Log("Info", "DKey: "+config["dKey"])
	logger.Log("Info", "Inputs: "+config["inputs"])
	logger.Log("Info", "Outputs: "+config["outputs"])
	logger.Log("Info", "Mon Period: "+config["monPeriod"])
	logger.Log("Info", "Act Period: "+config["actPeriod"])

	if !hardware {
		logger.Log("Debug", "Initializing without hardware")
		return
	}

	if err := embd.InitGPIO(); err != nil {
		logger.Log("Warning", "Failed to initialize GPIO pins with error message: \""+err.Error()+"\".")
	}
	embd.SetDirection(ledPin, embd.Out)
	embd.DigitalWrite(ledPin, embd.Low)

	if err := embd.InitSPI(); err != nil {
		logger.Log("Error", "Unable to init SPI")
	}
	spiBus := embd.NewSPIBus(embd.SPIMode0, 0, 1000000, 0, 0)

	adc = mcp3008.New(mcp3008.SingleMode, spiBus)

	initPins()
	cyclePin(ledPin, 3)
}

//Run is called at regular intervals and drives the polling, actuating and configuration functionality
func Run() bool {
	logger.Log("Debug", "Running")

	var err error
	var reply string
	reconfig := false
	slept := 0

	if config["inputs"] == "" && config["outputs"] == "" {
		if err = GetConfig(); err != nil {
			return false
		}
	}

	if config["inputs"] != "" {
		if _, reconfig, err = Send(RequestPoll, SplitCSV(config["inputs"])); err != nil {
			logger.Log("Error", err.Error())
		}
	}
	if config["outputs"] != "" {
		if !reconfig {
			if reply, reconfig, err = Send(RequestAct, SplitCSV(config["outputs"])); err != nil {
				logger.Log("Error", err.Error())
			}
		}
		lastActuation = reply
	}

	if reconfig {
		logger.Log("Info", "Received config request")
		if GetConfig() != nil {
			return false
		}
	}

	if val, err := strconv.Atoi(config["actPeriod"]); err == nil && val >= 0 {
		logger.Log("Debug", "Active for "+strconv.Itoa(val)+"s")
		time.Sleep(time.Second * time.Duration(val))
		slept = val
	} else {
		logger.Log("Warning", "invalid actPeriod")
	}

	cyclePin(ledPin, 1)

	if val, err := strconv.Atoi(config["monPeriod"]); err == nil && val > slept {
		logger.Log("Debug", "Sleeping for "+strconv.Itoa(val-slept)+"s")
		time.Sleep(time.Second * time.Duration(val-slept))
	} else {
		logger.Log("Warning", "invalid monPeriod")
	}
	return true
}
