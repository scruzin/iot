/*
NAME
  dht-netsender - netsender client for the DHT-11 humidity and temperature sensor

DESCRIPTION
  See Readme.md

AUTHOR
  Jack Richardson <richardson.jack@outlook.com>

LICENSE
  dht-netsender is Copyright (C) 2017 the Australian Ocean Lab (AusOcean).

  It is free software: you can redistribute it and/or modify them
  under the terms of the GNU General Public License as published by the
  Free Software Foundation, either version 3 of the License, or (at your
  option) any later version.

  It is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
  for more details.

  You should have received a copy of the GNU General Public License
  along with revid in gpl.txt.  If not, see [GNU licenses](http://www.gnu.org/licenses).
*/

package main

import (
	"errors"
	"os"
	"strconv"
	"time"

	"bitbucket.org/ausocean/IoT/pi/netsender"

	"github.com/d2r2/go-dht"
)

// defaults and networking consts
const (
	retryPeriod int    = 30 // seconds
	configFile  string = "/etc/netsender.conf"
)

//client configuration
var (
	dht11Temp int
	dht11Hum  int
	dht22Temp int
	dht22Hum  int
	dhtPin    int
)

type pinReference struct {
	variable   *int
	defaultVal string
}

var defaultPins map[string]pinReference

//dhtRead reads and interprets humidity and temperature data from a DHT11 sensor
func dhtRead(pin int) (int, error) {
	var val int
	var err error

	switch {
	case pin == dht11Temp:
		if val, _, _, err = dht.ReadDHTxxWithRetry(dht.DHT11, dhtPin, false, 5); err != nil {
			return -1, errors.New("DHT Read Err: " + err.Error())
		}

	case pin == dht11Hum:
		if _, val, _, err = dht.ReadDHTxxWithRetry(dht.DHT11, dhtPin, false, 5); err != nil {
			return -1, errors.New("DHT Read Err: " + err.Error())
		}

	case pin == dht22Temp:
		if val, _, _, err = dht.ReadDHTxxWithRetry(dht.DHT22, dhtPin, false, 5); err != nil {
			return -1, errors.New("DHT Read Err: " + err.Error())
		}

	case pin == dht22Hum:
		if _, val, _, err = dht.ReadDHTxxWithRetry(dht.DHT22, dhtPin, false, 5); err != nil {
			return -1, errors.New("DHT Read Err: " + err.Error())
		}

	default:
		return -1, errors.New("External pin not defined")
	}

	return int(val), nil
}

//getClientConfig fetches the secific configutation values for the client
func getClientConfig() error {

	var file *(os.File)
	var err error

	if file, err = os.Open(configFile); err != nil {
		netsender.Log("Error", "Error opening file for read: "+err.Error())
		return err
	}

	defer file.Close()
	var val string

	for name, pinRef := range defaultPins {
		if val, err = netsender.GetConfigValue(name, file); err != nil {
			netsender.Log("Warning", name+" not specified in config file")
			val = pinRef.defaultVal
		}
		if *(pinRef.variable), err = strconv.Atoi(val); err != nil || *(pinRef.variable) < 0 {
			netsender.Log("Error", name+" not a non-negative integer")
			return err
		}
	}
	return nil
}

func main() {

	defaultPins = map[string]pinReference{
		"dht22Temp": pinReference{&dht22Temp, "50"},
		"dht11Temp": pinReference{&dht11Temp, "40"},
		"dht22Hum":  pinReference{&dht22Hum, "51"},
		"dht11Hum":  pinReference{&dht11Hum, "41"},
		"dhtPin":    pinReference{&dhtPin, "22"},
	}

	if getClientConfig() != nil {
		netsender.Log("Error", "Fatal error in client config")
		return
	}

	netsender.Init()
	netsender.ExternalReader = dhtRead

	for {
		for !netsender.Run() {
			netsender.Log("Warning", "Run Failed")
			time.Sleep(time.Duration(retryPeriod) * time.Second)
		}
	}
}
