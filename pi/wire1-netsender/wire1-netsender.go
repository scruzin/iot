/*
NAME
  wire1-netsender - netsender client for the ds18b20 temperature sensor

DESCRIPTION
  See Readme.md

AUTHOR
  Jack Richardson <richardson.jack@outlook.com>

LICENSE
  gpio-netsender is Copyright (C) 2017 the Australian Ocean Lab (AusOcean).

  It is free software: you can redistribute it and/or modify them
  under the terms of the GNU General Public License as published by the
  Free Software Foundation, either version 3 of the License, or (at your
  option) any later version.

  It is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
  for more details.

  You should have received a copy of the GNU General Public License
  along with revid in gpl.txt.  If not, see [GNU licenses](http://www.gnu.org/licenses).
*/

package main

import (
	"errors"
	"time"

	"bitbucket.org/ausocean/IoT/pi/netsender"

	"github.com/kidoman/embd"
	_ "github.com/kidoman/embd/host/all"

	"github.com/yryz/ds18b20"
)

// defaults and networking consts
const (
	retryPeriod int = 30 // seconds
)

//X-pin sensor values
const (
	ds18b20Temp = 60
)

func ds18b20Read(pin int) (int, error) {
	sensors, err := ds18b20.Sensors()
	if err != nil {
		netsender.Log("Error", "Error retrieving connected sensors: "+err.Error())
		return -1, errors.New("No sensors connected")
	}

	t, err := ds18b20.Temperature(sensors[0])
	if err == nil {
		return int(t), nil
	}

	return -1, errors.New("Unable to read temperature")
}

func main() {
	netsender.Init()

	netsender.ExternalReader = ds18b20Read
	for {
		for !netsender.Run() {
			netsender.Log("Warning", "Run Failed")
			time.Sleep(time.Duration(retryPeriod) * time.Second)
		}
	}
	embd.CloseGPIO()
}
