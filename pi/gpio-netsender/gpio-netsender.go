/*
NAME
  gpio-netsender - netsender client for general purpose sensors that require no external libraries

DESCRIPTION
  See Readme.md

AUTHOR
  Jack Richardson <richardson.jack@outlook.com>

LICENSE
  gpio-netsender is Copyright (C) 2017-2018 the Australian Ocean Lab (AusOcean).

  It is free software: you can redistribute it and/or modify them
  under the terms of the GNU General Public License as published by the
  Free Software Foundation, either version 3 of the License, or (at your
  option) any later version.

  It is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
  for more details.

  You should have received a copy of the GNU General Public License
  along with revid in gpl.txt.  If not, see [GNU licenses](http://www.gnu.org/licenses).
*/

package main

import (
	"flag"
	"time"

	"bitbucket.org/ausocean/iot/pi/netsender"
)

// defaults and networking consts
const (
	retryPeriod int = 30 // seconds
)

func main() {
	var hardware bool
	flag.BoolVar(&hardware, "Hardware", false, "Enable hardware peripherals")
	var logLevel int
	flag.IntVar(&logLevel, "LogLevel", 3, "Log level")
	flag.Parse()

	logger := netsender.GetLogger()
	logger.SetVerbosity(logLevel)
	netsender.Init(hardware)
	var vars map[string]string
	vs := netsender.GetVarSum()

	if !netsender.IsConfigured() {
		logger.Log("Info", "Configuring...")
		_ = netsender.GetConfig()
	}
	for {
		for !netsender.Run() {
			logger.Log("Warning", "Run Failed. Retrying...")
			time.Sleep(time.Duration(retryPeriod) * time.Second)
		}
		if vs != netsender.GetVarSum() {
			vars, _ = netsender.GetVars()
			vs = netsender.GetVarSum()
			if vars["mode"] == "Stop" {
				logger.Log("Info", "Received Stop mode. Stopping...")
				return
			}
		}
	}
}
