# Readme

This repository has been deprecated.

See [ausocean/iot](https://bitbucket.org/ausocean/iot) and
[ausocean/iotsvc](https://bitbucket.org/ausocean/iotsvc) instead.

# License

NetReceiver is Copyright (C) 2017-2019 Alan Noble.

It is free software: you can redistribute it and/or modify them
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

It is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License
along with NetReceiver in gpl.txt.  If not, see [GNU licenses](http://www.gnu.org/licenses).

