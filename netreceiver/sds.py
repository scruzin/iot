# Description:
#   sds.py - Software-define sensors for NetReceiver
#
#   See also http://netreceiver.appspot.com
#
# License:
#   Copyright (C) 2019 The Australian Oceal Lab (AusOcean)
#
#   This file is part of NetReceiver. NetReceiver is free software: you can
#   redistribute it and/or modify it under the terms of the GNU
#   General Public License as published by the Free Software
#   Foundation, either version 3 of the License, or (at your option)
#   any later version.
#
#   NetReceiver is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with NetReceiver in gpl.txt.  If not, see
#   <http://www.gnu.org/licenses/>.
#

import struct

# dof_d extracts degree-of-freedom binary data and computes displacement difference.
def dof_d(data):
  sz = len(data)/4 # 4 bytes per float
  sz /= 6
  samples = []
  for ii in range(sz):
    samples.append(struct.unpack("ffffff", data[ii*24: ii*24+24]))
  # ToDo: computation goes here
  return sz
