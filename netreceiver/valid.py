# Description:
#   Common NetReceiver validation functions.
#
# License:
#   Copyright (C) 2017-2018 Alan Noble.
#
#   This file is part of NetReceiver. NetReceiver is free software: you can
#   redistribute it and/or modify it under the terms of the GNU
#   General Public License as published by the Free Software
#   Foundation, either version 3 of the License, or (at your option)
#   any later version.
#
#   NetReceiver is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with NetReceiver in gpl.txt.  If not, see
#   <http://www.gnu.org/licenses/>.

import re
import socket
import datetime

from lists import CONST

def IsTrue(val):
  """Return True if value is defined and equal to 1, 'true' or 'on'."""
  if val and (val == '1' or val.lower() == 'true' or val.lower() == 'on'):
    return True
  else:
    return False

def IsNumber(val):
  """Return True if value is a number (integer or floating point)."""
  if not val: return False
  pattern = re.compile(r'[+-]?[\d\.]+$')
  return pattern.match(val)

def IsInteger(val):
  """Return True is value is a positive integer."""
  if not val: return False
  pattern = re.compile(r'[\d]+$')
  return pattern.match(val)

def IsSiteKey(num):
  """Return True is num is a site key."""
  return num >= 10**11 and num < 10**12
  
def IsEmail(email):
  """Returns True if email is a valid email, False otherwise."""
  is_email = re.compile(r'^[^@]+@[^.@]+(\.[^.@]+)+$')
  return email is not None and is_email.match(email)

def IsMacAddress(mac):
  """Returns True if mac is a valid colon-separated, non-zero mac address. NB: 2 hex digits per tuple required."""
  if not mac or mac.replace('0','') == ':::::': return False
  is_mac = re.compile(r'^[\dA-F][\dA-F]:[\dA-F][\dA-F]:[\dA-F][\dA-F]:[\dA-F][\dA-F]:[\dA-F][\dA-F]:[\dA-F][\dA-F]$')
  if not is_mac.match(mac.upper()): return False
  return True

def IsIdentifier(id, extended=False):
  """Returns True if id is a valid identifier."""
  if extended:
    is_id = re.compile(r'^([a-zA-Z0-9_-]+\.)?[a-zA-Z0-9_-]+$')
  else:
    is_id = re.compile(r'^[a-zA-Z0-9_-]+$')
  return id is not None and is_id.match(id)

def IsPin(pin, pt=CONST['pinTypes']):
  """Returns True if pin is valid pin name."""
  is_pin = re.compile(r'^[' + pt + ']\d\d?\*?$')
  return pin is not None and is_pin.match(pin.upper())

def IsExtendedPin(pin, pt=CONST['pinTypes']):
  """Returns True if pin is an extended pin name, i.e., a pin name prefixed with an identifier."""
  is_pin = re.compile(r'^[a-zA-Z0-9_-]+\.[' + pt + ']\d\d?\*?$')
  return pin is not None and is_pin.match(FixSpaces(pin).upper())

def IsPinList(pin, pt=CONST['pinTypes']):
  """Returns True if pin is one or more valid pin names."""
  is_pin = re.compile(r'^[' + pt + ']\d\d?\*?(,[' + pt + ']\d\d?\*?)*$')
  return pin is not None and is_pin.match(FixSpaces(pin).upper())

def IsIpv4(addr):
  """Return True if addr is a valid IPv4 address."""    
  try:
    socket.inet_aton(addr)
    return True
  except:
    return False

def IsIpv6(addr):
  """Return True if addr is a valid IPv6 address."""
  try:
    socket.inet_pton(socket.AF_INET6, addr)
    return True
  except:
    return False

def IsLatLng(val):
  """Return (lat, lng) pair of floats if value is a latlng, else None"""
  if not val: return None
  pattern = re.compile(r'([-\.\d]+)\s*,?\s*([-\.\d]+)')
  match = pattern.match(val)
  if not match: return None
  lat = float(match.group(1))
  lng = float(match.group(2))
  if lat >= -90 and lat <= 90 and lng >= -180 and lng <= 180:
    return (lat, lng)
  else:
    return None

def IsTime(val, tz_offset=0):
  """Return a time and the number of minutes in the day if value is a string representing a time, i.e., hh:mm or hhmm."""
  if not val: return None
  pattern = re.compile(r'(\d\d):?(\d\d)')
  match = pattern.match(val)
  if not match: return None
  hour = int(match.group(1))
  minute = int(match.group(2))
  if hour >= 0 and hour < 24 and minute >= 0 and minute < 60:
    minutes = ((hour * 60) + minute - int(tz_offset * 60)) % CONST['minsInDay']
    return datetime.time(int(minutes/60), minutes%60), minutes
  else:
    return None, 0

def FixSpaces(str):
  """Remove extraneous spaces around commas, but leave other spaces."""
  if str is None:
    return ''
  else:
    return ','.join(re.split("\s*,\s*", str.strip()))

def Each(str, delim=','):
  """Split a string into each of its delimited items, return an empty list for an empty string (unlike the split builtin)."""
  if str:
    return str.split(delim)
  else:
    return []
