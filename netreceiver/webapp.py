# Description:
#   NetReceiver web app base functionality.
#
# License:
#   Copyright (C) 2017-2018 Alan Noble.
#
#   This file is part of NetReceiver. NetReceiver is free software: you can
#   redistribute it and/or modify it under the terms of the GNU
#   General Public License as published by the Free Software
#   Foundation, either version 3 of the License, or (at your option)
#   any later version.
#
#   NetReceiver is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with NetReceiver in gpl.txt.  If not, see
#   <http://www.gnu.org/licenses/>.

import os
import urllib
import jinja2
import webapp2

from google.appengine.api import users
from google.appengine.api import mail

from lists import ERRORS

# Globals
NAV_BAR = [{'title': 'Home', 'url': '/', 'icon':'/static/home.png', 'active':False },
           {'title': 'Sites', 'url': '/sites', 'icon':'/static/sites.png', 'active':False },
           {'title': 'Monitor', 'url': '/monitor', 'icon':'/static/monitor.png', 'active':False },
           {'title': 'Settings', 'url': '/settings', 'icon':'/static/settings.png', 'active':False },
           {'title': 'Admin', 'url': '/admin', 'icon':'/static/admin.png', 'active':False },
           {'title': 'Help', 'url': '/help', 'icon':'/static/help.png', 'active':False }]
DISCLAIMER = 'NetReceiver is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the <a href="http://www.gnu.org/licenses/">GNU General Public License</a> for more details.'
JINJA_ENV = jinja2.Environment(loader=jinja2.FileSystemLoader(os.path.join(os.path.dirname(__file__), 'templates')),
                               extensions=["jinja2.ext.do"])

# Jinja2 filters
def UrlEncode(val):
  """Escape a string for a URL."""
  return urllib.quote(val)

def FirstChar(str):
  """Return the first character of a string."""
  if len(str) > 0:
    return str[0]
  else:
    return ''

def FirstWord(str):
  """Return the first word of a space-separated string."""
  parts = str.split()
  if parts and len(parts) > 0:
    return parts[0]
  else:
    return ''

def LocalTime(time, tz_offset=0.0):
  """Format time as a local time."""
  minutes = ((time.hour * 60) + time.minute + int(tz_offset * 60)) % (24 * 60)
  return "%02d:%02d" % (int(minutes/60), minutes%60)

JINJA_ENV.filters['urlencode'] = UrlEncode
JINJA_ENV.filters['firstchar'] = FirstChar
JINJA_ENV.filters['firstword'] = FirstWord
JINJA_ENV.filters['localtime'] = LocalTime

# Common web app base class
class BaseHandler(webapp2.RequestHandler):
  """Base class for handlers."""

  def __init__(self, *args, **kwargs):
    """Initialize current user, etc."""
    super(BaseHandler, self).__init__(*args, **kwargs)
    if users.get_current_user():
      self.user_email = users.get_current_user().email().lower()
      self.is_app_admin = users.is_current_user_admin()
      self.signin_url = None
      self.signout_url = users.create_logout_url(self.request.uri)
    else:
      self.user_email = None
      self.is_app_admin = False
      self.signin_url = users.create_login_url(self.request.uri)
      self.signout_url = None
     
  def Template(self, filename):
    """Return template corresponding to filename."""
    return JINJA_ENV.get_template(filename)

  def RenderTemplate(self, filename, values):
    """Renders a given template with the given dictionary of values."""
    return self.Template(filename).render(values)

  def WriteTemplate(self, filename, values, err=None):
    """Renders and outputs a given template with the dictionary of values."""
    selected = None
    url = self.request.path
    for elt in NAV_BAR:
      elt['active'] = False
      if elt['url'] != '/' and url.find(elt['url']) >= 0:
        selected = elt
    if selected is None:
      selected = NAV_BAR[0]
    selected['active'] = True
    values.update({'request':self.request, 'nav_bar':NAV_BAR, 'signout':self.signout_url, 'disclaimer':DISCLAIMER })
    if err:
      values.update({'err':ERRORS[err]})
    self.response.out.write(self.RenderTemplate(filename, values))

  def RenderAndSendEmail(self, filename, values):
    """Renders an email and sends it."""
    SendEmail(self.Template(filename), values)

# Standalone functions
def SendEmail(template, values):
  """Renders a given email template and sends it."""
  email = mail.EmailMessage(sender=values['sender'],
                            subject = values['subject'],
                            to = values['recipient'])

  if values.get('content_type') == 'text/html':
    email.html = template.render(values)
    email.body = ''
  else:
    email.body = template.render(values)
  email.send()
