# Description:
#   NetReceiver site functions.
#
# License:
#   Copyright (C) 2017-2018 Alan Noble.
#
#   This file is part of NetReceiver. NetReceiver is free software: you can
#   redistribute it and/or modify it under the terms of the GNU
#   General Public License as published by the Free Software
#   Foundation, either version 3 of the License, or (at your option)
#   any later version.
#
#   NetReceiver is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with NetReceiver in gpl.txt.  If not, see
#   <http://www.gnu.org/licenses/>.

import datetime

from google.appengine.api import users
from google.appengine.ext import ndb

import suntime

from lists import CONST, KINDS
from valid import IsSiteKey, IsTime
from device import GetDevicesBySite

# Site object
class Site(ndb.Model):
  """Site details."""
  skey = ndb.IntegerProperty()
  name = ndb.StringProperty()
  user_email = ndb.StringProperty()
  user_phone = ndb.StringProperty()
  notify_email = ndb.StringProperty()
  notify_phone = ndb.StringProperty()
  notify_period = ndb.IntegerProperty(default=CONST['notifyPeriod'])
  country = ndb.StringProperty()
  tz_offset = ndb.FloatProperty(default=0)
  latitude = ndb.FloatProperty()
  longitude = ndb.FloatProperty()
  enabled = ndb.BooleanProperty(default=False)
  confirmed = ndb.BooleanProperty(default=False)
  premium = ndb.BooleanProperty(default=False)
  created = ndb.DateTimeProperty(auto_now_add=True)

  def timeOfDay(self, val):
    """Returns the UTC time corresponding to a symbolic time of day at the given site's location and the number of minutes for the UTC day.
Sunrise and Sunset default to 06:00 and 18:00 respectively if the site's location is unknown."""
    if val == 'Midnight':
      return IsTime('0000', self.tz_offset)
    elif val == 'Noon':
      return IsTime('1200', self.tz_offset)
    elif val == 'Sunrise':
      if self.latitude and self.longitude:
        dt = suntime.Sun(self.latitude, self.longitude).get_sunrise_time()
        return datetime.time(dt.hour, dt.minute), (dt.minute + int(self.tz_offset * 60)) % CONST['minsInDay']
      else:
        return IsTime('0600', self.tz_offset)
    elif val == 'Sunset':
       if self.latitude and self.longitude:
         dt = suntime.Sun(self.latitude, self.longitude).get_sunset_time()
         return datetime.time(dt.hour, dt.minute), (dt.minute + int(self.tz_offset * 60)) % CONST['minsInDay']
       else:
         return IsTime('1800', self.tz_offset)
    elif val[0] == '*' and len(val[1:]) <= 4 and val[1:].isdigit():
      # these are repeating crons which are always UTC
      return IsTime(('0' * (4 - len(val[1:]))) + val[1:], 0)
    else:
      return None, 0

def GetOrCreateSite(skey, **kwargs):
  """Get or create a site, using the skey as the entity id."""
  def _tx():
    site = GetSite(skey)
    if site:
      return site, False
    site = Site(id=skey, skey=skey, **kwargs)
    site.put()

    return site, True
  return ndb.transaction(_tx)

def GetSite(skey):
  """Get site by site skey."""
  return Site.get_by_id(skey)

def GetSiteByName(email, name):
  """Get site by user email and site name, confirmed or otherwise, if any."""
  query = Site.query().filter(Site.user_email == email).filter(Site.name == name)
  return query.get()

def GetKindBySite(skey, kind, records=CONST['maxRecords']):
  query = ndb.gql("SELECT * from " + kind + " WHERE skey = :1", skey)
  return query.fetch(records)

def DeleteSite(skey, all=False):
  """Delete site."""
  if all:
    for dev in GetDevicesBySite(skey):
      for kind in KINDS['data']:
        query = ndb.gql("SELECT __key__ from " + kind + " WHERE mac = :1", dev.mac)
        for key in query.iter(keys_only=True):
          key.delete()
  for kind in KINDS['settings']:
    query = ndb.gql("SELECT __key__ from " + kind + " WHERE skey = :1", skey)
    for key in query.iter(keys_only=True):
      key.delete()
  key = ndb.Key('Site', skey)
  if key:
    key.delete()

# Validation
def CheckSite(skey, premium=False):
  """Return site if skey is a valid site key, else error."""
  if not skey:
    return None, 'SiteKeyRequired'
  try:
    skey = int(skey)
  except:
    return None, 'InvalidSiteKey'
  if not IsSiteKey(skey):
    return None, 'InvalidSiteKey'
  site = GetSite(skey)
  if not site:
    return None, 'SiteNotFound'
  if premium and not site.premium:
    return site, 'PremiumFeature'
  return site, None
