# Description:
#   NetReceiver device functions.
#
# License:
#   Copyright (C) 2017-2018 Alan Noble.
#
#   This file is part of NetReceiver. NetReceiver is free software: you can
#   redistribute it and/or modify it under the terms of the GNU
#   General Public License as published by the Free Software
#   Foundation, either version 3 of the License, or (at your option)
#   any later version.
#
#   NetReceiver is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with NetReceiver in gpl.txt.  If not, see
#   <http://www.gnu.org/licenses/>.

from google.appengine.ext import ndb

from lists import CONST, DEV_STATUS
from valid import IsMacAddress, IsPin, Each

# Storage and retrieval
class Device(ndb.Model):
  """Device details."""
  skey = ndb.IntegerProperty()
  dkey = ndb.IntegerProperty(default=0)
  mac = ndb.IntegerProperty()
  did = ndb.StringProperty()
  inputs = ndb.StringProperty()
  outputs = ndb.StringProperty()
  wifi = ndb.StringProperty()
  monitor_period = ndb.IntegerProperty(default=CONST['monitorPeriod'])
  act_period = ndb.IntegerProperty(default=0)
  tag = ndb.StringProperty()
  enabled = ndb.BooleanProperty(default=True)
  status = ndb.IntegerProperty(default=DEV_STATUS['configure'])
  updated = ndb.DateTimeProperty(auto_now_add=True)

  @property
  def icon(self):
    """Icon basename that corresponds to the device's status."""
    if not self.enabled:
      return "disabled"
    for key, status in DEV_STATUS.iteritems():
      if status == self.status:
        return key
    return "unknown"

def GetOrCreateDevice(mac, **kwargs):
  """Get or create a device, using the MAC address encoded as a number as the entity ID."""
  mac = MacEncode(mac)
  def _tx():
    dev = Device.get_by_id(mac)
    if dev:
      return dev, False
    dev = Device(id=mac, mac=mac, **kwargs)
    dev.put()
    return dev, True
  return ndb.transaction(_tx)

def GetDevice(mac, use_cache=True):
  """Get device by MAC address."""
  return Device.get_by_id(MacEncode(mac), use_cache=use_cache, use_memcache=use_cache)

def GetDeviceById(skey, did):
  """Get device by skey and device ID."""
  query = Device.query().filter(Device.skey == skey).filter(Device.did == did)
  return query.get()

def GetDevicesBySite(skey, records=CONST['maxDevices']):
  """Get devices by site key."""
  query = Device.query().filter(Device.skey == skey).order(Device.did)
  return query.fetch(records)

def GetPinsBySite(skey):
  """Return lists of extended pin names (DeviceID.PinName) for a site,
sorted by type.  A, B, D, X pins are groups as either input
(I) or output (O) pins, while other pins are listed separately."""
  pins = { 'I':[], 'O':[], 'V':[], 'T':[] }
  devices = GetDevicesBySite(skey)
  for dev in devices:
    for pin in Each(dev.inputs):
      if pin[0] in ['A', 'B', 'D', 'X']:
        pins['I'].append(dev.did+'.'+pin)
      else:
        pins[pin[0]].append(dev.did+'.'+pin)
    for pin in Each(dev.outputs):
      pins['O'].append(dev.did+'.'+pin)
  for list in pins.values():
    list.sort()
  return pins

def DeleteDevice(mac):
  """Delete device."""
  key = ndb.Key('Device', MacEncode(mac))
  if key:
    key.delete()

# Conversion
def MacEncode(mac):
  """Encode a MAC address as a network-endian number, e.g., 00:00:00:00:00:01 is encoded as 1."""
  hexToDec = {'0':0, '1':1, '2':2, '3':3, '4':4, '5':5, '6':6, '7':7,
              '8':8, '9':9, 'A':10, 'B':11, 'C':12, 'D':13, 'E':14, 'F':15 }
  binMac = 0
  parts = mac.upper().split(':')
  parts.reverse()
  shift = 0
  for part in parts:
    num = hexToDec[part[-1]]
    if len(part) == 2:
      num += (hexToDec[part[0]] << 4)
    binMac += (num << shift)
    shift += 8
  return binMac

def MacDecode(binMac):
  """Decode a network-endian number as a MAC address, , e.g., 1 is decoded as 00:00:00:00:00:01."""
  if not binMac: return ''
  decToHex = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F']
  parts = []
  for _ in range(6):
    parts.insert(0, decToHex[(binMac >> 4) & 0xF] + decToHex[binMac & 0xF])
    binMac >>= 8
  return ':'.join(parts)

# Validation
def CheckDevice(ma, dk=None):
  """Return device if the supplied MAC address and device key are correct, else an error."""
  if not IsMacAddress(ma):
    return None, 'InvalidMac'
  dev = GetDevice(ma)
  if not dev:
    return None, 'DeviceNotFound'
  if not dev.enabled:
    return dev, 'DeviceNotEnabled'
  if dk is not None and dev.status == DEV_STATUS['ok'] and dk != dev.dkey:
    return dev, 'InvalidDeviceKey'
  return dev, None

def CheckMacPin(ma, pn):
  """Return device if the supplied MAC address and pin are valid, else an error."""
  if not ma:
    return None, 'MacRequired'
  if not IsMacAddress(ma):
    return None, 'InvalidMac'
  if not pn:
    return None, 'PinRequired'
  if not IsPin(pn):
    return None, 'InvalidPin'
  dev = GetDevice(ma)
  if not dev:
    return None, 'DeviceNotFound'
  return dev, None

