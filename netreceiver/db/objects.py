# Description:
#   NetReceiver datastore objects and related functions (except for Site, Asp and Device).
#
# License:
#   Copyright (C) 2017-2018 Alan Noble.
#
#   This file is part of NetReceiver. NetReceiver is free software: you can
#   redistribute it and/or modify it under the terms of the GNU
#   General Public License as published by the Free Software
#   Foundation, either version 3 of the License, or (at your option)
#   any later version.
#
#   NetReceiver is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with NetReceiver in gpl.txt.  If not, see
#   <http://www.gnu.org/licenses/>.

import logging
import binascii

from google.appengine.api import users
from google.appengine.ext import ndb

from lists import CONST, X_FUNCS
from valid import IsTrue, Each
from site import Site, GetSite
from device import MacEncode, GetDevice, GetDevicesBySite

# User object
class User(ndb.Model):
  """User details for a given site.
A user associated with multiple sites will have one record per site.
NB: A user may be have admin privileges for one site but only read or write privileges for another."""
  skey = ndb.IntegerProperty()
  email = ndb.StringProperty()
  user_id = ndb.StringProperty()
  role = ndb.StringProperty()
  created = ndb.DateTimeProperty(auto_now_add=True)

def GetUsersBySite(skey, records=CONST['maxUsers']):
  """Get users for a site."""
  query = User.query().filter(User.skey == skey).order(User.created)
  return query.fetch(records)

def GetOrCreateUser(skey, email, **kwargs):
  """Get or create a user, using concatenated skey.email as the key."""
  def _tx():
    user = User.get_by_id(str(skey)+'.'+email)
    if user:
      return user, False
    user = User(id=str(skey)+'.'+email, skey=skey, email=email, **kwargs)
    user.put()
    return user, True
  return ndb.transaction(_tx)

def GetUser(skey, email):
  """Get user by email for a given site."""
  return User.get_by_id(str(skey)+'.'+email)

def GetUserRole(skey, email):
  """Get user access for a given site."""
  user = GetUser(skey, email)
  if user:
    return user.role
  else:
    return None

def GetSitesByUser(email, records=CONST['maxSites']):
  """Get sites by user."""
  query = User.query().filter(User.email == email)
  sites = []
  for key in query.iter(keys_only=True):
    skey = int(key.id().split('.')[0]) # first part of user key is skey
    site = GetSite(skey)
    if site and site.confirmed:
      sites.append(site)
  if sites:
    sites.sort(key=lambda Site: Site.name)
    return sites
  # handle site owners that are not explicit users
  query = Site.query().filter(Site.user_email == email).filter(Site.confirmed == True).order(Site.name)
  return query.fetch(records)

def DeleteUser(skey, email):
  """Delete user for a given site."""
  key = ndb.Key('User', str(skey)+'.'+email)
  if key:
    key.delete()

# Sensor object
class Sensor(ndb.Model):
  """A sensor defines the mapping between a device's pin and a physical quantity."""
  skey = ndb.IntegerProperty()
  sid = ndb.StringProperty()
  pin = ndb.StringProperty()      # input pin
  quantity = ndb.StringProperty()
  func = ndb.StringProperty(default="none")
  args = ndb.StringProperty()
  scale = ndb.FloatProperty(default=1) # deprecate
  offset = ndb.FloatProperty(default=0) # deprecate
  units = ndb.StringProperty()
  format = ndb.StringProperty()

  def transform(self, value):
    """Transform a raw value into a sensor reading."""
    # ToDo: implement custom transformations
    if self.func == 'custom':
      return value
    func = X_FUNCS[self.func]
    args = [float(arg) for arg in Each(self.args)]
    return func(value, *args)

def GetOrCreateSensor(skey, sid, **kwargs):
  """Get or create a sensor, using concatenated skey.sid as the key."""
  def _tx():
    sen = Sensor.get_by_id(str(skey)+'.'+sid)
    if sen:
      return sen, False
    sen = Sensor(id=str(skey)+'.'+sid, skey=skey, sid=sid, **kwargs)
    sen.put()
    return sen, True
  return ndb.transaction(_tx)

def GetSensor(skey, sid):
  """Get sensor by sid."""
  return Sensor.get_by_id(str(skey)+'.'+sid)

def GetSensorByPin(skey, did, pin):
  """Get sensor by device pin (extended pin)."""
  query = Sensor.query(Sensor.skey==skey).filter(Sensor.pin==did+'.'+pin)
  return query.get()

def GetSensorsBySite(skey, records=CONST['maxRecords']):
  """Get sensors by site key."""
  if skey:
    query = Sensor.query().filter(Sensor.skey == skey).order(Sensor.sid)
  else:
    query = Sensor.query()
  return query.fetch(records)

def DeleteSensor(skey, sid):
  """Delete sensor."""
  key = ndb.Key('Sensor', str(skey)+'.'+sid)
  if key:
    key.delete()

# Actuator object
class Actuator(ndb.Model):
  """An actuator writes a variable value to an output pin."""
  skey = ndb.IntegerProperty()
  aid = ndb.StringProperty()
  var = ndb.StringProperty()
  pin = ndb.StringProperty() # output pin

def GetOrCreateActuator(skey, aid, **kwargs):
  """Get or create a actuator, using concatenated skey.aid as the key."""
  def _tx():
    act = Actuator.get_by_id(str(skey)+'.'+aid)
    if act:
      return act, False
    act = Actuator(id=str(skey)+'.'+aid, skey=skey, aid=aid, **kwargs)
    act.put()
    return act, True
  return ndb.transaction(_tx)

def GetActuator(skey, aid):
  """Get actuator by aid."""
  return Actuator.get_by_id(str(skey)+'.'+aid)

def GetActuatorByPin(skey, did, pin):
  """Get actuator by device pin (extended pin)."""
  query = Actuator.query(Actuator.skey==skey).filter(Actuator.pin==did+'.'+pin)
  return query.get()

def GetActuatorsBySite(skey, records=CONST['maxRecords']):
  """Get actuators by site key."""
  query = Actuator.query(Actuator.skey==skey).order(Actuator.aid)
  return query.fetch(records)

def DeleteActuator(skey, aid):
  """Delete actuator."""
  key = ndb.Key('Actuator', str(skey)+'.'+aid)
  if key:
    key.delete()

# Trigger object
class Trigger(ndb.Model):
  """Describes a trigger, which perform an action upon a target when an comparision operation upon a sensor output is true."""
  skey = ndb.IntegerProperty()
  tid = ndb.StringProperty()
  sid = ndb.StringProperty()
  precond = ndb.StringProperty()            # preconditions, if any
  op = ndb.StringProperty()                 # comparison operation
  cmp = ndb.FloatProperty()                 # comparison argument
  action = ndb.StringProperty()             # action to be performed
  var = ndb.StringProperty()                # action variable (if any) 
  data = ndb.StringProperty()               # action data (if any)
  @property
  def id(self):
    return self.tid

  def preconds(self):
    """Evaluate trigger preconditions, returning True upon successs."""
    for var in Each(self.precond):
      if not IsTrue(GetVar(self.skey, var)):
        return False
    return True

  def execute(self, notifier):
    """Execute a trigger, returning True upon success."""
    logging.info("executing trigger: sk=%d ti=%s ta=%s" % (self.skey, self.tid, self.action))
    if self.action == 'set':
      UpdateOrCreateVar(self.skey, self.var, self.data)
    elif self.action == 'del':
      DeleteVar(self.skey, self.var)
    elif self.action == 'email' or self.action == 'sms':
      if notifier:
        return notifier(GetSite(self.skey), 'trigger', self)
    return True

def GetOrCreateTrigger(skey, tid, **kwargs):
  """Get or create a trigger, using concatenated skey.tid as the key."""
  def _tx():
    trig = Trigger.get_by_id(str(skey)+'.'+tid)
    if trig:
      return trig, False
    trig = Trigger(id=str(skey)+'.'+tid, skey=skey, tid=tid, **kwargs)
    trig.put()
    return trig, True
  return ndb.transaction(_tx)

def GetTrigger(skey, tid):
  """Get trigger by tid."""
  return Trigger.get_by_id(str(skey)+'.'+tid)

def GetTriggersBySite(skey, records=CONST['maxRecords']):
  """Get triggers by site key."""
  query = Trigger.query().filter(Trigger.skey == skey).order(-Trigger.precond).order(Trigger.tid)
  return query.fetch(records)

def GetTriggersBySensor(skey, sid, records=CONST['maxRecords']):
  """Get triggers by sensor."""
  query = Trigger.query().filter(Trigger.skey == skey).filter(Trigger.sid == sid).order(-Trigger.precond)
  return query.fetch(records)

def DeleteTrigger(skey, tid):
  """Delete trigger."""
  key = ndb.Key('Trigger', str(skey)+'.'+tid)
  if key:
    key.delete()

# Cron object
class Cron(ndb.Model):
  """Crons perform actions at specified times (to nearest minute).
Unlike triggers, crons have no precondiions."""
  skey = ndb.IntegerProperty()
  cid = ndb.StringProperty()
  time = ndb.TimeProperty()                 # action time
  tod = ndb.StringProperty(default=None)    # symbolic time of day, e.g., "Sunset", or repeating time "*30"
  repeat = ndb.BooleanProperty()            # true if the tod is a repeating time
  minutes = ndb.IntegerProperty()           # minutes since start of UTC day
  action = ndb.StringProperty()             # action to be performed
  var = ndb.StringProperty()                # action variable (if any) 
  data = ndb.StringProperty()               # action data (if any)
  enabled = ndb.BooleanProperty(default=True) 
  @property
  def id(self):
    return self.cid

  def __cmp__(self, other):
    """Compare two crons by minutes, ordering repeating crons first.""" 
    if self.repeat and not other.repeat:
      return -1
    elif not self.repeat and other.repeat:
      return 1
    if self.minutes < other.minutes:
      return -1
    elif self.minutes > other.minutes:
      return 1
    else:
      return 0
    
  def update(self, site=None):
    """Update if the cron's time is a time of day."""
    if not self.tod or self.tod[0] == '*':
      return self
    if not site:
      site = GetSite(self.skey)
    time, mins = site.timeOfDay(self.tod)
    if not time:
      return self
    self.time = time
    self.minutes = mins
    self.put()
    return self

  def execute(self, notifier=None):
    """Execute a cron job, returning True upon success."""
    logging.info("cron @ %02d:%02d: sk=%d ci=%s ca=%s" % (self.time.hour, self.time.minute, self.skey, self.cid, self.action))
    if self.action == 'set':
      UpdateOrCreateVar(self.skey, self.var, self.data)
    elif self.action == 'del':
      DeleteVar(self.skey, self.var)
    elif self.action == 'email' or self.action == 'sms':
      if notifier:
        return notifier(GetSite(self.skey), 'cron', self)
    return True

def GetOrCreateCron(skey, cid, tod, **kwargs):
  """Get or create a cron, using concatenated skey.cid as the key."""
  def _tx():
    trig = Cron.get_by_id(str(skey)+'.'+cid)
    if trig:
      return trig, False
    repeat = tod is not None and tod[0] == '*'
    trig = Cron(id=str(skey)+'.'+cid, skey=skey, cid=cid, tod=tod, repeat=repeat, **kwargs)
    trig.put()
    return trig, True
  return ndb.transaction(_tx)

def GetCron(skey, cid):
  """Get cron by cid."""
  return Cron.get_by_id(str(skey)+'.'+cid)

def GetCronsBySite(skey, tz_offset=0, records=CONST['maxRecords']):
  """Get crons by site key, ordered by minute of the day."""
  query = Cron.query(Cron.skey==skey).order(Cron.minutes)
  crons = query.fetch(records)
  if tz_offset == 0:
    return crons
  tz_minutes = int(tz_offset * 60)
  for cron in crons:
    if cron.repeat:
      continue
    cron.minutes = (cron.minutes + tz_minutes) % CONST['minsInDay']
  crons.sort()
  return crons

def GetCronsByTime(time, records=CONST['maxRecords']):
  """Get (enabled) non-repeating crons for given time, if any."""
  query = Cron.query(Cron.time==time).filter(Cron.repeat==False).filter(Cron.enabled==True).order(Cron.cid)
  return query.fetch(records)

def GetRepeatingCrons(records=CONST['maxRecords']):
  """Get (enabled) repeating crons, if any."""
  query = Cron.query(Cron.repeat==True).filter(Cron.enabled==True).order(Cron.minutes)
  return query.fetch(records)

def UpdateTimesOfDay(records=CONST['maxRecords']):
  """Update all time-of-day crons, and return the count."""
  query = Cron.query(Cron.tod!=None)
  count = 0
  for cron in query.fetch(records):
    cron.update()
    count += 1
  return count

def DeleteCron(skey, cid):
  """Delete cron."""
  key = ndb.Key('Cron', str(skey)+'.'+cid)
  if key:
    key.delete()

# Variable object
# System variables:
#   /dev.<MAC>
#   /varsum.<DeviceID>
# Example of a local variable scoped to <DeviceID>:
#  <DeviceID>.mode
class Var(ndb.Model):
  """Variables store state as strings. When variable names include a dot, the potion to the left of the dot represents the scope.
Scope is either system-wide ('/'), site global (''), or scoped to a device."""
  skey = ndb.IntegerProperty()
  scope = ndb.StringProperty(default='')
  name = ndb.StringProperty()
  value = ndb.StringProperty()
  updated = ndb.DateTimeProperty(auto_now=True)

def CreateVar(skey, name, value=None):
  """Create a variable, using concatenated skey.name as the key.
Calling with no value simply updates the last-updated time."""
  dot = name.find('.')
  if dot == -1:
    scope = ''
  else:
    scope = name[:dot]
  var = Var(id=str(skey)+'.'+name, skey=skey, name=name, scope=scope, value=value)
  var.put()
  return var

def GetVar(skey, name):
  """Get variable by skey.name."""
  return Var.get_by_id(str(skey)+'.'+name)

def UpdateOrCreateVar(skey, name, value=None):
  def _tx():
    var = GetVar(skey, name)
    if var:
      if value:
        var.value = value
      var.put()
      return var
    else:
      return CreateVar(skey, name, value)
  var = ndb.transaction(_tx)
  InvalidateVarSum(skey, name)
  return var

def GetVarsBySite(skey, scope='', records=CONST['maxRecords']):
  """Get variables by site skey, optionally limited to a given scope."""
  if scope:
    query1 = Var.query(Var.skey == skey).filter(Var.scope == '').order(Var.name)
    query2 = Var.query(Var.skey == skey).filter(Var.scope == scope).order(Var.name)
    return query1.fetch(records) + query2.fetch(records)
  else:
    query = Var.query(Var.skey == skey).order(Var.name)
    return query.fetch(records)

def GetVarsByDevice(skey, did):
  """Return device vars as a dictionary."""
  vars = GetVarsBySite(skey, did)
  dct = {}
  for var in vars:
    dot = var.name.find('.')
    if dot > 0:
      name = var.name[dot+1:]
    else:
      name = var.name
    dct.update({name: var.value})
  return dct
  
def DeleteVar(skey, name):
  """Delete variable."""
  key = ndb.Key('Var', str(skey)+'.'+name)
  if key:
    key.delete()
    InvalidateVarSum(skey, name)

# The following are for the 'varsum' system variables, CRC32 sums of strings
# consisting of variable name/value pairs, i.e., var1=val1&var2=val2&var3=val3...
# Varsums are calculated upon demand.
# A change to a global variable invalidates all varsums, whereas
# a change to a scoped variable only invalidates the varsum for that scope.
def ComputeVarSum(skey, scope):
  """Compute the var sum as CRC checksum 32-bit signed integer."""
  vars = GetVarsBySite(skey, scope)
  str = '&'.join([var.name + '=' + var.value for var in vars])
  vs = binascii.crc32(str) & 0xffffffff
  return (vs ^ 0x80000000) - 0x80000000

def GetVarSum(skey, scope):
  """Get the var sum for a given scope."""
  name = '/varsum.' + scope
  var = GetVar(skey, name)
  if not var:
    var = UpdateOrCreateVar(skey, name, str(ComputeVarSum(skey, scope)))
  return int(var.value)

def InvalidateVarSum(skey, var):
  """If the var is scoped, we delete the varsum just for that scope, else we delete all."""
  if var[0] == '/':
    return # ignore system vars
  dot = var.find('.')
  if dot == -1:
    query = Var.query(Var.scope == '/varsum')
    for key in query.iter(keys_only=True):
      key.delete()
  else:
    scope = var[:dot]
    key = ndb.Key('Var', str(skey)+'./varsum.'+scope)
    if key:
      key.delete()

# Signal object
class Signal(ndb.Model):
  """A signal stores one measured value. Values are raw pin values, which are mapped to physical quantities by the associated sensor."""
  mac = ndb.IntegerProperty()
  pin = ndb.StringProperty()
  value = ndb.IntegerProperty()
  date = ndb.DateTimeProperty(auto_now_add=True)

def CreateSignal(mac, pin, value):
  """Create a signal."""
  signal = Signal(mac=MacEncode(mac), pin=pin, value=value)
  signal.put()

def GetSignals(mac, pin, start, finish=None, records=CONST['maxRecords']):
  """Get signals for a device and pin."""
  if finish:
    query = Signal.query().filter(Signal.mac == mac).filter(Signal.pin == pin).filter(Signal.date > start).filter(Signal.date <= finish).order(Signal.date)
  else:
    query = Signal.query().filter(Signal.mac == mac).filter(Signal.pin == pin).filter(Signal.date > start).order(Signal.date)
  return query.fetch(records)

def CountSignals(mac, pin, start, finish=None):
  """Count signals for a device and pin start a date."""
  if finish:
    query = Signal.query().filter(Signal.mac == mac).filter(Signal.pin == pin).filter(Signal.date >= start).filter(Signal.date < finish)
  else:
    query = Signal.query().filter(Signal.mac == mac).filter(Signal.pin == pin).filter(Signal.date >= start)
  return query.count();

def GetLatestSignal(mac, pin):
  """Get the most recent signal for a device and pin."""
  query = Signal.query().filter(Signal.mac == mac).filter(Signal.pin == pin).order(-Signal.date)
  return query.get()

def DeleteSignals(skey, mac=None):
  """Delete signals for given site."""
  if mac:
    devs = [GetDevice(mac)]
  else:
    devs = GetDevicesBySite(skey)
  for dev in devs:
    query = Signal.query().filter(Signal.mac == dev.mac)
    for key in query.iter(keys_only=True):
      key.delete()

# Binary data
class BinaryData(ndb.Model):
  """Represents blob data, such an audio clip or text file (not necessarily binary)."""
  mac = ndb.IntegerProperty()
  pin = ndb.StringProperty()
  fmt = ndb.StringProperty()
  data = ndb.BlobProperty()
  timestamp = ndb.IntegerProperty()
  date = ndb.DateTimeProperty(auto_now_add=True)

def CreateBinaryData(mac, pin, fmt, data, ts):
  """Store binary data."""
  bd = BinaryData(mac=MacEncode(mac), pin=pin, fmt=fmt, data=data, timestamp=ts)
  bd.put()

def GetBinaryDatum(mac, pin, ts):
  """Get binary datum for a device, pin and timestamp."""
  query = BinaryData.query().filter(BinaryData.mac == mac).filter(BinaryData.pin == pin).filter(BinaryData.timestamp == ts)
  return query.get()

def GetLatestBinaryDatum(mac, pin):
  """Get the most recent signal for a device and pin."""
  query = BinaryData.query().filter(BinaryData.mac == mac).filter(BinaryData.pin == pin).order(-BinaryData.date)
  return query.get()

def GetBinaryData(mac, pin, start, finish=None, records=CONST['maxBinaryData']):
  """Get binary data for a device, pin and date range."""
  if finish:
    query = BinaryData.query().filter(BinaryData.mac == mac).filter(BinaryData.pin == pin).filter(BinaryData.date >= start).filter(BinaryData.date < finish).order(BinaryData.date)
  else:
    query = BinaryData.query().filter(BinaryData.mac == mac).filter(BinaryData.pin == pin).filter(BinaryData.date >= start)
  return query.fetch(records)

def CountBinaryData(mac, pin, start, finish):
  """Count binary data for a device, pin and date range."""
  query = BinaryData.query().filter(BinaryData.mac == mac).filter(BinaryData.pin == pin).filter(BinaryData.date >= start).filter(BinaryData.date < finish)
  return query.count();

def DeleteBinaryData(skey, mac=None):
  """Delete binary data for a given site."""
  if mac:
    devs = [GetDevice(mac)]
  else:
    devs = GetDevicesBySite(skey)
  for dev in devs:
    query = BinaryData.query().filter(BinaryData.mac == dev.mac)
    for key in query.iter(keys_only=True):
      key.delete()

# Request object
class Request(ndb.Model):
  """Records requests."""
  name = ndb.StringProperty()
  mac = ndb.IntegerProperty()
  lan = ndb.StringProperty()
  wan = ndb.StringProperty()
  status = ndb.StringProperty()
  date = ndb.DateTimeProperty(auto_now_add=True)

def CreateRequest(name, mac, lan, wan, status):
  """Log a request."""
  config = Request(name=name, mac=MacEncode(mac), lan=lan, wan=wan, status=status)
  config.put()

def GetRequest(name, mac, lan, wan, status):
  """Get last request for a given MAC/LAN/WAN/status tuple."""
  query = Request.query().filter(Request.name == name).filter(Request.mac == MacEncode(mac)).filter(Request.lan == lan).filter(Request.wan == wan).filter(Request.status == status).order(-Request.date)
  return query.get()

def GetRequests(name, wan, records=CONST['maxRequests']):
  """Get requests from a WAN."""
  query = Request.query().filter(Request.name == name).order(-Request.date)
  if wan != 'all':
    query = query.filter(Request.wan == wan)
  return query.fetch(records)

def GetLocalAddr(mac):
  """Get last-known local address for a given MAC."""
  query = Request.query().filter(Request.mac == mac).order(-Request.date)
  req = query.get()
  if req:
    return req.lan
  else:
    return None

def DeleteRequests(skey, mac=None):
  """Delete requests for a site."""
  if mac:
    devs = [GetDevice(mac)]
  else:
    devs = GetDevicesBySite(skey)
  for dev in devs:
    query = Request.query().filter(Request.mac == dev.mac)
    for key in query.iter(keys_only=True):
      key.delete()

# Clock object, used for cron job time keeping
# NB: there is only one clock and its ID is 'clock'.
class Clock(ndb.Model):
  time = ndb.TimeProperty()
  minutes = ndb.IntegerProperty() # minutes since start of day (UTC)

def GetOrCreateClock(time):
  """Get or create the clock."""
  def _tx():
    clock = Clock.get_by_id('clock')
    if clock:
      return clock, False
    minutes = ((time.hour * 60) + time.minute) % CONST['minsInDay']
    clock = Clock(id='clock', time=time, minutes=minutes)
    clock.put()
    return clock, True
  return ndb.transaction(_tx)

def UpdateClock(time):
  """Update clock time and minutes since start of day."""
  def _tx():
    clock = Clock.get_by_id('clock')
    minutes = ((time.hour * 60) + time.minute) % CONST['minsInDay']
    if clock:
      clock.time = time
      clock.minutes = minutes
    else:
      clock = Clock(id='clock', time=time, minutes=minutes)
    clock.put()
  return ndb.transaction(_tx)

# Notice object
class Notice(ndb.Model):
  skey = ndb.IntegerProperty()
  kind = ndb.StringProperty()    # 'trigger' or 'cron'
  id = ndb.StringProperty()      # trigger or cron ID
  action = ndb.StringProperty()  # email or sms
  recipient = ndb.StringProperty()
  date = ndb.DateTimeProperty(auto_now_add=True)
  
def GetLatestNotice(skey, kind, id, action, since=None):
  """Get the latest notice for given skey/kind/id/action, or none."""
  if since:
    query = Notice.query().filter(Notice.skey == skey).filter(Notice.kind == kind).filter(Notice.id == id).filter(Notice.action == action).filter(Notice.date >= since).order(-Notice.date)
  else:
    query = Notice.query().filter(Notice.skey == skey).filter(Notice.kind == kind).filter(Notice.id == id).filter(Notice.action == action).order(-Notice.date)
  return query.get()

def DeleteNotices(skey):
  """Delete notices for given key."""
  query = Notice.query(Notice.skey == skey)
  for ent_key in query.iter(keys_only=True):
    ent_key.delete()
