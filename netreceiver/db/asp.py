# Description:
#   NetReceiver application-specific password (ASP) functions.
#
# License:
#   Copyright (C) 2017-2018 Alan Noble.
#
#   This file is part of NetReceiver. NetReceiver is free software: you can
#   redistribute it and/or modify it under the terms of the GNU
#   General Public License as published by the Free Software
#   Foundation, either version 3 of the License, or (at your option)
#   any later version.
#
#   NetReceiver is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with NetReceiver in gpl.txt.  If not, see
#   <http://www.gnu.org/licenses/>.

from google.appengine.ext import ndb

from lists import CONST

# Storage and retrieval

# NB: This type is accessed by remote datastore clients.
class ASP(ndb.Model):
  """Application-specific password details."""
  key = ndb.StringProperty()
  skey = ndb.IntegerProperty()
  url = ndb.StringProperty()
  description = ndb.StringProperty()
  enabled = ndb.BooleanProperty(default=True)
  created = ndb.DateTimeProperty(auto_now_add=True)

def GetOrCreateAsp(key, **kwargs):
  """Get or create an ASP."""
  def _tx():
    asp = ASP.get_by_id(key)
    if asp:
      return asp, False
    asp = ASP(id=key, key=key, **kwargs)
    asp.put()
    return asp, True
  return ndb.transaction(_tx)

def GetAsp(key):
  """Get an ASP."""
  return ASP.get_by_id(key)

def GetAspsBySite(skey, records=CONST['maxAsps']):
  """Return ASPs for a given site."""
  query = ASP.query().filter(ASP.skey == skey).filter(ASP.enabled == True).order(ASP.created)
  return query.fetch(records)

def DisableAsp(key):
  """Disable an ASP. NB: We don't delete ASPs because we don't want to
inadvertently recycle one that was used previously."""
  asp = GetAsp(key)
  if asp:
    asp.enabled = False
    asp.put()

# Validation
def CheckAsp(key, url):
  """Return site key if key is a valid ASP, else return an error."""
  if not key:
    return None, 'KeyRequired'
  asp = GetAsp(key)
  if not asp:
    return None, 'KeyNotFound'
  if asp.url != url:
    return None, 'AccessDenied'
  return asp.skey, None
