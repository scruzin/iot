# Readme

NetReceiver is a simple cloud service for collecting and monitoring
remote sensor data ("sensing") and controlling devices ("actuating").

# See Also

* [NetReceiver Help](http://netreceiver.appspot.com/help)

# License

NetReceiver is Copyright (C) 2017-2018 Alan Noble.

It is free software: you can redistribute it and/or modify them
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

It is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License
along with NetReceiver in gpl.txt.  If not, see [GNU licenses](http://www.gnu.org/licenses).

