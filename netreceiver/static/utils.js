// common Javascript
function updateForm(formName, objName, fields) {
  var theForm = document.getElementById(formName);
  var forms = document.getElementById(objName).getElementsByTagName('form');
  for (var ii = 0; ii < forms.length; ii++) {
    var form = forms[ii];
    var checkbox = form.elements['select'];
    if (checkbox && checkbox.checked) {
      for (var fld in fields) {
        if (fields[fld] == 'input' || fields[fld] == 'select') {
          theForm.elements[fld].value = form.elements[fld].value;
        } else if (fields[fld] == 'checkbox') {
          theForm.elements[fld].value = form.elements[fld].checked;
        }
      }
      return true;
    }
  }
  alert("Nothing selected");
  return false;
}

function deleteForm(formName, objName, key) {
  var theForm = document.getElementById(formName);
  var forms = document.getElementById(objName).getElementsByTagName('form');
  for (var ii = 0; ii < forms.length; ii++) {
    var form = forms[ii];
    var checkbox = form.elements['select'];
    if (checkbox && checkbox.checked) { 
      theForm.elements[key].value = form.elements[key].value;
      return true;
    }
  }
  alert("Nothing selected");
  return false;
}

function confirmForm(msg) {
  if (confirm(msg)) {
    return true;
  } else {
    return false;
  }
}
