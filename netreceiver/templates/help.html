<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" 
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en_US" xml:lang="en_US">
<head>
  <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <title>NetReceiver - Help</title>
  <link type="text/css" rel="stylesheet" href="/static/main.css" />
</head>

<body>
  <div id="nav-bar" class="section">
    <ul id="nav-bar-items" class="tpl-nest">
      {% for elt in nav_bar %}{% if elt.active %}<li class="unit nav-active">{% if elt.icon %}<img src="{{ elt.icon }}">{% endif %}{{ elt.title }}</li>
      {% else %}<li class="unit"><a href="{{ elt.url }}{% if sk %}?sk={{ sk }}{% endif %}">{% if elt.icon %}<img src="{{ elt.icon }}">{% endif %}{{ elt.title }}</a></li>{% endif %}
      {% endfor %}
      {% if signout %}<li  class="runit"><a href="{{ signout }}">Sign out</a></li>{% endif %}
    </ul>
  </div>

  <div id="body-content">
  <div id="header"><h1>NetReceiver Help</h1></div>

  <p>
  NetReceiver is a free cloud service for collecting and monitoring
  remote sensor data ("sensing") and controlling devices ("actuating").
  NetReceiver clients are called <em>NetSenders</em>. 
  </p>

  <p>
  NetReceiver is based on the idea of keeping clients very simple and
  putting as much processing as possible into NetReceiver. Read about
  these principles <a href="/help/principles">here</a>.
  </p>

  <p>
  Read about the ESP8266 Arduino client <a href="/help/esp8266">here</a>.
  </p>
    
  <h2>Objects</h2>

  <p>
  NetReceiver defines the following objects:
  
  <ul>
  <li>Site:     a place where NetReceiver is deployed, comprising one or more devices.
  <li>Device:   a microcontroller or other computer, with one or more <a href="#pins">pins</a> associated with sensors or actuators.
  <li>Sensor:   an object associated with a pin which measures a physical property., e.g. a depth.
  <li>Actuator: an object associated with a pin which performs a physical action, e.g., a relay.
  <li>Trigger:  an action performed when a sensor condition occurs.
  <li>Cron:     an action performed at a certain time.
  <li>Signal:   a raw pin value, for a specific sensor and time.
  <li>Variable: a user-defined variable for storing state.
  <li>BinaryData: arbitrary binary data, up to 1MB in size.
  <li>MtsVideo: a video clip in MPEG-TS container format (this is an experimental premium feature)
  </ul>
  </p>

  <h2><a name="protocol"></a>Protocol</h2>
  
  <p>NetReceiver clients communicate with the service by means of HTTP
  requests which return JSON data. Devices are identified by their
  unique MAC address. Devices in turn have one or
  more <a href="#pins">pins</a>, which are either associated with
  <em>sensors</em> which convert physical measurements
  or <em>actuators</em> which perform physical actions.
  </p>

  <h3><a name="pins"></a>Pins</h3>

  <p>Despite the name, pins represent more than just physical device
  pins. In NetReceiver, pins represent different <em>data
  types</em>. <em>Scalar</em> pins represent non-negative integers,
  with -1 signifying an error. Scalar values are sent as URL encoded
  parameters in the HTTP request. <em>Vector</em> pins represent
  everything else, i.e., any value that cannot be represented as a
  single non-negative, number. Vector data is sent in the body of a
  HTTP POST request.
  </p>  

  <p>Pins may be further classified as <em>real</em>
  or <em>virtual</em>. Real pins, such as analog or digital (GPIO)
  pins, are attached to a physical device. Virtual pins, on the other
  hand, provide an additional layer of abstraction. For example, while
  a DHT22 combined temperature/humidity sensor is physically connected
  to a single GPIO pin, extracting useful data requires additional
  computation. In this case, two scalar "X" pins suffice to convey
  computed temperature and humidity values. A more complicated example
  is video where a single "V" pin represents one channel of video
  data.
  </p>

  <h4>Pin types</h4>

  <table>
    <tr>
      <th class="half">Name</th>
      <th class="full">Type</th>
      <th class="dbl">Description</th>
    </tr>
    <tr>
      <td class="half">A</td>
      <td class="full">Scalar</td>
      <td class="dbl">Analog value</td>
    </tr>
    <tr>
      <td class="half">B</td>
      <td class="full">Vector</td>
      <td class="dbl">Binary data</td>
    </tr>
    <tr>
      <td class="half">D</td>
      <td class="full">Scalar</td>
      <td class="dbl">Digital value</td>
    </tr>
    <tr>
      <td class="half">S</td>
      <td class="full">Vector</td>
      <td class="dbl">Sound (audio or sonar) data</td>
    </tr>
    <tr>
      <td class="half">T</td>
      <td class="full">Vector</td>
      <td class="dbl">Text (log or GPS) data</td>
    </tr>
    <tr>
      <td class="half">V</td>
      <td class="full">Vector</td>
      <td class="dbl">Video data</td>
    </tr>
    <tr>
      <td class="half">X</td>
      <td class="full">Scalar</td>
      <td class="dbl">Software-defined sensor value</td>
    </tr>
  </table>
  </p>

  <h4>Pin ordering</h4>

  <p>By convention, pin numbers start from 0, not 1. For example, A0
  is the first analog output (or <em>only</em> analog output in the
  case of the ESP8266), V0 is the first video channel, etc. Log data
  is typically reported as the first text output, namely T0, and GPS
  data, if present, is reported as T1.
  </p>

  <h3>Configuration</h2>

  <p>Clients obtain configuration data, i.e., the list of sensor
  and actuator pins and WiFi network info, from the service by means
  of <em>config</em> requests, which are sent at startup and
  subsequently in response to configuration changes. Clients bootstrap
  themselves by communicating initially via a WiFi Network named
  "netreceiver" with WPA password "netsender" (the default network).

  The clients identify themselves by means of their unique MAC address
  via the "ma" parameter. NetReceiver issues each client a device key,
  the "dk"parameter, which must be supplied with config
  requests. Requests for which the MAC address does not correspond to
  the device with the given device key are rejected. The exception is
  the initial config request when the client does not yet know its
  device key. Finally, clients may optionally supply their LAN IP
  address via the "la" parameter.
  </p> 

  <p>The format of <em>config</em> request is as follows:

  <pre>
  /config?ma=01:02:03:04:05:06&dk=12345678&la=1.2.3.4
  </pre>

  Whenever the user has made a configuration change, the service
  returns a reconfig "rc" parameter of 1, for example:
  
  <pre>
  {"ma": "01:02:03:04:05:06", "rc": 1} 
  </pre>

  Note that any response can include the "rc" parameter, not just
  config responses. The client then reissues a <em>config</em>
  request. Clients also issue config requests periodically if no input
  or output pins are currently defined.
  </p>

  <h4><a name="devicesettings"></a>Device settings</h4>
  <p>
  Devices are configured under <a href="/settings{% if sk %}?sk={{ sk
  }}{% endif %}">Settings</a>. The ESP8266 client is configured 100%
  this way, however the Raspberry Pi client requires some
  pre-configuration via <em>/etc/netsender.conf</em>. Specifically,
  the MAC address (<em>ma</em>) and device key (<em>dk</em>) must be
  manually specified in <em>/etc/netsender.conf</em> before the client
  can run as a NetSender client. Other parameters are updated
  automatically.
  <br>
    
  <table>
    <tr>
      <th class="half">Parameter</th>
      <th class="dbl">Description</th>
      <th class="dbl">Example</th>
    </tr>
    <tr>
      <td class="half">di</td>
      <td class="dbl">Device ID (when sent as a variable it is "id")</td>
      <td>"esp-10"</td>
    </tr>
    <tr>
      <td class="half">ma</td>
      <td class="dbl">MAC address (required in netsender.conf for Pi)</td>
      <td class="dbl">"0A:0B:0C:0D:0E:0F"</td>
    </tr>
    <tr>
      <td class="half">dk</td>
      <td class="dbl">8-digit device key (generated by NetReceiver; required in netsender.conf for Pi)</td>
      <td>01234567</td>
    </tr>
    <tr>
      <td class="half">wi</td>
      <td class="dbl">Comma-separated SSID and key</td>
      <td class="dbl">"MySSID,MyKey"</td>
    </tr>
    <tr>
      <td class="half">ip</td>
      <td class="dbl">Comma-separated input pin names</td>
      <td class="dbl">"A0,D1,X22"</td>
    </tr>
    <tr>
      <td class="half">op</td>
      <td class="dbl">Comma-separated output pin names</td>
      <td>"D2,D12,X10"</td>
    </tr>
    <tr>
      <td class="half">mp</td>
      <td class="dbl">Monitor period in seconds</td>
      <td>60</td>
    </tr>
    <tr>
      <td class="half">ap</td>
      <td class="dbl">Active period in seconds (for ESP clients only)</td>
      <td></td>
    </tr>
    <tr>
      <td class="half">tg</td>
      <td class="dbl">Software version tag (a Git tag)</td>
      <td>"v1.2"</td>
    </tr>
    <tr>
      <td class="half">rc</td>
      <td class="dbl">Reconfig code</td>
      <td>See <a href="#deviceactions">device actions</a></td>
    </tr>
    <tr>
      <td class="half">hw</td>
      <td class="dbl">Client-specific hardware specifier (not used by NetReceiver but saved by clients)</td>
      <td>"dhtPin=22"</td>
    </tr>
  </table>
  </p>
  

  <h3>Sensing</h3>
  
  <p>The <em>poll</em> method sends sensed input values from the
  client to the service. For example, if analog port 0 (A0) reads 100
  and digital port 2 (D2) reads LOW, the poll for the device with MAC
  01:02:03:04:05:06 and device key 12345678 is:

  <pre>
  /poll?ma=01:02:03:04:05:06&dk=12345678&A0=100&D2=0
  </pre>
  </p>

  <h3>Actuating</h3>

  <p>The <em>act</em> method requests values to write to output pins, e.g.:

  <pre>
  /act?ma=01:02:03:04:05:06&dk=12345678
  </pre>

  which returns JSON with pin output data, e.g., if D1 is to be set HIGH:

  <pre>
  {"ma": "01:02:03:04:05:06", "D1": 1} 
  </pre>
  </p>

  <h3>State</h3>

  <p>In addition to a site's configuration information, users may
  define <a href="#variables">variables</a> to represent
  state. The <em>vars</em> method requests such state, e.g.:

  <pre>
  /vars?ma=01:02:03:04:05:06&dk=12345678
  </pre>

  returning JSON variable name/value pairs:

  <pre>
  {"id": "MyDevice", "MyDevice.Light": "on", "MyDevice.Camera": "off", ...}
  </pre>
  </p>

  <h3><a name="deviceactions"></a>Device actions</h3>

  <p>
  Reconfig "rc" parameters can be used for more than just device
  reconfiguation, shown below with their corresponding NetReceiver
  status icon.
  </p>

  <ul class="nobullet">
  <li><img src="/static/ok.png"> 0: device is up to date (ready).
  <li><img src="/static/configure.png"> 1: reconfigure/update device.
  <li><img src="/static/reboot.png"> 2: reboot device
  <li><img src="/static/debug.png"> 3: debug device
  <li><img src="/static/upgrade.png"> 4: upgrade device software (Raspberry Pi only, not Arduino).
  </ul>

  <p>
  The status icon remains in effect until the action completes,
  when it reverts to the ready state (<img src="/static/ok.png">).
  </p>

  <p>
  Upgrading is a 3-step process as follows:
  <ol>
  <li>Update the device tag to the desired tag, e.g., from "v1.0" to "v1.1".
  <li>Once the device is ready, initiate the upgrade.
  <li>Once the device is ready, reboot the device for the upgrade to take effect.
  </ol>
  If the device remains stuck in the upgrade state, it means the upgrade has failed.
  If this happens, updating or rebooting the device will reset things.
  </p>

  <h2><a name="variables"></a>Variables</h2>

  <p>
  A NetReceiver variable is simply a string. Boolean variables are
  variables for which a value of "1", "true" or "on" is treated as
  logical <em>true</em>. A value of "0", "false" or "off" is
  logical <em>false</em>, as is a variable that is not defined.
  </p>

  <p>
  Variables can be global, i.e., <em>Var</em>, or local to a
  device, i.e., <em>Device.Var</em>. Global variables are shared
  across all devices, whereas local variables are only accessible to
  the given device.
  </p>

  <p>
  When variables are mapped onto actuator pin ervalues,
  logical <em>true</em> is sent as integer 1 and <em>false</em> as
  integer 0. Other integers are sent as is.
  </p>

  <p>
  The <em>var sum</em>, denoted "vs", is a 32-bit CRC-style checksum
  of the current variables and their values which is returned as part
  of each <em>poll</em> or <em>act</em> response. Clients are
  responsible for comparing the latest "vs" value with their cached
  value to determine if their variables are out of date. Clients then
  reissue a <em>/vars</em> request.
  </p>

  <h3><a name="specialvars"></a>Special variables</h3>

  <p>
  There are two special variables which are always included,
  namely <em>id</em> and <em>mode</em>. The former is the ID assigned
  to the device by the user (not to be confused with the MAC
  address). The latter represents the client's operating mode. Clients
  may set arbitrary values for the mode but
  conventionally <em>Normal</em> signifies the default mode
  and <em>Paused</em> signifies that the client is paused in some way
  (case sensitive). Clients should therefore check the <em>mode</em>
  value and act accordingly.
  </p>

  <p>
  While most variables flow one way from NetReceiver to the client,
  clients may communicate <em>mode</em> changes in the other
  direction. This is done by adding the <em>md</em> parameter to
  the <em>/vars</em> request. For example:

  <pre>
  /vars?ma=01:02:03:04:05:06&dk=12345678&md=Special
  </pre>

  Upon success, the <em>/vars</em> response always includes the new
  value for the mode. Also, as for other variables, changes
  to <em>mode</em> can fire triggers.
  </p>

  <h2><a name="sensors"></a>Sensors</h2>

  <p>
  Sensors transform raw pin values into useful measurements, including
  binary data. NetReceiver includes a number of built-in functions to
  make this easy. These generally take comma-separated values of
  floating-point numbers. Additional custom functions can be defined
  in code, in which case the single argument is the name of the
  function that performs the custom transformation.
  
  <table>
    <tr>
      <th class="half">Name</th>
      <th class="half">Args</th>
      <th class="full">fn(<em>x</em>)</th>
    </tr>
    <tr>
      <td class="half">none</td>
      <td class="half"></td>
      <td class="full"><em>x</em></td>
    </tr>
    <tr>
      <td class="half">scale</td>
      <td class="half">a</td>
      <td class="full">a * <em>x</em></td>
    </tr>
    <tr>
      <td class="half">linear</td>
      <td class="half">a,b</td>
      <td class="full">a * <em>x</em> + b</td>
    </tr>
    <tr>
      <td class="half">quadratic</td>
      <td class="half">a,b,c</td>
      <td class="full">a * <em>x</em><sup>2</sup> + b * <em>x</em> + c</td>
    </tr>
    <tr>
      <td class="half">custom</td>
      <td class="half">fn</td>
      <td class="full">fn(<em>x</em>)</td>
    </tr>
  <table>
  </p>
  
  <h2>Triggers and crons</h2>

  <p>
  Triggers perform actions resulting from specified sensor conditions,
  e.g., a voltage is less than a specified value.
  
  Triggers have optional preconditions, which are simply one or more
  boolean variables which must all be <em>true</em> in order for the trigger
  to fire. Triggers with preconditions are evaluated prior to those
  without preconditions (in the same order as displayed). Triggers
  executed previously can set variables which are read or modified by
  subsequent triggers.
  </p>

  <p>
  Crons perform actions based on specified times of the day. To
  perform an action for an interval of time requires one cron to start
  and another to finish.
  </p>

  <p>
  For example, one trigger or cron may set the "Alarm" variable to
  "on" (<em>true</em>) and another may set it to "off" (<em>false</em>). Further,
  other triggers may specify "Alarm" as a precondition and will only
  fire when "Alarm" is <em>true</em>.
  </p>

  <p>
  Crons may use either a numeric time such as 09:45 or a symbolic
  time, namely one of Midnight, Noon, Sunrise or Sunset. Midnight and
  Noon are calculated based on the site's timezone and Sunrise and
  Sunset are calculated based on the site's location. The current
  values for Midnight, Noon, Sunrise and Sunset are displayed on the
  <a href="/settings{% if sk %}?sk={{ sk }}{% endif %}">Settings</a>
  page. Crons may also use a repeating time, represented by asterisk
  (*) followed by 1 to 4 digits. For example, *30 means repeat every
  30 minutes, and *800 means repeat every 8 hours (not 800 minutes).
  </p>

  <h2>Notifications</h2>

  <p>NetReceiver sends notification emails
  from <b>NetReceiverService@gmail.com</b>, so ensure this address is
  not treated as spam by your email provider.

  <h2>Client</h2>
  <p>
  The client is known as <em>NetSender</em>, which is also the name of
  the client library. The default client
  is <em>gpio-netsender</em>. There is an Arduino C++ version for the
  ESP8266 microcontroller (based on the Adafruit HUZZAH ESP8266
  breakout) and a Go version for the Raspberry Pi. You may install
  multiple clients at multiple locations at your site. Note that a
  WiFi connection is required and the ESP8266 only operates in the
  2.4Ghz WiFi band. Read about the special features of the ESP8266
  Arduino client <a href="/help/esp8266">here</a>.
  </p>
  
  <h3>Setup</h3>
  <p>
  You will need to know the client device's MAC address in order to
  register it with NetReceiver. Once you have this information, go to
  your site's Settings page and create a Device object with the given
  MAC address. Also specify the WiFi network that you wish to use. The
  other parameters can be specified later.
  </p>

  <p>
  When a NetSender powers up it first tries to connect to the default
  network, which is a WPA/WPA2 WiFi network named "netreceiver" with
  the password "netsender". Any easy way to do this is to temporarily
  set up your smart phone's hotspot (tethering) this way. Do not use
  the default network as your device's permanent WiFi settings as
  it is only intended to be used as the fallback network.
  </p>

  <p>
  If a NetSender cannot connect to the last saved network it will
  attempt to connect to the default network and will continue to do so
  indefinitely.
  </p>

  <p>
  Note that NetSender does <en>not</em> require a WiFi connection that
  is either permanent or 100% reliable. NetSender will keep trying
  to reconnect until it is successfully able to connect.
  </p>

  <h3>Monitor Period</h3>

  <p>The <em>monitor period</em> for a device is the number of seconds
  between monitor/actuation cycles.
  </p>
  
  <h3>Extending the Client</h3>

  <p>The NetSender library and default
  client, <em>gpio-netsender</em>, has support for reading values from
  analog (A) and GPIO digital (D) pins. For sensors that require more
  work than simply reading a pin, e.g., reading a data sequence,
  counting interrupts or a software-defined sensor, you'll need a
  custom client. NetReceiver makes this possible with the concept
  of <em>external pins</em> prefixed with an "X", i.e., "X0", "X1",
  etc. You simply map these virtual pins to your custom
  functions. NetReceiver treats external pins like regular pins,
  allowing you to define sensors and triggers, etc. See dht-netsender
  and weather-netsender for examples.
  </p>

  <h4><a name="externalpins"></a>External pins</h4>

  <p>External pins are "X" followed by 2 or 3 digits. The first digit
  of a 2-digit pin (or first two digits of a 3-digit pin) denotes the
  device <em>type</em> and last digit denotes
  the <em>quantity</em>. No more than 10 quantities can be assigned to
  a single device type, however a second type can be assigned to the
  same physical device if more than 10 quantitites are required. While
  digits representing devices are unique, digits representing
  quantities are not. For example, temperature may be quantity #0 for
  one type of temperature sensor or quantity #2 for a different type
  of sensor.</p>

  <h4>Device type assignments</h4>
  <p>
  <table>
    <tr>
      <th class="half">Device</th>
      <th class="half">Type</th>
    </tr>
    <tr>
      <td class="half">ESP</td>
      <td class="half">1</td>
    </tr>
    <tr>
      <td class="half">Pi</td>
      <td class="half">2</td>
    </tr>
    <tr>
      <td class="half">SDLWR</td>
      <td class="half">3</td>
    </tr>
    <tr>
      <td class="half">DHT11</td>
      <td class="half">4</td>
    </tr>
    <tr>
      <td class="half">DHT22</td>
      <td class="half">5</td>
    </tr>
    <tr>
      <td class="half">DS18B20</td>
      <td class="half">6</td>
    </tr>
  <table>
  </p>

  <h4>External pin assignments</h4>
  <p>
  <table>
    <tr>
      <th class="half">Device</th>
      <th class="half">Number</th>
      <th class="full">Input quantity</th>
      <th class="full">Output quantity</th>
    </tr>
    <tr>
      <td class="half">Custom</th>
      <td class="half">00 ~ 09</th>
      <td class="full">Not assigned</th>
      <td class="full">Not assigned</th>
    </tr>
    <tr>
    <tr>
      <td class="half">ESP</th>
      <td class="half">10</th>
      <td class="full">Analog (A0) value</th>
      <td class="full">Suppress pulse (bool)</th>
    </tr>
    <tr>
      <td class="half">ESP</th>
      <td class="half">11</th>
      <td class="full">Alarm (bool)</th>
      <td class="full"></th>
    </tr>
    <tr>
      <td class="half">ESP</th>
      <td class="half">12</th>
      <td class="full">Alarm count</th>
      <td class="full"></th>
    </tr>
    <tr>
      <td class="half">ESP</th>
      <td class="half">13</th>
      <td class="full">Boot reason</th>
      <td class="full"></th>
    </tr>
    <tr>
      <td class="half">Pi</th>
      <td class="half">20</th>
      <td class="full">CPU temperature</th>
      <td class="full"></th>
    </tr>
    <tr>
      <td class="half">Pi</th>
      <td class="half">21</th>
      <td class="full">CPU utilization</th>
      <td class="full"></th>
    </tr>
    <tr>
      <td class="half">Pi</th>
      <td class="half">22</th>
      <td class="full">Virtual memory</th>
      <td class="full"></th>
    </tr>
    <tr>
      <td class="half">SDLWR</th>
      <td class="half">30</th>
      <td class="full">Wind speed</th>
      <td class="full"></th>
    </tr>
    <tr>
      <td class="half">SDLWR</th>
      <td class="half">31</th>
      <td class="full">Wind gust speed</th>
      <td class="full"></th>
    </tr>
    <tr>
      <td class="half">SDLWR</th>
      <td class="half">32</th>
      <td class="full">Wind direction</th>
      <td class="full"></th>
    </tr>
    <tr>
      <td class="half">SDLWR</th>
      <td class="half">33</th>
      <td class="full">Precipitation</th>
      <td class="full"></th>
    </tr>
    <tr>
      <td class="half">DHT11</th>
      <td class="half">40</th>
      <td class="full">Temperature</th>
      <td class="full"></th>
    </tr>
    <tr>
      <td class="half">DHT11</th>
      <td class="half">41</th>
      <td class="full">Humidity</th>
      <td class="full"></th>
    </tr>
    <tr>
      <td class="half">DHT22</th>
      <td class="half">50</th>
      <td class="half">Temperature</th>
      <td class="full"></th>
    </tr>
    <tr>
      <td class="half">DHT22</th>
      <td class="half">51</th>
      <td class="half">Humidity</th>
      <td class="full"></th>
    </tr>
    <tr>
      <td class="half">DS18B20</th>
      <td class="half">60</th>
      <td class="half">Temperature</th>
      <td class="full"></th>
    </tr>
  </table>
  </p>

  <h2>Attributes and codes</h2>

  <p>
  The following attribute codes are used in HTTP params and JSON replies:

  <ul>
  <li>sk: site key
  <li>sn: site name
  <li>oe: owner email
  <li>op: owner phone
  <li>cc: country code
  <li>tz: timezone offset from UTC
  <li>ue: user email
  <li>ur: user role (read, write or admin)
  <li>np: notify period
  <li>mp: monitor period (seconds between monitor/actuation cycles)
  <li>di: device ID
  <li>dk: device key
  <li>ma: MAC address
  <li>ip: input pin(s), comma separated (e.g., "A0,D1,X2")
  <li>op: output pin(s), comma separated (e.g., "D2")
  <li>wi: WiFi network info, comma separated (e.g., "ssid,key")
  <li>tg: device tag (typically a Git tag)
  <li>si: sensor ID
  <li>sp: sensor (input) pin
  <li>sq: sensor quantity
  <li>sx: sensor transformation (xform) function
  <li>sa: sensor arguments
  <li>su: sensor units
  <li>sf: sensor formatting function
  <li>ai: actuator ID
  <li>ap: actuator (output) pin or active period
  <li>av: actuator variable
  <li>ti: trigger ID
  <li>tp: trigger precondition (comma separated variable names)
  <li>ts: trigger subject (sensor ID)
  <li>to: trigger operation
  <li>tc: trigger comparate or trigger count
  <li>ta: trigger action
  <li>tv: trigger variable
  <li>td: trigger data
  <li>ci: cron ID
  <li>ct: cron time
  <li>ca: cron action
  <li>cv: cron variable
  <li>cd: cron data
  <li>ce: cron enabled
  <li>ei: endpoint ID
  <li>es: endpoint sensor ID
  <li>ea: endpoint address (IPv4 or IPv6 address)
  <li>ep: endpoint port
  <li>ex: endpoint protocol (udp or rtp)
  <li>et: endpoint timeout in seconds
  <li>ee: endpoint enabled
  <li>vn: version
  <li>er: error (results in a client-side netsender.ServerError)
  </ul>
  </p>

  <h2>Experimental methods</h2>

  <h3>Video</h3>

  <p>NetReceiver has experimental support for receiving, storing and
  forwarding video as MPEG Transport Stream (MPEG-TS or MTS). This
  must be enabled by the NetReceiver administrator.</p>

  <p>Video in the form of short clips (typically 1 second) is uploaded
  as <em>recv</em> method POST data with the video size encoded as
  video ("V") query params, starting with V0. Multiple videos
  concatenated in a single body must be in the same order as the video
  IDs in query params. Therefore V0=52640&V1=64672 represents a video
  of 52640 bytes in size followed by a video of 64672 bytes. The full
  URL is as follows:

  <pre>
  /recv?ma=01:02:03:04:05:06&dk=12345678&V0=52640&V1=64672
  </pre>
  </p>

  <p>In addition to being stored, MPEG-TS video can be forwarded to
  endpoints as either raw UDP or RTP. Endpoints, which are configured
  in the <em>Endpoints</em> section of the settings page, must have
  globally public IP addresses in order to be visible to App
  Engine. Video will be streamed to an endpoint from the time it is
  enabled until the specified timeout, at which point the endpoint
  will be automatically disabled. Note that endpoints cannot request
  streaming; all control is by NetReceiver.
  </p>

  <h3>Other binary and text data</h3>

  <p>Other binary and text data may also be sent to NetReceiver using
  the <em>recv</em> method, using pin types <em>B</em> and <em>T</em>
  respectively. Binary data can be used for audio.
  </p>

  <h3>Retrieving video, audio and text data</h3>

  <p>Recent data can be downloaded using the
  <em>video</em>, <em>audio</em> and <em>text</em> methods
  respectively. For example, to download video V0 originating from a
  device named DEV:

  <pre>
  /video/KEY/DEV.V0
  </pre>

  KEY is either the site key or an application-specific
  password granted by the site's administrator.
  </p>

  <p>
  Here is another example for text data for a specific date/time range:

  <pre>
  /text/KEY/DEV.T0?ds=START&df=FINISH
  </pre>

  or for a duration from a start time:

  <pre>
  /text/KEY/DEV.T0?ds=START&dd=DURATION&du=UNITS
  </pre>

  START and FINISH are times are ISO-format date/time strings, i.e.,
  "YYYY-MM-DD HH:MM", or a time string "HH:MM", DURATION is a number
  and UNITS is one of <em>minutes</em> (default), <em>hours</em>
  or <em>days</em>.
  </p>

  <h3>Vidgrind and Vidrecord</h3>

 <p>Vidgrind and Vidrecord are experimental servers written in Go that implement video capture and processing.
 These can be found under the <a href="https://bitbucket.org/ausocean/iotsvc" target="_new">ausocean/iotsvc</a> repository.
 </p>

  <h2>License</h2>
  <p>
  NetReceiver is Copyright &copy; 2017-2019 Alan Noble.
  </p>

  <p>
  NetReceiver is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  </p>

  <p>
  NetReceiver is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
  the <a href="http://www.gnu.org/licenses/">GNU General Public
  License</a> for details.
  </p>

  </div><!-- body-content -->

  <div id="footer-content">
  <p><em>{{ disclaimer }}</em></p>
</body>
</html>
