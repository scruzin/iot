Hello,

Your site "{{ site.name }}" has run the following cron job:

{{ cron.cid }}

Manage settings for all devices at this site:
http://netreceiver.appspot.com/settings?sk={{ site.skey }}

Unsubscribe from future notifications for this site with just one click:
http://netreceiver.appspot.com/admin/site/unsubscribe?&sk={{ site.skey }}

Thanks for using NetReceiver!

The NetReceiver Team
