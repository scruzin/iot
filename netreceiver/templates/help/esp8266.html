<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" 
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en_US" xml:lang="en_US">
<head>
  <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <title>NetReceiver - ESP8266 Help</title>
  <link type="text/css" rel="stylesheet" href="/static/main.css" />
</head>

<body>
  <div id="nav-bar" class="section">
    <ul id="nav-bar-items" class="tpl-nest">
      {% for elt in nav_bar %}{% if elt.active %}<li class="unit nav-active">{% if elt.icon %}<img src="{{ elt.icon }}">{% endif %}{{ elt.title }}</li>
      {% else %}<li class="unit"><a href="{{ elt.url }}{% if sk %}?sk={{ sk }}{% endif %}">{% if elt.icon %}<img src="{{ elt.icon }}">{% endif %}{{ elt.title }}</a></li>{% endif %}
      {% endfor %}
      {% if signout %}<li  class="runit"><a href="{{ signout }}">Sign out</a></li>{% endif %}
    </ul>
  </div>

  <div id="body-content">
  <div id="header"><h1>NetReceiver ESP8266 Help</h1></div>
    
  <p>
  This page describes the special features of the ESP8266 Arduino
  client. Please read the <a href="/help">general help</a> first.
  </p>

  <h2>Monitor Period vs. Active Period</h2>

  <p>
  The <em>monitor period</em> for a device is the number of seconds
  between monitor/actuation cycles. For ESP8266 clients,
  the <em>active period</em> is the number seconds that the device is
  not deep sleeping. In stateless monitoring applications an ESP8266
  client can wake up from a deep sleep, measure inputs and go straight
  back go back to deep sleep. An active period is required however
  when actuating pins or handling interrupts. If the <em>active
  period</em> equals the <em>monitor period</em> then the device never
  enters deep sleep.
  </p>

  <h3>Wiring for deep sleep</h3>

  <p>
  If you plan to utilize the ESP8266's deep sleep feature,
  you <em>must</em> connect GPIO pin 16 to the RST pin. This ensures
  that the wake-up signal on GPIO 16 resets the device. If you fail to
  do so the ESP8266 will never return from its first deep sleep.
  </p> 

  <h2>Blue LED</h2>
  
  <p>
  By default, the ESP8266 Arduino client uses the blue LED (GPIO pin
  2) to signal various conditions as follows:
  </p>
  
  <ul>
  <li>1 flash signifies that a monitor cycle completed successfully.
  <li>2 flashes signifies a config error. Check your site and device are enabled and the device key is correct. 
  <li>3 flashes signifies a WiFi connection failure. Check your WiFi SSID and key are correct, or enable the default network.
  <li>4 flashes signifies that the configuration or variables changed and was saved to EEPROM.
  <li>5 flashes signifies that the ESP8266 has triggered a low-voltage alarm.
  <li>6 flashes signifies that the ESP8266 is about to restart.
  </ul>

  <h2>Variables</h2>

  <p>
  The ESP8266 Arduino client implements a number of variables which
  are used to control pulse mode and alarm mode described below. As
  for configuration data, variables are saved to the EEPROM. Therefore
  once configured, the functions these variables control do not
  require network connectivity to operate. Note that variable names
  are case sensitive.

  <table>
    <tr>
      <th class="full">Variable</th>
      <th class="dbl">Description</th>
    </tr>
    <tr>
      <td class="half">Pulses</td>
      <td class="dbl"># of pulses (required for pulse mode).</td>
    </tr>
    <tr>
      <td class="half">PulsePeriod</td>
      <td class="dbl">Pulse period in milliseconds<br>(required for pulse mode).</td>
    </tr>
    <tr>
      <td class="half">PulseCycle</td>
      <td class="dbl">Cycle (repeat) period in seconds (<em>monitor period</em> by default).</td>
    </tr>
    <tr>
      <td class="half">PulseDutyCycle</td>
      <td class="dbl">Pulse duty cycle (50% by default).</td>
    </tr>
    <tr>
      <td class="half">AlarmPin</td>
      <td class="dbl">GPIO pin to drive during an alarm (GPIO 0 by default).</td>
    </tr>
    <tr>
      <td class="half">AlarmLevel</td>
      <td class="dbl">Logical level during alarm condition (LOW by default).</td>
    </tr>
    <tr>
      <td class="half">AlarmPeriod</td>
      <td class="dbl">Alarm period for network alarms, or 0 for continuous alarm (milliseconds).</td>
    </tr>
    <tr>
      <td class="half">AlarmNetwork</td>
      <td class="dbl">Raise alarm when the number of network failures exceeds this number (0 to disable).</td>
    </tr>
    <tr>
      <td class="half">AlarmVoltage</td>
      <td class="dbl">Raise alarm when the analog (A0) value drops below this value
      (number between 1 and 1023, or 0 to disable).</td>
    </tr>
    <tr>
      <td class="half">AlarmRecoveryVoltage</td>
      <td class="dbl">Clear the voltage alarm when the analog (A0)
      value exceeds this value (number between 1 and 1023, or 0 to
      use the AlarmVoltage).</td>
    </tr>
    <tr>
      <td class="half">AlarmRestart</td>
      <td class="dbl">Restart device if the alarm continues for too long (minutes).</td>
    </tr>
    <tr>
      <td class="half">SensePin</td>
      <td class="dbl">GPIO pin that is driven HIGH while sensing pins,
	otherwise held LOW.  Used to drive a relay which regulates
	power to one or more sensors or a voltage divider (0 to
	disable).</td>
    </tr>
  </table>
  </p>
  
  <h2>Pulse mode</h2>

  <p>
  Pulse mode enables the ESP8266 Arduino client to drive the blue LED
  and GPIO pin 2. The shape of the pulse is controlled by the Pulses,
  PulsePeriod, PulseCycle and PulseDutyCycle variables as shown in the
  diagram below.
  </p>

  <img src="/static/pulse-mode.png" alt="Pulse mode">

  <p>
  Pulse mode can be <em>temporarily</em> suppressed by actuating the
  X10 custom pin. When X10 is present and <em>true</em>, the pulse is
  suppressed. X10 is <b>not</b> stored in EEPROM to avoid permanently
  suppressing the pulse due to loss of network. To permanently disable
  pulsing, either delete the Pulses and PulsePeriod variables or set
  them to zero.
  </p>

  <h3>Pulse duty cycle</h3>

  <p>
  
  <ul>
  <li>When the PulseDutyCycle variable is between 0 and 100, the
  quiescent level is LOW and pulses are from LOW to HIGH (as shown
  above). By default the duty cycle is 50%.
    
  <li>When the PulseDutyCycle variable is greater than 100 and less
  than 200, the duty cycle is the value minus 100 and the quiescent
  level is HIGH. Pulses are instead from HIGH to LOW. For example, a
  variable value of 150 results in a duty cycle of 50% with HIGH to
  LOW pulses. Note that the built-in blue LED is driven from HIGH to
  LOW.
  </ul>
  </p>
    
  <h2><a name="alarms"></a>Alarm mode</h2>

  <p>
  Alarm mode enables the ESP8266 Arduino client to drive a specified
  GPIO pin whenever the number of consecutive network failures exceeds
  a given threshold (AlarmNetwork) or the analog (A0) value drops
  below a given value (AlarmVoltage). Since the default AlarmPin is
  GPIO 0 and the default AlarmLevel is LOW, this means the ESP8266's
  red LED will be lit whenever there is an alarm. The alarm pin can
  also be connected to a power supply relay in order to power cycle
  network equipment (for AlarmPeriod milliseconds).
  </p>

  <h3>Network alarm</h3>

  <p>
  A network alarm is raised whenever the number of consecutive network
  failures exceeds the threshold specified by AlarmNetwork. This alarm
  is active only for AlarmPeriod milliseconds, and is intended to be
  used to reset network equipment.
  </p>
    
  <h3>Voltage alarm</h3>

  <p>
  The voltage alarm is intended to prevent a battery-powered system
  from completely losing power. Whereas network alarms are transient
  (i.e., for AlarmPeriod milliseconds), voltage alarms persist until
  the voltage recovers (i.e., exceeds AlarmRecoveryVoltage). Therefore
  if the voltage alarm is used to turn off power to network equipment,
  the ESP8266 will be disconnected from the network until the alarm is
  cleared. If AlarmRecoveryVoltage is not specified, it defaults to
  AlarmVoltage. Needless to say, ensure that the ESP8266 is not
  powered by the relay!
  </p>

  <p>
  If you accidentally set AlarmVoltage or AlarmRecoveyVoltage too
  high, the ESP8266 will stay in alarm mode forever (indicated by 5
  LED flashes). As a precaution, the voltage alarm is only enabled
  after the device has been updated at least once after starting. If
  the ESP8266 is stuck in alarm mode, simply reduce the analog values,
  restart the ESP8266, wait one full cycle for the new variables to be
  fetched from NetReceiver and then select Update. Once updated the
  new values for AlarmVoltage and AlarmRecoveyVoltage will take
  effect.
  </p>

  <p>
  Despite their names, AlarmVoltage and AlarmRecoveryVoltage are
  analog (A0) values, not voltages. This is because NetReceiver
  sensors convert analog values to a voltages, not NetSender. To find
  suitable analog values first include X10 in the input pin list. This
  reports the current analog (A0) value. A value of -1 means the
  device is not configured and the voltage check is disabled. In the
  latter case, first set AlarmVoltage to 1 then update the device.
  The voltage scale is found by dividing the analog value by the
  measured voltage. For example, if the current battery voltage is
  12.5V and the analog value is 856, the conversion scale is 68.48 (=
  856/12.5). Use the scale to calculate appropriate values for
  AlarmVoltage and AlarmRecoveryVoltage (examples shown below).

  <table>
    <tr>
      <th class="full">Description</th>
      <th class="full">Analog value</th>
      <th class="full">Voltage (12V system)</th>
      <th class="full">Voltage (24V system)</th>
    </tr>
    <tr>
      <td class="full">Normal</td>
      <td class="full">856</td>
      <td class="full">12.5 V</td>
      <td class="full">25.0 V</td>
    </tr>
   <tr>
      <td class="full">AlarmRecoveryVoltage</td>
      <td class="full">835</td>
      <td class="full">12.2 V</td>
      <td class="full">24.4 V</td>
    </tr>
   <tr>
      <td class="full">AlarmVoltage</td>
      <td class="full">822</td>
      <td class="full">12.0 V</td>
      <td class="full">24.0 V</td>
    </tr>
  </table>
  </p>

  <p>
  NB: A typical lead-acid battery that is fully-charged at 12.6V will
  be almost completely discharged at 11.9V. Don't wait till then!
  </p>

  <h3>Forcing an alarm</h3>

  <p>
  The alarm pin can also be actuated just like any other GPIO pin,
  making it possible to force an alarm condition remotely. To do this
  perform the following steps:

  <ul>
  <li>Temporarily set AlarmRestart to 5 minutes.
  <li>Add the alarm pin to the device's list of output pins, e.g., D0.
  <li>Create an actuator for the given device pin using the variable ForceAlarm.
  <li>Set ForceAlarm to true.
  <li>The device status should change to red within 2 minutes.
  <li>Delete the ForceAlarm variable.
  <li>The device status should revert to green within a few minutes.
  <li>Restore AlarmRestart to its original value (or 0 to disable).
  </ul>
  </p>

  <h2>Serial output</h2>

  <p>
  The ESP8266 client writes serial output at 115200 Baud. When the
  device is unconfigured it outputs the NetSender version followed the
  device's MAC address at the start of each cycle. Once the device is
  configured it runs silently, unless put into debug mode. Updating
  the device returns the device to normal mode.
  </p>

  <h2>License</h2>
  <p>
  NetReceiver is Copyright &copy; 2017-2019 Alan Noble.
  </p>

  <p>
  NetReceiver is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  </p>

  <p>
  NetReceiver is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
  the <a href="http://www.gnu.org/licenses/">GNU General Public
  License</a> for details.
  </p>

  </div><!-- body-content -->

  <div id="footer-content">
  <p><em>{{ disclaimer }}</em></p>
</body>
</html>
