# Description:
#   NetReceiver video and streaming functions.
#
# License:
#   Copyright (C) 2017-2018 Alan Noble.
#
#   This file is part of NetReceiver. NetReceiver is free software: you can
#   redistribute it and/or modify it under the terms of the GNU
#   General Public License as published by the Free Software
#   Foundation, either version 3 of the License, or (at your option)
#   any later version.
#
#   NetReceiver is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with NetReceiver in gpl.txt.  If not, see
#   <http://www.gnu.org/licenses/>.

import logging
import datetime
import socket
import json

from google.appengine.ext import ndb

from lists import CONST, DEV_STATUS
from valid import IsInteger, IsExtendedPin, IsIpv6, Each
from webapp import BaseHandler
import db

# Non-persistent endpoint state
ENDPOINTS = {} 

class EndpointState:
  """Transient state info for UDP/RTP communications."""
  def __init__(self):
    self.sock = None
    self.seq_num = random.randrange(0, 2**16)

  def init(self, address):
    """Create a socket for a given IP address."""
    if IsIpv6(address):
      self.sock = socket.socket(socket.AF_INET6, socket.SOCK_DGRAM)
    else:
      self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    logging.debug("Created socket for address %s" % address)

# Storage and retrieval
class MtsVideo(ndb.Model):
  """Represents a video clip in MPEG-TS format (typically 1-second duration)."""
  mac = ndb.IntegerProperty()
  vid = ndb.StringProperty()
  clip = ndb.BlobProperty()
  date = ndb.DateTimeProperty(auto_now_add=True)

def CreateMtsVideo(mac, vid, clip):
  """Store MPEG-TS video clip."""
  video = MtsVideo(mac=db.MacEncode(mac), vid=vid, clip=clip)
  video.put()

def GetMtsVideos(mac, vid, records=CONST['maxMtsVideo']):
  """Get MPEG-TS video for a given mac and vid in reverse time order."""
  query = MtsVideo.query().filter(MtsVideo.mac == db.MacEncode(mac)).filter(MtsVideo.vid == vid).order(-MtsVideo.date)
  return query.fetch(records)

def DeleteMtsVideos(skey, mac=None):
  """Delete MPEG-TS video for a given site."""
  if mac:
    devs = [db.GetDevice(mac)]
  else:
    devs = db.GetDevicesBySite(skey)
  for dev in devs:
    query = MtsVideo.query().filter(MtsVideo.mac == dev.mac)
    for key in query.iter(keys_only=True):
      key.delete()

class Endpoint(ndb.Model):
  """An endpoint is a communications destination for a stream.
Currently it is used just for video streams over UDP or RTP but in future it could be any app which receives UDP messages."""
  skey = ndb.IntegerProperty()
  eid = ndb.StringProperty()
  sid = ndb.StringProperty() # stream ID is an extended pin ID, e.g., DeviceId.V0
  address = ndb.StringProperty()
  port = ndb.IntegerProperty()
  proto = ndb.StringProperty()
  timeout = ndb.IntegerProperty()
  enabled = ndb.BooleanProperty(default=True)
  updated = ndb.DateTimeProperty(auto_now_add=True)
  started = ndb.DateTimeProperty(default=None)
  
  @property
  def state(self):
    """Transient state info for this endpoint."""
    state = ENDPOINTS.get(self.eid)
    if state is None:
      state = EndpointState()
      ENDPOINTS[self.eid] = state
      state.init(self.address)
    return state

  def send(self, msg, size=CONST['udpSize']):
    """Send a message in size chunks to an endpoint according to its protocol.
If sending fails, re-initialize and continue without retrying."""
    count = int(len(msg) / size)
    offset = 0
    
    if self.proto == 'udp':
      for _ in range(count):
        try:
          self.state.sock.sendto(msg[offset:offset+size], (self.address, self.port))
          offset += size
        except:
          self.state.init(self.address)

    elif self.proto == 'rtp':
      rtp_header = bytearray(12) # 12-byte RTP header
      rtp_header[0] = 0x80       # version = 2
      rtp_header[1] = 33         # payload type = 33 indicates MPEG-TS (MP2T)
      rtp_header[8] = 0          # Synchronization Source Identifer (SSRC) = 1
      rtp_header[9] = 0
      rtp_header[10] = 0
      rtp_header[11] = 1

      for _ in range(count):
        timestamp = int(time.time() * 1000) # milliseconds
        rtp_header[2] = (self.state.seq_num >> 8) & 0xff
        rtp_header[3] = self.state.seq_num & 0xff
        rtp_header[4] = (timestamp >> 24) & 0xff
        rtp_header[5] = (timestamp >> 16) & 0xff
        rtp_header[6] = (timestamp >> 8) & 0xff
        rtp_header[7] = timestamp & 0xff
        try:
          self.state.sock.sendto(str(rtp_header + msg[offset:offset+size]), (self.address, self.port))
          self.state.seq_num += 1
          offset += size
        except:
          self.state.init(self.address)

    elif self.proto == 'log':
      logging.info("Streaming %s to endpoint %s (%d bytes, %d packets)" % (self.sid, self.eid, len(msg), count))

  def forward(self, msg):
    """Send a message if the endpoint is enabled and not timed out. 
Auto disable endpoints that have timed out."""
    if not self.enabled:
      return  
    now = datetime.datetime.now()
    if self.started is None:
      self.started = now
      self.put()
    elif now - self.started < datetime.timedelta(minutes=self.timeout):
      self.send(msg)
    else:
      self.enabled = False
      self.started = None
      self.put()
      
def GetOrCreateEndpoint(skey, eid, **kwargs):
  """Get or create an endpoint, using concatenated skey.eid as the key."""
  def _tx():
    ep = Endpoint.get_by_id(str(skey)+'.'+eid)
    if ep:
      return ep, False
    ep = Endpoint(id=str(skey)+'.'+eid, skey=skey, eid=eid, **kwargs)
    ep.put()
    return ep, True
  return ndb.transaction(_tx)

def GetEndpoint(skey, eid):
  """Get endpoint its ID."""
  return Endpoint.get_by_id(str(skey)+'.'+eid)

def GetEndpointsByStream(skey, sid, records=CONST['maxEndpoints']):
  """Get endpoints by stream ID."""
  query = Endpoint.query(Endpoint.skey==skey).filter(Endpoint.sid==sid)
  return query.fetch(records)

def GetEndpointsBySite(skey, records=CONST['maxEndpoints']):
  """Get endpoints by site key."""
  query = Endpoint.query(Endpoint.skey==skey).order(Endpoint.eid)
  return query.fetch(records)

def DeleteEndpoint(skey, eid):
  """Delete endpoint."""
  key = ndb.Key('Endpoint', str(skey)+'.'+eid)
  if key:
    key.delete()

# Capture
class RecvVideoHandler(BaseHandler):
  """Receive video from devices."""
  def get(self):
    self.post()

  def post(self):
    ma = self.request.get('ma').upper()
    dk = self.request.get('dk')
    if IsInteger(dk):
      dk = int(dk)
    else:
      dk = 0
    dev, err = db.CheckDevice(ma, dk)
    if err:
      return self.output(None, err)
  
    response = { 'ma':ma }
    body_offset = 0
    for pin in Each(dev.inputs):
      if pin[0] != 'V':
        continue
      value = self.request.get(pin)
      if value is None:
        continue
      try:
        value = int(value)
      except:
        return self.output(None, 'InvalidValue')
      response[pin] = value
      endpoints = GetEndpointsByStream(dev.skey, dev.did+'.'+pin)
      err = self.save(ma, pin, value, body_offset, endpoints)
      if err:
        return self.output(None, err)
      body_offset += value

    if dev.status != DEV_STATUS['ok']:
      response['rc'] = dev.status
    self.output(response, None)

  def save(self, ma, vid, size, offset, endpoints=[]):
    """Save a video clip. The POST data is MPEG-TS video. Video sizes
    are encoded as query params, enabling multiple videos from the same
    device to share a POST payload. Optionally forward video is via
    UDP/RTP to given endpoints."""

    body = self.request.body
    if size <= 0:
      return 'InvalidValue'
    if len(body) < offset + size or len(body) % CONST['udpSize'] != 0 or size > CONST['maxBlob']:
      logging.debug("Invalid video data, size=%d, len(body)=%d" % (size, len(body)))
      return 'InvalidPayloadSize'

    clip = body[offset:offset+size]
    # ToDo: check that video is actually MPEG-TS format
    CreateMtsVideo(ma, vid, clip)
    # forward to endpoints, if any
    for endpoint in endpoints:
      endpoint.forward(clip)
    return None

  def output(self, response, err):
    if err:
      output = json.dumps({'er':err})
    else:
      output = json.dumps(response)

    self.response.headers['Access-Control-Allow-Origin'] = '*'
    self.response.headers['Content-Type'] = 'application/json'
    self.response.out.write(output)

# Download/playback
class VideoHandler(BaseHandler):
  """Handle requests for stored video."""
  def get(self, key, name):
    """Serve a video. Key must be a site key or an ASP key."""
    err = None
    if key.isalpha():
      key, err = db.CheckAsp(key, 'video')
    if not err:
      site, err = db.CheckSite(key, True)
    if not err and not IsExtendedPin(name, 'V'):
      err = 'InvalidVideoId'
    if err:
      return self.output(None, err)

    di, vi = name.split('.')
    dev = db.GetDeviceById(site.skey, di)
    if not dev:
      return self.output(None, 'DeviceNotFound')
    if not vi in dev.inputs.split(','):
      return self.output(None, 'PinNotFound')
  
    vids = GetMtsVideos(db.MacDecode(dev.mac), vi) # NB: vid may no longer be associated with device
    output = ''
    vids.reverse() # since reverse chronological
    for vid in vids:
      output += vid.clip

    logging.debug("Serving video from %s/%s.%s (%d bytes)" % (key, di, vi, len(output)))
    self.response.headers['Access-Control-Allow-Origin'] = '*'
    self.output(output, err)

  def output(self, response, err):
    if err:
      response = json.dumps({'er':err})
      self.response.headers['Content-Type'] = 'application/json'
    else:
      self.response.headers['Content-Type'] = 'video/mp2t'

    self.response.headers['Access-Control-Allow-Origin'] = '*'
    self.response.out.write(response)
