# Description:
#   Lists used by NetReceiver
#
#   CONST - various constants
#
#   ERRORS - error codes/values
#
#   DEV_STATUS - NetReceive device status/actions
#
#   KINDS - data store classes
#
#   COUNTRIES - country names and 2-letter codes
#   Source: 
#     https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2
#
#   QUANTITIES - quantity names, 3-letter codes and types
#   Source:
#     A blend of Seatalk and NMEA codes, plus some I made up
#     Seatalk reference: http://www.thomasknauf.de/rap/seatalk2.htm
#     NMEA 0183: http://catb.org/gpsd/NMEA.html
#
#   FMT_NAMES - format names
#   FMT_FUNCS - format funtions
#
#   OP_NAMES - trigger operation names
#   OP_FUNCS - trigger operation funtions
#
#   ACTIONS - trigger actions
#
#   ROLES - user roles
#
#   ASP_URLS - URLs we accept for app-specific passwords
#
#   TIMES - times of day we accept in lieu of numerical times
#
#   VARS - standard variable names
#
# License:
#   Copyright (C) 2017-2018 Alan Noble.
#
#   This file is part of NetReceiver. NetReceiver is free software: you can
#   redistribute it and/or modify it under the terms of the GNU
#   General Public License as published by the Free Software
#   Foundation, either version 3 of the License, or (at your option)
#   any later version.
#
#   NetReceiver is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with NetReceiver in gpl.txt.  If not, see
#   <http://www.gnu.org/licenses/>.
#

import binascii

from sds import dof_d

CONST = {
  'emailSender':       'netreceiverservice@gmail.com',
  'minsInDay':         1440,
  'megaByte':          1024000,
  'maxBlob':           1024000,   # 1 MegaByte
  'maxHttpResponse':   32768000,  # 32 MegaBytes
  'pinTypes':          'ABDTVX',
  'monitorPeriod':     60,        # minutes
  'minMonitorPeriod':  60,        # minutes
  'notifyPeriod':      1,         # hour
  'endpointTimeout':   5,         # minutes
  'udpSize':           1316,      # = 7 x MPEG-TS packets of size 188 bytes
  'maxSites':          10,
  'maxPins':           10,
  'maxRequests':       10,
  'maxAsps':           100,
  'maxUsers':          100,
  'maxVars':           100,
  'maxDevices':        100,
  'maxEndpoints':      100,
  'maxMtsVideo':       600,       # 10 minutes worth of 1-second clips
  'maxRecords':        1000,
  'maxBinaryData':     60,        # ~5 minutes worth of 5-second data
  'dataDuration':      60,        # minutes
  'audioDuration':     3,         # minutes
}

ERRORS = {
  'SiteConfirmationRequired': "Your have been emailed a confirmation email. Please click on the confirm link to complete your registration.",
  'SiteConfirmationNotRequired': "Your site has been created without confirmation and can be found under Sites.",
  'KeyRequired': 'Key is required.',
  'SiteKeyRequired': 'Site key is required.',
  'SiteNameRequired': 'Site name is required.',
  'MacRequired': 'MAC address is required.',
  'PinRequired': 'Pin is required.',
  'TagRequired': 'Tag is required.',
  'AccessDenied': 'Access denied. Contact your administrator for access.',
  'InvalidEmail': 'Invalid email address.',
  'InvalidSiteKey': 'Invalid site key.',
  'InvalidLatLng': 'Invalid latitude,longitude.',
  'InvalidDeviceKey': 'Invalid device key.',
  'InvalidMac': 'Invalid MAC address.',
  'InvalidObject': 'Invalid object.',
  'InvalidOperation': 'Invalid operation.',
  'InvalidAction': 'Invalid action.',
  'InvalidPin': 'Invalid pin name(s).',
  'InvalidMethod': 'Invalid method for given data type.',
  'TooManyPins': 'Too many pins (10 is maximum).',
  'InvalidMonitorPeriod': 'Invalid monitor period.',
  'InvalidActuationPeriod': 'Invalid actuation period.',
  'InvalidId': 'Invalid ID.',
  'InvalidSensorId': 'Invalid sensor ID.',
  'InvalidVariable': 'Invalid variable ID.',
  'InvalidValue': 'Invalid value.',
  'InvalidTime': 'Invalid time.',
  'InvalidTimeOfDay': 'Invalid time of day.',
  'InvalidIpAddress': 'Invalid IP address.',
  'InvalidVideoId': 'Invalid video ID.',
  'InvalidPayloadSize': 'Invalid payload size.',
  'InvalidRole': 'Invalid role.',
  'InvalidKey': 'Invalid Key.',
  'InvalidURL': 'Invalid URL.',
  'InvalidNumber': 'Invalid number.',
  'InvalidStartTime': 'Invalid start time.',
  'InvalidFinishTime': 'Invalid finish time.',
  'InvalidDuration': 'Invalid period.',
  'InvalidFunction': 'Invalid function.',
  'InvalidArgs': 'Invalid function arguments.',
  'InvalidFormat': 'Invalid format.',
  'InvalidDataType': 'Invalid data type.',
  'ExistingSite': 'Site already registered.',
  'ExistingDevice': 'Device with this MAC address already exists.',
  'ExistingEndpoint': 'Endpoint with this ID already exists.',
  'ExistingSensor': 'Sensor with this ID already exists.',
  'ExistingTrigger': 'Trigger with this ID already exists.',
  'ExistingCron': 'Cron with this ID already exists.',
  'ExistingAsp': 'This application specific password already exists.',
  'SiteDeleted': 'Site deleted.',
  'SiteNotFound': 'Site not found.',
  'DeviceNotFound': 'Device not found.',
  'PinNotFound': 'Pin not found.',
  'EndpointNotFound': 'Endpoint not found.',
  'SensorNotFound': 'Sensor not found.',
  'TriggerNotFound': 'Trigger not found.',
  'KeyNotFound': 'Key not found.',
  'ValueNotFound': 'Value missing.',
  'SiteNotEnabled': 'Site not enabled.',
  'DeviceNotEnabled': 'Device not enabled.',
  'DeviceNotReady': 'Device not ready.',
  'PremiumFeature': 'Premium feature not enabled for this site.'
}

DEV_STATUS = {
  'ok':        0,
  'configure': 1,
  'reboot':    2,
  'debug':     3,
  'upgrade':   4
}

KINDS = {
  'settings': ['User', 'ASP', 'Device', 'Sensor', 'Actuator', 'Endpoint', 'Trigger', 'Cron', 'Var'],
  'data':     ['Signal', 'BinaryData', 'MtsVideo', 'Request', 'Notice']
}

COUNTRIES = [
  {'code':'AF', 'name':"Afghanistan"},
  {'code':'AL', 'name':"Albania"},
  {'code':'DZ', 'name':"Algeria"},
  {'code':'AS', 'name':"American Samoa"},
  {'code':'AD', 'name':"Andorra"},
  {'code':'AO', 'name':"Angola"},
  {'code':'AQ', 'name':"Antarctica"},
  {'code':'AG', 'name':"Antigua and Barbuda"},
  {'code':'AR', 'name':"Argentina"},
  {'code':'AM', 'name':"Armenia"},
  {'code':'AW', 'name':"Aruba"},
  {'code':'AU', 'name':"Australia"},
  {'code':'AT', 'name':"Austria"},
  {'code':'AZ', 'name':"Azerbaijan"},
  {'code':'BS', 'name':"Bahamas"},
  {'code':'BH', 'name':"Bahrain"},
  {'code':'BD', 'name':"Bangladesh"},
  {'code':'BB', 'name':"Barbados"},
  {'code':'BY', 'name':"Belarus"},
  {'code':'BE', 'name':"Belgium"},
  {'code':'BZ', 'name':"Belize"},
  {'code':'BJ', 'name':"Benin"},
  {'code':'BM', 'name':"Bermuda"},
  {'code':'BT', 'name':"Bhutan"},
  {'code':'BO', 'name':"Bolivia"},
  {'code':'BA', 'name':"Bosnia and Herzegovina"},
  {'code':'BW', 'name':"Botswana"},
  {'code':'BV', 'name':"Bouvet Island"},
  {'code':'BR', 'name':"Brazil"},
  {'code':'IO', 'name':"British Indian Ocean Territory"},
  {'code':'BN', 'name':"Brunei Darussalam"},
  {'code':'BG', 'name':"Bulgaria"},
  {'code':'BF', 'name':"Burkina Faso"},
  {'code':'BI', 'name':"Burundi"},
  {'code':'KH', 'name':"Cambodia"},
  {'code':'CM', 'name':"Cameroon"},
  {'code':'CA', 'name':"Canada"},
  {'code':'CV', 'name':"Cape Verde"},
  {'code':'KY', 'name':"Cayman Islands"},
  {'code':'CF', 'name':"Central African Republic"},
  {'code':'TD', 'name':"Chad"},
  {'code':'CL', 'name':"Chile"},
  {'code':'CN', 'name':"China"},
  {'code':'CX', 'name':"Christmas ISLand"},
  {'code':'CC', 'name':"Cocos (Keeling) Islands"},
  {'code':'CO', 'name':"Colombia"},
  {'code':'KM', 'name':"Comoros"},
  {'code':'CG', 'name':"Congo"},
  {'code':'CD', 'name':"The Democratic Republic of the Congo"},
  {'code':'CK', 'name':"Cook Islands"},
  {'code':'CR', 'name':"Costa Rica"},
  {'code':'CI', 'name':"Cote D'Ivoire"},
  {'code':'HR', 'name':"Croatia"},
  {'Code':'CU', 'name':"Cuba"},
  {'code':'CY', 'name':"Cyprus"},
  {'Code':'CZ', 'name':"Czech Republic"},
  {'code':'DK', 'name':"Denmark"},
  {'Code':'DJ', 'name':"Djibouti"},
  {'code':'DM', 'name':"Dominica"},
  {'code':'DO', 'name':"Dominican Republic"},
  {'code':'EC', 'name':"Ecuador"},
  {'code':'EG', 'name':"Egypt"},
  {'code':'SV', 'name':"El Salvador"},
  {'code':'GQ', 'name':"Equatorial Guinea"},
  {'code':'ER', 'name':"Eritrea"},
  {'code':'EE', 'name':"Estonia"},
  {'code':'ET', 'name':"Ethiopia"},
  {'code':'FK', 'name':"Falkland Islands (Malvinas)"},
  {'code':'FO', 'name':"Faroe Islands"},
  {'code':'FJ', 'name':"Fiji"},
  {'code':'FI', 'name':"Finland"},
  {'code':'FR', 'name':"France"},
  {'code':'GF', 'name':"French Guiana"},
  {'code':'PF', 'name':"French Polynesia"},
  {'code':'TF', 'name':"French Southern Territories"},
  {'code':'GA', 'name':"Gabon"},
  {'code':'GM', 'name':"Gambia"},
  {'code':'GE', 'name':"Georgia"},
  {'code':'DE', 'name':"Germany"},
  {'code':'GH', 'name':"Ghana"},
  {'code':'GI', 'name':"Gibraltar"},
  {'code':'GR', 'name':"Greece"},
  {'code':'GL', 'name':"Greenland"},
  {'code':'GD', 'name':"Grenada"},
  {'code':'GP', 'name':"Guadeloupe"},
  {'code':'GU', 'name':"Guam"},
  {'code':'GT', 'name':"Guatemala"},
  {'code':'GN', 'name':"Guinea"},
  {'code':'GW', 'name':"Guinea-Bissau"},
  {'code':'GY', 'name':"Guyana"},
  {'code':'HT', 'name':"Haiti"},
  {'code':'HM', 'name':"Heard Island and Mcdonald Islands"},
  {'code':'HN', 'name':"Honduras"},
  {'code':'HK', 'name':"Hong Kong"},
  {'code':'HU', 'name':"Hungary"},
  {'code':'IS', 'name':"Iceland"},
  {'code':'IN', 'name':"India"},
  {'code':'ID', 'name':"Indonesia"},
  {'code':'IR', 'name':"Iran, Islamic Republic of"},
  {'code':'IQ', 'name':"Iraq"},
  {'code':'IE', 'name':"Ireland"},
  {'code':'IL', 'name':"Israel"},
  {'code':'IT', 'name':"Italy"},
  {'code':'JM', 'name':"Jamaica"},
  {'code':'JP', 'name':"Japan"},
  {'code':'JO', 'name':"Jordan"},
  {'code':'KZ', 'name':"Kazakhstan"},
  {'code':'KE', 'name':"Kenya"},
  {'code':'KI', 'name':"Kiribati"},
  {'code':'KP', 'name':"Korea, Democratic People's Republic of"},
  {'code':'KR', 'name':"Korea, Republic of"},
  {'code':'KW', 'name':"Kuwait"},
  {'code':'KG', 'name':"Kyrgyzstan"},
  {'code':'LA', 'name':"Lao People'S Democratic Republic"},
  {'code':'LV', 'name':"Latvia"},
  {'code':'LB', 'name':"Lebanon"},
  {'code':'LS', 'name':"Lesotho"},
  {'code':'LR', 'name':"Liberia"},
  {'code':'LY', 'name':"Libyan Arab Jamahiriya"},
  {'code':'LI', 'name':"Liechtenstein"},
  {'code':'LT', 'name':"Lithuania"},
  {'code':'LU', 'name':"Luxembourg"},
  {'code':'MO', 'name':"Macao"},
  {'code':'MK', 'name':"Macedonia, the Former Yugoslav Republic of"},
  {'code':'MG', 'name':"Madagascar"},
  {'code':'MW', 'name':"Malawi"},
  {'code':'MY', 'name':"Malaysia"},
  {'code':'MV', 'name':"Maldives"},
  {'code':'ML', 'name':"Mali"},
  {'code':'MT', 'name':"Malta"},
  {'code':'MH', 'name':"Marshall Islands"},
  {'code':'MQ', 'name':"Martinique"},
  {'code':'MR', 'name':"Mauritania"},
  {'code':'MU', 'name':"Mauritius"},
  {'code':'YT', 'name':"Mayotte"},
  {'code':'MX', 'name':"Mexico"},
  {'code':'FM', 'name':"Micronesia, Federated States of"},
  {'code':'MD', 'name':"Moldova, Republic of"},
  {'code':'MC', 'name':"Monaco"},
  {'code':'MN', 'name':"Mongolia"},
  {'code':'ME', 'name':"Montenegro"},
  {'code':'MS', 'name':"Montserrat"},
  {'code':'MA', 'name':"Morocco"},
  {'code':'MZ', 'name':"Mozambique"},
  {'code':'MM', 'name':"Myanmar"},
  {'code':'NA', 'name':"Namibia"},
  {'code':'NR', 'name':"Nauru"},
  {'code':'NP', 'name':"Nepal"},
  {'code':'NL', 'name':"Netherlands"},
  {'code':'AN', 'name':"Netherlands Antilles"},
  {'code':'NC', 'name':"New Caledonia"},
  {'code':'NZ', 'name':"New Zealand"},
  {'code':'NI', 'name':"Nicaragua"},
  {'code':'NE', 'name':"Niger"},
  {'code':'NG', 'name':"Nigeria"},
  {'code':'NU', 'name':"Niue"},
  {'code':'NF', 'name':"Norfolk Island"},
  {'code':'MP', 'name':"Northern Mariana Islands"},
  {'code':'NO', 'name':"Norway"},
  {'code':'OM', 'name':"Oman"},
  {'code':'PK', 'name':"Pakistan"},
  {'code':'PW', 'name':"Palau"},
  {'code':'PS', 'name':"Palestinian Territory, Occupied"},
  {'code':'PA', 'name':"Panama"},
  {'code':'PG', 'name':"Papua New Guinea"},
  {'code':'PY', 'name':"Paraguay"},
  {'code':'PE', 'name':"Peru"},
  {'code':'PH', 'name':"Philippines"},
  {'code':'PN', 'name':"Pitcairn"},
  {'code':'PL', 'name':"Poland"},
  {'code':'PT', 'name':"Portugal"},
  {'code':'PR', 'name':"Puerto RICO"},
  {'code':'QA', 'name':"Qatar"},
  {'code':'RE', 'name':"Reunion"},
  {'code':'RO', 'name':"Romania"},
  {'code':'RU', 'name':"Russian Federation"},
  {'code':'RW', 'name':"Rwanda"},
  {'code':'SH', 'name':"Saint Helena"},
  {'code':'KN', 'name':"Saint Kitts and Nevis"},
  {'code':'LC', 'name':"Saint Lucia"},
  {'code':'PM', 'name':"Saint Pierre and Miquelon"},
  {'code':'VC', 'name':"Saint Vincent and the Grenadines"},
  {'code':'WS', 'name':"Samoa"},
  {'code':'SM', 'name':"San Marino"},
  {'code':'ST', 'name':"Sao Tome and Principe"},
  {'code':'SA', 'name':"Saudi Arabia"},
  {'code':'SN', 'name':"Senegal"},
  {'code':'RS', 'name':"Serbia"},
  {'code':'SC', 'name':"Seychelles"},
  {'code':'SL', 'name':"Sierra Leone"},
  {'code':'SG', 'name':"Singapore"},
  {'code':'SK', 'name':"Slovakia"},
  {'code':'SI', 'name':"Slovenia"},
  {'code':'SB', 'name':"Solomon Islands"},
  {'code':'SO', 'name':"Somalia"},
  {'code':'ZA', 'name':"South Africa"},
  {'code':'GS', 'name':"South Georgia and the South Sandwich Islands"},
  {'code':'ES', 'name':"Spain"},
  {'code':'LK', 'name':"Sri Lanka"},
  {'code':'SD', 'name':"Sudan"},
  {'code':'SR', 'name':"Suriname"},
  {'code':'SJ', 'name':"Svalbard and Jan Mayen"},
  {'code':'SZ', 'name':"Swaziland"},
  {'code':'SE', 'name':"Sweden"},
  {'code':'CH', 'name':"Switzerland"},
  {'code':'SY', 'name':"Syrian Arab Republic"},
  {'code':'TW', 'name':"Taiwan"},
  {'code':'TJ', 'name':"Tajikistan"},
  {'code':'TZ', 'name':"Tanzania, United Republic of"},
  {'code':'TH', 'name':"Thailand"},
  {'code':'TL', 'name':"Timor-Leste"},
  {'code':'TG', 'name':"Togo"},
  {'code':'TK', 'name':"Tokelau"},
  {'code':'TO', 'name':"Tonga"},
  {'code':'TT', 'name':"Trinidad and Tobago"},
  {'code':'TN', 'name':"Tunisia"},
  {'code':'TR', 'name':"Turkey"},
  {'code':'TM', 'name':"Turkmenistan"},
  {'code':'TC', 'name':"Turks and Caicos Islands"},
  {'code':'TV', 'name':"Tuvalu"},
  {'code':'UG', 'name':"Uganda"},
  {'code':'UA', 'name':"Ukraine"},
  {'code':'AE', 'name':"United Arab Emrates"},
  {'code':'GB', 'name':"United Kingdom"},
  {'code':'US', 'name':"United States"},
  {'code':'UM', 'name':"United States Minor Outlying Islands"},
  {'code':'UY', 'name':"Uruguay"},
  {'code':'UZ', 'name':"Uzbekistan"},
  {'code':'VU', 'name':"Vanuatu"},
  {'code':'VE', 'name':"Venezuela"},
  {'code':'VN', 'name':"Vietnam"},
  {'code':'VG', 'name':"Virgin Islands, British"},
  {'code':'VI', 'name':"Virgin Islands, U.S."},
  {'code':'WF', 'name':"Wallis and Futuna"},
  {'code':'EH', 'name':"Western Sahara"},
  {'code':'YE', 'name':"Yemen"},
  {'code':'ZM', 'name':"Zambia"},
  {'code':'ZW', 'name':"Zimbabwe"}
]

QUANTITIES = [
  {'code':'AWA', 'name':'Apparent Wind Angle', 'type':'angle'},
  {'code':'AWS', 'name':'Apparent Wind Speed', 'type':'speed'},
  {'code':'AUD', 'name':'Audio', 'type':'audio'},
  {'code':'BIN', 'name':'Boolean', 'type':'bool'},
  {'code':'DIS', 'name':'Distance', 'type':'length'},
  {'code':'DPT', 'name':'Depth', 'type':'length'},
  {'code':'GGA', 'name':'GPS Fix', 'type':'position'},
  {'code':'DCV', 'name':'DC Voltage', 'type':'voltage'},
  {'code':'ACV', 'name':'AC Voltage', 'type':'voltage'},
  {'code':'HDM', 'name':'Heading (Magnetic)', 'type':'angle'},
  {'code':'HDT', 'name':'Heading (True)', 'type':'angle'},
  {'code':'MMB', 'name':'Humidity', 'type':'percent'},
  {'code':'MTA', 'name':'Air Pressure', 'type':'pressure'},
  {'code':'MWH', 'name':'Air Temperature', 'type':'temperature'},
  {'code':'MTW', 'name':'Water Temperature', 'type':'temperature'},
  {'code':'PPT', 'name':'Precipitation', 'type':'length'},
  {'code':'SOG', 'name':'Speed Over Ground', 'type':'speed'},
  {'code':'STW', 'name':'Speed Thru Water', 'type':'speed'},
  {'code':'TBD', 'name':'Turbidty', 'type':'turbidity'},
  {'code':'TWA', 'name':'True Wind Angle', 'type':'angle'},
  {'code':'TWG', 'name':'True Wind Gust', 'type':'speed'},
  {'code':'TWS', 'name':'True Wind Speed', 'type':'speed'},
  {'code':'MWS', 'name':'Wave Height', 'type':'distance'},
  {'code':'VID', 'name':'Video', 'type':'video'},
  {'code':'OTH', 'name':'Other', 'type':'unknown'},
]

POC = ['N', 'NE', 'E', 'SE', 'S', 'SW', 'W', 'NW']

# NB: keep xxx_NAMES and xxx_FUNCS in sync
X_NAMES = ['none', 'scale', 'linear', 'quadratic', 'len', 'dof.d', 'custom']

X_FUNCS = {'none':lambda x: x,
           'scale':lambda x, a: a * x,
           'linear':lambda x, a, b: a * x + b,
           'quadratic':lambda x, a, b, c: a * x**2 + b * x + c,
           'len':lambda x: len(x),
           'dof.d':lambda x: dof_d(x),
           'custom':lambda x: x }

X_ARGC = {'none':0,
          'scale':1,
          'linear':2,
          'quadratic':3,
          'len':0,
          'dof.d':0,
          'custom':0 }

FMT_NAMES = ['bool', 'round', 'round1', 'compass', 'hex']

FMT_FUNCS = {'bool':lambda x: not (not x or x == '0' or x == 'false' or x == 'off'),
             'round':lambda x: int(round(float(x))),
             'round1':lambda x: round(float(x), 1),
             'compass':lambda x: POC[(int(x)/45) % 8],
             'hex':lambda x: str(binascii.hexlify(x)),
             'noop':lambda x: x }

OP_NAMES = ['<', '<=', '=', '>=', '>']

OP_FUNCS = {'<':lambda a, b: a < b,
            '<=':lambda a, b: a <= b,
            '=':lambda a, b: a == b,
            '>=':lambda a, b: a >= b,
            '>':lambda a, b: a > b }

ACTIONS = ['email', 'sms', 'set', 'del']

ROLES = ['read', 'write', 'admin']

ASP_URLS = ['monitor', 'var', 'video', 'audio', 'text', 'data', 'play', 'upload']

TIMES = ['Midnight', 'Sunrise', 'Noon', 'Sunset']

VARS = [
  'Pulses', 'PulsePeriod', 'PulseDutyCycle', 'PulseCycle', 'AlarmRestart',
  'AlarmPin', 'AlarmLevel', 'AlarmPeriod', 'AlarmNetwork', 'AlarmVoltage', 'AlarmRecoveryVoltage',
  'SensePin'
]
