# Description:
#   NetReceiver - a service for collecting and monitoring remote sensor data ("sensing") and
#   controlling ("actuating") Internet of Things (IoT) clients.
#
#   See also http://netreceiver.appspot.com
#
# License:
#   Copyright (C) 2017-2018 Alan Noble.
#
#   This file is part of NetReceiver. NetReceiver is free software: you can
#   redistribute it and/or modify it under the terms of the GNU
#   General Public License as published by the Free Software
#   Foundation, either version 3 of the License, or (at your option)
#   any later version.
#
#   NetReceiver is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with NetReceiver in gpl.txt.  If not, see
#   <http://www.gnu.org/licenses/>.

import datetime
import random
import logging
import json
import string
import wave
import StringIO
import webapp2

# AppEngine modules
from google.appengine.api import taskqueue

# Other 3rd-party modules
import gviz_api

# NetReceiver files
from lists import CONST, KINDS, DEV_STATUS, COUNTRIES, QUANTITIES, X_NAMES, X_FUNCS, X_ARGC, FMT_NAMES, FMT_FUNCS, OP_NAMES, OP_FUNCS, ACTIONS, ROLES, ASP_URLS, TIMES, VARS
from valid import IsTrue, IsNumber, IsInteger, IsSiteKey, IsEmail, IsMacAddress, IsIdentifier, IsPin, IsExtendedPin, IsPinList, IsIpv4, IsIpv6, IsLatLng, IsTime, FixSpaces, Each
from webapp import JINJA_ENV, BaseHandler, SendEmail
import db
from video import DeleteMtsVideos, Endpoint, GetOrCreateEndpoint, GetEndpoint, GetEndpointsByStream, GetEndpointsBySite,DeleteEndpoint, RecvVideoHandler, VideoHandler

###############################################################################
# Global constants

VERSION = "1.1"
JINJA_ENV.filters['macencode'] = db.MacEncode
JINJA_ENV.filters['macdecode'] = db.MacDecode

###############################################################################
# Conversion

def ToFloat(val, default=0.0):
  """Return a value as float if it is a number, otherwise return a default value."""
  if IsNumber(val):
    return float(val)
  else:
    return default

def ToInteger(val, default=0):
  """Return a value as an integer, otherwise return a default value."""
  if IsInteger(val):
    return int(val)
  else:
    return default

def ToIntegerValue(val):
  """Return a value as an integer."""
  if IsInteger(val):
    return int(val)
  elif IsTrue(val):
    return 1
  else:
    return 0

def DateToTime(date, tz_offset=0):
  """Return time from a date."""
  if tz_offset != 0:
    date += datetime.timedelta(hours=tz_offset)
  return datetime.time(date.hour, date.minute, date.second)

def ToDate(val, tz_offset=0):
  """Convert a date/time, date or time string into a datetime or return None on error."""
  try:
    date = datetime.datetime.strptime(val, "%Y-%m-%d %H:%M")
  except:
    try:
      date = datetime.datetime.strptime(val, "%Y-%m-%d")
    except:
      time, mins = IsTime(val, 0)
      if not time:
        return None
      today = datetime.datetime.today()
      date = datetime.datetime(today.year, today.month, today.day, time.hour, time.minute)

  date += datetime.timedelta(hours=tz_offset)
  return date

def ToDateRange(start_str, finish_str, duration, tz_offset=0):
  """Return a (start,finish,error) tuple given date strings and/or a duration.
The latter is a datetime.timedelta."""
  start = None
  if start_str:
    start = ToDate(start_str, tz_offset)
    if not start:
      return None, None, 'InvalidStartTime'
  finish = None
  if finish_str:
    finish = ToDate(finish_str, tz_offset)
    if not finish:
      return None, None, 'InvalidFinishTime'

  if start:
    if not finish:
      finish = start + duration
  else:
    if finish:
      start = finish - duration
    else:
      now = datetime.datetime.now()
      start = now - duration
      finish = now

  return start, finish, None

class utctz(datetime.tzinfo):
  def utcoffset(self, dt):
    return datetime.timedelta(0)
  def dst(self, dt):
    return datetime.timedelta(0)
  def tzname(self, dt):
    return "UTC"

def ToTimestamp(dt, epoch=datetime.datetime(1970,1,1,0,0,0,0,utctz())):
  """Return timestamp to nearest second from given date.  Date must have a timezone."""
  td = dt - epoch
  return td.seconds + (td.days * 86400)

def FromTimestamp(ts, epoch=datetime.datetime(1970,1,1,0,0,0,0,utctz())):
  """Return date from given timestamp."""
  return epoch + datetime.timedelta(seconds=ts)

def IncrTime(time):
  """Increment a time and return a new time object and minutes in the day."""
  minutes = ((time.hour * 60) + (time.minute + 1)) % CONST['minsInDay']
  return datetime.time(int(minutes/60), minutes%60), minutes

# serialization/deserialization
def Jsonify(obj):
  """JSON serializer for objects that not serializable by default, such as date and time."""
  if isinstance(obj, (datetime.datetime, datetime.date)):
    serial = obj.isoformat()
  elif isinstance(obj, (datetime.datetime, datetime.time)):
    serial = "%02d:%02d" % (obj.hour, obj.minute)
  else:
    raise TypeError ("Type %s not serializable" % type(obj))
  return serial

def Dictify(str, major, minor):
  """Dictify splits a string twice into a dictionary.
  The major delimiter separates items and the minor delimiter separates each item into a name/value pair.
  For example, Dictify("a=b,c=d,e", ",", "=") => { "a":"b", "c":"d", "e":"" }"""
  dct = {}
  for item in Each(str, major):
    pair = Each(item, minor)
    if len(pair) == 0:
      continue
    if len(pair) == 1:
      dct[pair[0]] = ""
    else:
      dct[pair[0]] = pair[1]
  return dct

###############################################################################
# Notification

def Notify(site, kind, obj):
  """Send a notification unless we recently sent an identical notice."""
  if not (kind == 'trigger' or kind == 'cron'):
    return False
  if obj.action != 'email':
    return False # ToDo: implement SMS

  since = datetime.datetime.now() - datetime.timedelta(hours=site.notify_period)
  notice = db.GetLatestNotice(site.skey, kind, obj.id, obj.action, since)
  if notice:
    return False
  
  if site.notify_email:
    recipient = site.notify_email
  else:
    recipient = site.user_email
    
  template = JINJA_ENV.get_template(kind + '_email.txt')
  subject = 'NetReceiver ' + kind
  values = {'sender':CONST['emailSender'], 'recipient':recipient, 'subject':subject, 'site':site, kind:obj}
  SendEmail(template, values)
  
  notice = db.Notice(skey=site.skey, kind=kind, id=obj.id, action=obj.action, recipient=recipient)
  notice.put()
  logging.info("notified: sk=%d, kind=%s, id=%s, action=%s, recipient=%s" % (site.skey, kind, obj.id, obj.action, recipient))
  return True

###############################################################################
# Request handlers

class MainHandler(BaseHandler):
  """Handles requests to the main page.""" 
  def get(self):
    sk = ToInteger(self.request.get('sk'))
    return self.WriteTemplate("index.html", { 'sk':sk })

class SitesHandler(BaseHandler):
  """Return the list of sites for the current user."""
  def get(self):
    sk = ToInteger(self.request.get('sk'))
    sites = db.GetSitesByUser(self.user_email)
    for site in sites:
      if self.is_app_admin or self.user_email == site.user_email:
        site.role = 'admin'
      else:
        site.role = db.GetUserRole(site.skey, self.user_email)
    values = { 'sk':sk, 'sites':sites }
    self.WriteTemplate('sites.html', values)

class SettingsHandler(BaseHandler):
  """Handles setting requests."""
  def get(self, obj, action):
    sk = ToInteger(self.request.get('sk'))
    ma = self.request.get('ma')
    
    self.values = { 'sk':sk, 'ma':ma, 'pins':{}, 'quantities':QUANTITIES, 'funcs':X_NAMES, 'formats':FMT_NAMES, 'operations':OP_NAMES, 'actions':ACTIONS, 'varlist':VARS }

    site, err = db.CheckSite(sk)
    if not err and db.GetUserRole(site.skey, self.user_email) == 'read':
      err = 'AccessDenied'
    if not err:
      times = [{'name':tod, 'value':site.timeOfDay(tod)[0]} for tod in TIMES]
      crons = [cron.update(site) for cron in db.GetCronsBySite(sk, site.tz_offset)]
      self.values.update({'sn':site.name, 'pr':site.premium, 'tz':site.tz_offset,
                          'devices':db.GetDevicesBySite(sk), 'pins':db.GetPinsBySite(sk),
                          'vars':db.GetVarsBySite(sk), 'sensors':db.GetSensorsBySite(sk),
                          'actuators':db.GetActuatorsBySite(sk), 'endpoints':GetEndpointsBySite(sk),
                          'triggers':db.GetTriggersBySite(sk), 'crons':crons, 'times':times })
    self.WriteTemplate('settings.html', self.values, err)

  def post(self, obj, action):
    sk = ToInteger(self.request.get('sk'))

    self.values = { 'sk':sk, 'pins':{}, 'quantities':QUANTITIES, 'funcs':X_NAMES, 'formats':FMT_NAMES, 'operations':OP_NAMES, 'actions':ACTIONS, 'varlist':VARS }

    if obj in ['device', 'sensor', 'actuator', 'endpoint', 'trigger', 'cron', 'var']: 
      if db.GetUserRole(sk, self.user_email) == 'read':
        err = 'AccessDenied'
      else:
        err = self.__getattribute__(obj)(action)
    else:
      err = 'InvalidObject'

    if sk and err:
      site = db.GetSite(sk)
      if site:
        self.values.update({'sn':site.name, 'pr':site.premium, 'tz':site.tz_offset,
                            'devices':db.GetDevicesBySite(sk), 'pins':db.GetPinsBySite(sk),
                            'vars':db.GetVarsBySite(sk), 'sensors':db.GetSensorsBySite(sk),
                            'actuators':db.GetActuatorsBySite(sk), 'endpoints':GetEndpointsBySite(sk),
                            'triggers':db.GetTriggersBySite(sk), 'crons':db.GetCronsBySite(sk) })
      self.WriteTemplate('settings.html', self.values, err)
    else:
      self.redirect('/settings?sk=%d' % sk)

  def device(self, action):
    """Handles device settings."""
    sk = ToInteger(self.request.get('sk'))
    dk = ToInteger(self.request.get('dk'))
    ma = self.request.get('ma').upper()
    di = self.request.get('di')
    wi = FixSpaces(self.request.get('wi'))
    ip = FixSpaces(self.request.get('ip')).upper()
    op = FixSpaces(self.request.get('op')).upper()
    mp = ToInteger(self.request.get('mp'), CONST['monitorPeriod'])
    ap = ToInteger(self.request.get('ap'))
    tg = self.request.get('tg')
    de = IsTrue(self.request.get('de'))

    self.values.update({ 'ma':ma, 'di':di, 'wi':wi, 'ip':ip, 'op':op, 'mp':mp, 'ap':ap, 'tg':tg, 'de':de })

    if action not in ['add', 'update', 'delete', 'reboot', 'debug', 'upgrade']:
      return 'InvalidAction'

    if not IsMacAddress(ma):
      return 'InvalidMac'

    if action == 'delete':
      db.DeleteDevice(ma)
      logging.info("/settings/device/delete: ma=%s" % ma)
      self.values.update({ 'ma':'' })
      return None

    site, err = db.CheckSite(sk)
    if err:
      return err
    if not IsIdentifier(di):
      return 'InvalidId'
    if (ip and not IsPinList(ip)) or (op and not IsPinList(op)):
      return 'InvalidPin'
    if (len(ip.split(",")) > CONST['maxPins']) or (len(op.split(",")) > CONST['maxPins']):
      return 'TooManyPins'
    if not site.premium and mp < CONST['minMonitorPeriod']:
      return 'InvalidMonitorPeriod'
    if ap > mp:
      return 'InvalidActuationPeriod'

    dev = db.GetDevice(ma, False)
    if action == 'add':
      if dev:
        return 'ExistingDevice'
      else:
        # generate a 8-digit random number for the device key
        dk = random.randrange(10**7, 10**8)
        dev, created = db.GetOrCreateDevice(ma, skey=sk, dkey=dk, did=di, inputs=ip, outputs=op, wifi=wi, 
                                         monitor_period=mp, act_period=ap, tag=tg, enabled=de)
        logging.info("/settings/device/add: ma=%s sk=%d" % (ma, sk))
    elif not dev:
      return 'DeviceNotFound'

    if action == 'update':
      # NB: MAC address and did are immutable
      dev.dkey = dk
      dev.inputs = ip
      dev.outputs = op
      dev.wifi = wi
      dev.monitor_period = mp
      dev.act_period = ap
      dev.tag = tg
      dev.enabled = de
      dev.status = DEV_STATUS['configure']
      dev.put()
      logging.info("/settings/device/update: ma=%s, dk=%d, de=%d" % (ma, dk, de))

    elif action == 'reboot' or action == 'debug':
      dev.status = DEV_STATUS[action]
      dev.put()
      logging.info("/settings/device/%s: ma=%s, dk=%d" % (action, ma, dk))

    elif action == 'upgrade':
      if not dev.tag:
        return 'TagRequired'
      if dev.status != DEV_STATUS['ok']:
        return 'DeviceNotReady'
      dev.status = DEV_STATUS['upgrade']
      dev.put()
      logging.info("/settings/device/upgrade: ma=%s, dk=%d, tg=%s" % (ma, dk, tg))

    self.values.update({'ma':'', 'di':'', 'wi':'', 'ip':'', 'op':'', 'mp':'', 'ap':'', 'tg':'', 'de':'' })
    return None

  def sensor(self, action):
    """Handles sensor settings."""
    sk = ToInteger(self.request.get('sk'))
    si = self.request.get('si')
    sp = self.request.get('sp')
    sq = self.request.get('sq')
    sx = self.request.get('sx')
    sa = self.request.get('sa').strip()
    su = self.request.get('su')
    sf = self.request.get('sf')

    self.values.update({ 'si':si, 'sp':sp, 'sq':sq, 'sx':sx, 'sa':sa, 'su':su, 'sf':sf })

    site, err = db.CheckSite(sk)
    if err:
      return err
    if not IsIdentifier(si):
      return 'InvalidId'

    if action == 'delete':
      db.DeleteSensor(sk, si)
      logging.info("/settings/sensor/delete: sk=%d si=%s" % (sk, si))
      self.values.update({ 'si':'' })
      return None

    if sp and not IsExtendedPin(sp):
      return 'InvalidPin'
    if sx not in X_NAMES:
      return 'InvalidFunction'
    args = Each(sa)
    if len(args) != X_ARGC[sx]:
      return 'InvalidArgs'
    if len(args) > 0:
      try:
        for arg in args:
          float(arg)
      except:
        return 'InvalidArgs'

    sen = db.GetSensor(sk, si)
    if action == 'add':
      if sen:
        return 'ExistingSensor'
      else:
        # ToDo: ensure only one sensor for an extended pin
        sen, created = db.GetOrCreateSensor(sk, si, pin=sp, quantity=sq, func=sx, args=sa, units=su, format=sf)
        logging.info("/settings/sensor/add: sk=%d si=%s" % (sk, si))
        self.values.update({ 'sensors':db.GetSensorsBySite(sk) })

    elif action == 'update':
      if not sen:
        return 'SensorNotFound'
      sen.pin = sp
      sen.quantity = sq
      sen.func = sx
      sen.args = sa
      sen.units = su
      sen.format = sf
      sen.put();
      logging.info("/settings/sensor/update: sk=%d si=%s" % (sk, si))

    else:
      return 'InvalidAction'

    self.values.update({'si':'', 'sp':'', 'sq':sq, 'sx':'', 'sa':'', 'su':'', 'sf':''})
    return None

  def actuator(self, action):
    """Handles actuator settings."""
    sk = ToInteger(self.request.get('sk'))
    ai = self.request.get('ai')
    av = self.request.get('av')
    ap = self.request.get('ap')

    if av and not ai:
      ai = av
    self.values.update({ 'ai':ai, 'av':av, 'ap':ap })

    site, err = db.CheckSite(sk)
    if err:
      return err
    if not IsIdentifier(ai, True):
      return 'InvalidId'

    if action == 'delete':
      db.DeleteActuator(sk, ai)
      logging.info("/settings/actuator/delete: sk=%d ai=%s" % (sk, ai))
      self.values.update({ 'ai':'' })
      return None

    if av and not IsIdentifier(av, True):
      return 'InvalidVariable'
    if ap and not IsExtendedPin(ap):
      return 'InvalidPn'

    act = db.GetActuator(sk, ai)
    if action == 'add':
      if act:
        return 'ExistingActuator'
      else:
        # ToDo: ensure only one actuator for an extended pin
        act, created = db.GetOrCreateActuator(sk, ai, var=av, pin=ap)
        logging.info("/settings/actuator/add: sk=%d ai=%s" % (sk, ai))
        self.values.update({ 'actuators':db.GetActuatorsBySite(sk) })
    
    elif action == 'update':
      if not act:
        return 'ActuatorNotFound'
      act.var = av
      act.pin = ap
      act.put()
      logging.info("/settings/actuator/update: sk=%d ai=%s" % (sk, ai))

    else:
      return 'InvalidAction'

    self.values.update({'ai':'', 'av':'', 'ap':''})
    return None

  def endpoint(self, action):
    """Handles endpoint settings."""
    sk = ToInteger(self.request.get('sk'))
    ei = self.request.get('ei')
    es = self.request.get('es')
    ea = self.request.get('ea')
    ep = ToInteger(self.request.get('ep'))
    ex = self.request.get('ex')
    et = ToInteger(self.request.get('et'), CONST['endpointTimeout'])
    ee = IsTrue(self.request.get('ee'))

    self.values.update({ 'ei':ei, 'es':es, 'ea':ea, 'ep':ep, 'ex':ex, 'et':et, 'ee':ee })

    site, err = db.CheckSite(sk, True)
    if err:
      return err
    if not IsIdentifier(ei):
      return 'InvalidId'
    
    if action == 'delete':
      DeleteEndpoint(sk, ei)
      logging.info("/settings/endpoint/delete: sk=%d ei=%s" % (sk, ei))
      self.values.update({ 'ei':'' })
      return None

    if es and not IsExtendedPin(es, 'V'):
      return 'InvalidVideoId'
    if not IsIpv4(ea) and not IsIpv6(ea):
      return 'InvalidIpAddress'
    if not ex:
      ex = 'udp'
    ex = ex.lower()

    endpoint = GetEndpoint(sk, ei)
    if action == 'add':
      if endpoint:
        return 'ExistingEndpoint'
      else:
        endpoint, created = GetOrCreateEndpoint(sk, ei, sid=es, address=ea, port=ep, proto=ex, timeout=et, enabled=ee)
        logging.info("/settings/endpoint/add: sk=%d ei=%s" % (sk, ei))
        self.values.update({ 'endpoints':GetEndpointsBySite(sk) })

    elif action == 'update':
      if not endpoint:
        return 'EndpointNotFound'
      endpoint.sid = es
      endpoint.address = ea
      endpoint.port = ep
      endpoint.proto = ex
      endpoint.timeout = et
      endpoint.started = None
      endpoint.enabled = ee
      endpoint.put()
      logging.info("/settings/endpoint/update: sk=%d ei=%s" % (sk, ei))

    else:
      return 'InvalidAction'

    self.values.update({ 'ei':'', 'es':'', 'ea':'', 'ep':'', 'ex':'', 'et':'', 'ee':'' })
    return None

  def trigger(self, action):
    """Handles trigger settings."""
    sk = ToInteger(self.request.get('sk'))
    ti = self.request.get('ti')
    tp = FixSpaces(self.request.get('tp'))
    ts = self.request.get('ts')
    to = self.request.get('to')
    tc = ToFloat(self.request.get('tc'))
    ta = self.request.get('ta')
    tv = self.request.get('tv')
    td = self.request.get('td')

    self.values.update({ 'ti':ti, 'tp':tp, 'ts':ts, 'to':to, 'tc':tc, 'ta':ta, 'tv':tv, 'td':td })

    site, err = db.CheckSite(sk)
    if err:
      return err
    if not IsIdentifier(ti):
      return 'InvalidId'

    if action == 'delete':
      db.DeleteTrigger(sk, ti)
      logging.info("/settings/trigger/delete: sk=%d ti=%s" % (sk, ti))
      self.values.update({ 'ti':'' })
      return None

    if tp and not IsIdentifier(tp, True):
      return 'InvalidVariable'
    if not IsIdentifier(ts):
      return 'InvalidSensorId'
    if not db.GetSensor(sk, ts):
      return 'SensorNotFound'
    if to not in OP_NAMES:
      return 'InvalidOperation'
    if ta not in ACTIONS:
      return 'InvalidAction'
    if tv and not IsIdentifier(tv, True):
      return 'InvalidVariable'

    trig = db.GetTrigger(sk, ti)
    if action == 'add':
      if trig:
        return 'ExistingTrigger'
      else:
        trig, created = db.GetOrCreateTrigger(sk, ti, sid=ts, precond=tp, op=to, cmp=tc, action=ta, var=tv, data=td)
        logging.info("/settings/trigger/add: sk=%d ti=%s" % (sk, ti))
        self.values.update({ 'triggers':db.GetTriggersBySite(sk) })
    
    elif action == 'update':
      if not trig:
        return 'TriggerNotFound'
      trig.sid = ts
      trig.precond = tp
      trig.op = to
      trig.cmp = tc
      trig.action = ta
      trig.var = tv
      trig.data = td
      trig.put();
      logging.info("/settings/trigger/update: sk=%d ti=%s" % (sk, ti))
        
    else:
      return 'InvalidAction'

    self.values.update({ 'ti':'', 'tp':'', 'ts':'', 'to':'', 'tc':'', 'ta':'', 'tv':'', 'td':'' })
    return None

  def cron(self, action):
    """Handles cron settings."""
    sk = ToInteger(self.request.get('sk'))
    ci = self.request.get('ci')
    ct = self.request.get('ct')
    ca = self.request.get('ca')
    cv = self.request.get('cv')
    cd = self.request.get('cd')
    ce = IsTrue(self.request.get('ce'))

    self.values.update({ 'ci':ci, 'ct':ct, 'ca':ca, 'cv':cv, 'cd':cd, 'ce':ce })

    site, err = db.CheckSite(sk)
    if err:
      return err
    if not IsIdentifier(ci):
      return 'InvalidId'

    if action == 'delete':
      db.DeleteCron(sk, ci)
      logging.info("/settings/cron/delete: sk=%d ci=%s" % (sk, ci))
      self.values.update({ 'ci':'' })
      return None

    mins = 0
    if ct[0] == '*' or ct.isalpha():
      time, mins = site.timeOfDay(ct)
      if not time:
        return 'InvalidTimeOfDay'
      tod = ct
    else:
      time, mins = IsTime(ct, site.tz_offset)
      if not time:
        return 'InvalidTime'
      tod = None

    if ca not in ACTIONS:
      return 'InvalidAction'
    if cv and not IsIdentifier(cv, True):
      return 'InvalidVariable'

    cron = db.GetCron(sk, ci)

    if action == 'add':
      if cron:
        return 'ExistingCron'
      else:
        cron, created = db.GetOrCreateCron(sk, ci, tod, time=time, minutes=mins, action=ca, var=cv, data=cd, enabled=ce)
        logging.info("/settings/cron/add: sk=%d ci=%s" % (sk, ci))
        self.values.update({ 'crons':db.GetCronsBySite(sk) })

    elif action == 'update':
      if not cron:
        return 'CronNotFound'
      cron.sid = ci
      cron.time = time
      cron.tod = tod
      cron.repeat = tod is not None and tod[0] == '*'
      cron.minutes = mins
      cron.action = ca
      cron.var = cv
      cron.data = cd
      cron.enabled = ce
      cron.put();
      logging.info("/settings/cron/update: sk=%d ci=%s" % (sk, ci))

    else:
      return 'InvalidAction'

    self.values.update({ 'ci':'', 'ct':'', 'ca':'', 'cv':'', 'cd':'', 'ce':ce })
    return None

  def var(self, action):
    """Handles var changes."""
    sk = ToInteger(self.request.get('sk'))
    vn = self.request.get('vn')
    vv = self.request.get('vv')

    self.values.update({ 'vn':vn, 'vv':vv })

    site, err = db.CheckSite(sk)
    if err:
      return err
    if not IsIdentifier(vn, True):
      return 'InvalidId'

    if action == 'delete':
      db.DeleteVar(sk, vn)
      logging.info("/settings/var/delete: sk=%d vn=%s" % (sk, vn))
      self.values.update({ 'vn':'' })
      return None

    if action == 'add' or action == 'update':
      db.UpdateOrCreateVar(sk, vn, vv)
      logging.info("/settings/var/%s: sk=%d vn=%s vv=%s" % (action, sk, vn, vv))
    else:
      return 'InvalidAction'

    self.values.update({ 'vn':'', 'vv':'' })
    return None

class MonitorHandler(BaseHandler):
  """Handles monitor requests."""

  def get(self, action):
    return self.post(action)
  
  def post(self, action):
    sk = ToInteger(self.request.get('sk'), 0)
    ak = self.request.get('ak')
    err = self.request.get('er')
    cp = ToInteger(self.request.get('cp'), 60) # count period in minutes
    df = self.request.get('df')

    if not err:
      if ak:
        sk, err = db.CheckAsp(ak, 'monitor')
      if not err:
        site, err = db.CheckSite(sk)

    if err:
      return self.WriteTemplate('monitor.html', {}, err);

    if site.tz_offset:
      tdelta = datetime.timedelta(hours=site.tz_offset)
    else:
      tdelta = None

    # annotate devices with vars, lan, data (pin,sensor,signals), status and uptime
    now = datetime.datetime.now()
    if cp:
      since = now - datetime.timedelta(seconds=cp*60)
    devices = db.GetDevicesBySite(site.skey)
    for dev in devices:
      dev.vars = db.GetVarsByDevice(dev.skey, dev.did)
      dev.la = db.GetLocalAddr(dev.mac)
      dev.data = []
      dev.count = 0
      dev.max_count = cp * 60 / dev.monitor_period
      for pin in Each(dev.inputs):
        if cp and pin[0] in ['A', 'D', 'X'] and dev.count == 0:
          dev.count = db.CountSignals(dev.mac, pin, since)
          dev.throughput = int(0.5 + 100.0 * dev.count / dev.max_count)

        value = None
        if pin[0] == 'B':
          signal = db.GetLatestBinaryDatum(dev.mac, pin)
          if signal:
            value = signal.data
            scalar = False
        else:
          signal = db.GetLatestSignal(dev.mac, pin)
          if signal:
            value = signal.value
            scalar = True

        sensor = db.GetSensorByPin(site.skey, dev.did, pin)
        if value is None or sensor is None:
          continue # no signal or no associated sensor

        if sensor.format:
          fmt = FMT_FUNCS[sensor.format]
        else:
          fmt = FMT_FUNCS['noop']
        signal.fvalue = fmt(sensor.transform(value))
        if tdelta:
          signal.date = signal.date + tdelta
        dev.data.append({'pin':pin, 'sensor':sensor, 'signal':signal})

      # determine status & uptime
      dev.uptime = None
      dev.sending = "black"
      var = db.GetVar(dev.skey, '/dev.' + db.MacDecode(dev.mac))
      if var:
        uptime = now - dev.updated
        dev.uptime = { 'days':uptime.days, 'hours':int(uptime.seconds/3600), 'minutes':int(uptime.seconds/60) % 60 }
        if now - var.updated < datetime.timedelta(seconds=2*dev.monitor_period):
          dev.sending = "green"
        else:
          dev.sending = "red"

    self.WriteTemplate('monitor.html', {'sk':sk, 'ak':ak, 'sn':site.name, 'devices':devices, 'df':df})

class AdminHandler(BaseHandler):
  """Handles admin requests."""
  def get(self, obj, action):
    sk = ToInteger(self.request.get('sk'))
    oe = self.user_email
    rr = IsTrue(self.request.get('rr'))
    ra = self.request.remote_addr
    ip = self.request.get('ip')
    ma = self.request.get('ma')
    me = self.request.get('me')
    nm = self.request.get('nm')
    if not ip:
      ip = ra

    if obj == 'site' and action == 'add':
      self.WriteTemplate('admin.html', { 'add':True, 'oe':oe, 'ra':ra, 'countries':COUNTRIES, 'vn':VERSION })
      return

    self.values = {}
  
    site, err = db.CheckSite(sk)

    if not err:
      admin = self.is_app_admin or self.user_email == site.user_email or db.GetUserRole(sk, self.user_email) == 'admin'
      if not admin:
        err = 'AccessDenied'

      else:
        sl = ''
        if site.latitude and site.longitude:
          sl = str(site.latitude)+','+str(site.longitude)
        self.values = {'sk':sk, 'oe':oe, 'sl':sl, 'rr':rr, 'ip':ip, 'ra':ra, 'ma':ma, 'me':me, 'nm':nm,
                       'countries':COUNTRIES, 'vn':VERSION, 'roles':ROLES, 'urls':ASP_URLS, 'admin':admin,
                       'sites':db.GetSitesByUser(self.user_email), 'devices':db.GetDevicesBySite(sk),
                       'pins':db.GetPinsBySite(sk)}

        if action == 'confirm':
          if site.skey == sk:
            site.confirmed = True
            site.enabled = True
            site.put()
            msg = 'You have confirmed this site'
          else:
            err = 'InvalidSiteKey'
            self.values.update({'unconfirmed':True})
        elif action == 'unsubscribe':
          site.enabled = False
          site.put()
          msg = 'You have disabled this site'

      if not err:
        reqs = db.GetRequests('config', ip)
        if site.tz_offset != 0:
          tdelta = datetime.timedelta(hours=site.tz_offset)
          for req in reqs:
            req.date = req.date + tdelta

        self.values.update({'sn':site.name, 'pr':site.premium, 'oe':oe, 'op':site.user_phone,
                            'cc':site.country, 'tz':site.tz_offset, 'en':site.enabled,
                            'np':site.notify_period, 'users':db.GetUsersBySite(sk), 'asps':db.GetAspsBySite(sk),
                            'pins':db.GetPinsBySite(sk), 'reqs':reqs})
    self.WriteTemplate('admin.html', self.values, err)
    
  def post(self, obj, action):
    sk = ToInteger(self.request.get('sk'))
    admin = self.is_app_admin or db.GetUserRole(sk, self.user_email) == 'admin'

    self.values = {'sk':sk, 'countries':COUNTRIES, 'vn':VERSION, 'roles':ROLES, 'urls':ASP_URLS, 'admin':admin, 'pins':{}}

    if not (obj == 'site' and action == 'add') and not admin:
      err = 'AccessDenied'
    else:
      if obj in ['site', 'user', 'asp', 'util']: 
        err = self.__getattribute__(obj)(action)
      else:
        err = 'InvalidObject'

    if err:
      self.WriteTemplate('admin.html', self.values, err)
    elif action != 'export':
      url = '/admin?sk=' + str(sk)
      ma = self.values.get('ma')
      if ma:
        url += '&ma=' + ma
      me = self.values.get('me')
      if me:
        url += '&me=' + str(me)
      nm = self.values.get('nm')
      if nm:
        url += '&nm=' + nm
      self.redirect(url)

  def site(self, action):
    """Handles site administration."""
    sk = ToInteger(self.request.get('sk'))
    sn = self.request.get('sn')
    rr = IsTrue(self.request.get('rr'))
    oe = self.user_email
    op = self.request.get('op')
    np = ToInteger(self.request.get('np'), CONST['notifyPeriod'])
    cc = self.request.get('cc')
    tz = ToFloat(self.request.get('tz'))
    sl = self.request.get('sl')
    en = IsTrue(self.request.get('en'))
    ma = self.request.get('ma')

    # local sites come enabled, confirmed and premium for ease of testing
    if self.request.host_url[:16] == "http://localhost" or self.request.host_url[:16] == "http://127.0.0.1":
      en = True
      cf = True
      pr = True
    else:
      pr = False
      cf = False

    self.values.update({ 'sn':sn, 'cc':cc, 'tz':tz, 'sl':sl, 'en':en })

    # location is optional, but if present it must be a valid latlng
    ll = (None,None)
    if sl:
      ll = IsLatLng(sl)
      if not ll:
        return 'InvalidLatLng'

    if action == 'add':
      if not sn:
        return 'SiteNameRequired'
      if db.GetSiteByName(oe, sn): 
        return 'ExistingSite'
      if rr and not IsSiteKey(sk):
        return 'InvalidSiteKey'

      created = False
      while not created:
        # generate a 12-digit random number, unless re-registering an old site
        if not rr:
          sk = random.randrange(10**11, 10**12)
        site, created = db.GetOrCreateSite(sk, name=sn, user_email=oe, user_phone=op, notify_period=np,
                                        country=cc, tz_offset=tz, latitude=ll[0], longitude=ll[1],
                                        enabled=en, confirmed=cf, premium=pr)

      logging.info("/admin/site/add: sk=%d, sn=%s" % (sk, sn))
      # create an admin user for the owner
      db.GetOrCreateUser(sk, oe, role='admin')
      self.values.update({ 'unconfirmed':True, 'sk':'' })
      if cf:
        return 'SiteConfirmationNotRequired'

      self.RenderAndSendEmail('registration_email.txt', 
                              {'sender':CONST['emailSender'], 'recipient':oe, 'subject':'NetReceiver registration', 'sk':sk, 'sn':sn})
      return 'SiteConfirmationRequired'

    elif action == 'update':
      if not sk:
        return 'SiteKeyRequired'
      else:
        site = db.GetSite(sk)
        if not site:
          return 'SiteNotFound'

      # NB: site key is immutable
      site.name = sn
      site.user_phone = op
      site.notify_period=np
      site.country = cc
      site.tz_offset = tz
      site.latitude = ll[0]
      site.longitude = ll[1]
      site.enabled = en
      site.put();
      logging.info("/admin/site/update: sk=%d, sn=%s" % (sk, sn))

    elif action == 'delete':
      dt = self.request.get('dt')
      if dt == 'data':
        db.DeleteSignals(sk, ma)
      elif dt == 'binary':
        db.DeleteBinaryData(sk, ma)
      elif dt == 'video':
        DeleteMtsVideos(sk, ma)
      elif dt == 'request':
        db.DeleteRequests(sk, ma)
      elif dt == 'site':
        db.DeleteSite(sk, True)
        return 'SiteDeleted'
      else:
        return 'InvalidDataType'
      logging.info("/admin/site/delete: sk=%d dt=%s" % (sk, dt))

    else:
      return 'InvalidAction'

    return None

  def user(self, action):
    """Handles user adminstration."""
    sk = ToInteger(self.request.get('sk'))
    ue = self.request.get('ue')
    ur = self.request.get('ur')

    self.values.update({'ue':ue, 'ur':ur})

    site, err = db.CheckSite(sk)
    if err:
      return err
    if not IsEmail(ue):
      return 'InvalidEmail'

    if action == 'delete':
      db.DeleteUser(sk, ue)
      logging.info("/admin/user/delete: sk=%d ue=%s" % (sk, ue))
      self.values.update({'ue':'', 'ur':''})
      return None

    if ur not in ROLES:
      return 'InvalidRole'

    user = db.GetUser(sk, ue)
    if action == 'add':
      if user:
        return 'ExistingUser'
      else:
        user, created = db.GetOrCreateUser(sk, ue, role=ur)
        logging.info("/admin/user/add: sk=%d ue=%s" % (sk, ue))
        self.values.update({ 'users':db.GetUsersBySite(sk) })
        site = db.GetSite(sk)
        subject = "You've been given %s access to NetReceiver site %s" % (ur, site.name)
        self.RenderAndSendEmail('user_email.txt', {'sender':CONST['emailSender'], 'recipient':ue, 'subject':subject, 'sk':sk, 'sn':site.name, 'role':ur})

    elif action == 'update':
      if not user:
        return 'UserNotFound'
      # NB: email is immutable
      user.role = ur
      user.put();
      logging.info("/admin/user/update: sk=%d ue=%s" % (sk, ue))

    else:
      return 'InvalidAction'

    self.values.update({'ue':'', 'ur':''})
    return None

  def asp(self, action):
    """Handles administration of application specific passwords."""
    sk = ToInteger(self.request.get('sk'))
    ak = self.request.get('ak')
    au = self.request.get('au')
    ad = self.request.get('ad')

    self.values.update({'ak':ak, 'au':au})

    site, err = db.CheckSite(sk)
    if err:
      return err

    if action == 'delete':
      if not ak or not ak.isalpha():
        return 'InvalidKey'
      db.DisableAsp(ak)
      logging.info("/admin/asp/delete: sk=%d ak=%s" % (sk, ak))
      self.values.update({'ak':'', 'au':''})
      return None

    elif action == 'add':
      if au not in ASP_URLS:
        return 'InvalidURL'

      created = False
      while not created:
        # generate a brand new unique 12-letter key containing no digits
        ak = ''.join(random.choice(string.letters) for _ in range(12))
        asp, created = db.GetOrCreateAsp(ak, skey=sk, url=au, description=ad)

      logging.info("/admin/asp/add: sk=%d ak=%s" % (sk, ak))
      self.values.update({ 'asps':db.GetAspsBySite(sk) })

    else:
      return 'InvalidAction'

    self.values.update({'ak':'', 'au':''})
    return None

  def util(self, action):
    """Handles admin utility actions."""
    sk = ToInteger(self.request.get('sk'))

    site, err = db.CheckSite(sk)
    if err:
      return err

    if action == 'find':
      ma = self.request.get('ma').upper()
      dev, err = db.CheckDevice(ma)
      site2 = None
      if not err:
        site2 = db.GetSite(dev.skey)
      if site2:
        nm = site2.name
      else:
        nm = '?'
      self.values.update({'ma':ma, 'nm':nm})
      return None

    elif action == 'move':
      ma = self.request.get('ma').upper()
      dev, err = db.CheckDevice(ma)
      if err:
        return err
      sk2 = ToInteger(self.request.get('sk2'))
      if not sk2:
        return 'SiteKeyRequired'
      site2 = db.GetSite(sk2)
      if not site2:
        return 'SiteNotFound'
      # move the device
      dev.skey = sk2
      dev.put()
      dev = db.GetDevice(ma, False)
      # move variables
      for var in db.GetVarsBySite(sk, dev.did):
        db.CreateVar(sk2, var.name, var.value)
        db.DeleteVar(sk, var.name)
      # move sensors
      for sen in db.GetSensorsBySite(sk):
        if sen.pin[:len(dev.did)] == dev.did:
          db.GetOrCreateSensor(sk2, sen.sid, pin=sen.pin, quantity=sen.quantity, func=sen.func, args=sen.args, units=sen.units, format=sen.format)
          db.DeleteSensor(sk, sen.sid)
      # move actuators
      for act in db.GetActuatorsBySite(sk):
        if act.pin[:len(dev.did)] == dev.did:
          db.GetOrCreateActuator(sk2, act.aid, var=act.var, pin=act.pin)
          db.DeleteActuator(sk, act.aid)
      return None

    elif action == 'encode':
      ma = self.request.get('ma').upper()
      me = self.request.get('me')
      if ma:
        if not IsMacAddress(ma):
          return 'InvalidMac'
        me = db.MacEncode(ma)
      elif me:
        if not IsInteger(me):
          return 'InvalidNumber'
        me = ToInteger(me)
        ma = db.MacDecode(me)
      self.values.update({'ma':ma, 'me':me})
      return None

    output = '{"site":\n  ' + json.dumps(site.to_dict(), default=Jsonify) + ","
    for kind in KINDS['settings']:
      output += '\n "' + kind + 's":[\n  ' + ",\n  ".join([json.dumps(obj.to_dict(), default=Jsonify) for obj in db.GetKindBySite(sk, kind)]) + '],'
    output += '\n}'
    self.response.headers['Access-Control-Allow-Origin'] = '*'
    self.response.headers['Content-Type'] = 'application/json'
    self.response.out.write(output)

    return None

class HelpHandler(BaseHandler):
  """Handles help requests."""
  def get(self, subpage):
    sk = ToInteger(self.request.get('sk'))
    if subpage:
      self.WriteTemplate('help/' + subpage + '.html', { 'sk':sk })
    else:
      self.WriteTemplate('help.html', { 'sk':sk })

###############################################################################
#
class ClockHandler(BaseHandler):
  """Clock handler: fires every minute to run crons."""
  def get(self, action):
    time_now = datetime.datetime.time(datetime.datetime.now())
    clock, created = db.GetOrCreateClock(time_now)
    if created:
      logging.info("clock created at time %02d:%02d" % (time_now.hour, time_now.minute))

    output = ''
    if action == 'update':
      updated = db.UpdateTimesOfDay()
      output += "%02d:%02d: updated %d crons; " % (clock.time.hour, clock.time.minute, updated)

    # run crons, if any
    minutes_now = (time_now.hour * 60) + time_now.minute
    if minutes_now >= clock.minutes:
      minutes_elapsed = minutes_now - clock.minutes
    else:
      minutes_elapsed = (minutes_now + CONST['minsInDay']) - clock.minutes # rolled over
 
    if minutes_elapsed == 0:
      output += "%02d:%02d: clock unchanged" % (clock.time.hour, clock.time.minute)

    elif minutes_elapsed > 5:
      # fast forward to now
      # NB: should never happen on App Engine if cron.yaml is correct, but can happen on dev_appserver.
      db.UpdateClock(time_now)
      output += "%02d:%02d: fast forwarding clock" % (clock.time.hour, clock.time.minute)

    else:
      rcrons = db.GetRepeatingCrons()
      cron_count = 0
      while minutes_elapsed > 0:
        clock.time, clock.minutes = IncrTime(clock.time)

        if clock.time.hour == 0 and clock.time.minute == 0:
          # we update times of day at midnight UTC
          updated = db.UpdateTimesOfDay()
          output += "%02d:%02d: updated %d crons; " % (clock.time.hour, clock.time.minute, updated)

        # first get crons for this specific time
        crons = db.GetCronsByTime(clock.time)
        for cron in crons:
          if cron.execute(Notify):
            cron_count += 1

        # next check repeating crons, if any
        for cron in rcrons:
          if clock.minutes % cron.minutes == 0:
            if cron.execute(Notify):
              cron_count += 1
        minutes_elapsed -= 1
      db.UpdateClock(clock.time)
      output += "%02d:%02d: ran %d crons" % (clock.time.hour, clock.time.minute, cron_count)

    self.response.out.write(output)    

class VarHandler(BaseHandler):
  """Handles requests for variable values.
NB: Variable changes are made via Settings."""
  def get(self, key, name):
    """Get a variable. Key must be a site key or an ASP key."""
    err = None
    if key.isalpha():
      key, err = db.CheckAsp(key, 'var')
    if not err:
      site, err = db.CheckSite(key)

    if not err:
      var = db.GetVar(site.skey, name)
      if not var:
        err = 'VariableNotFound'

    if err:
      output = json.dumps({'er':err})
    else:
      output = var.value

    self.response.headers['Access-Control-Allow-Origin'] = '*'
    self.response.headers['Content-Type'] = 'application/json'
    self.response.out.write(output)

class BinaryHandler(BaseHandler):
  """Common base class for serving binary data."""
  def check(self, key, name, dt, url):
    """Check prereqs for serving binary data. Key must be a site key or an ASP key. Name must be and extended ID."""
    err = None
    if key.isalpha():
      sk, err = db.CheckAsp(key, url)
    if not err:
      site, err = db.CheckSite(key)
    if err:
      return None, None, err
    
    if not IsExtendedPin(name, dt):
      return None, None, 'InvalidId'
    did,pin = name.split('.')
    dev = db.GetDeviceById(site.skey, did)
    if not dev:
      return None, None, 'DeviceNotFound'
    return dev, pin, None

  def data(self, dev, pin, duration=CONST['dataDuration']):
    """Return data for a given device and id. When a time period is
specified (through some combination of data start (ds), data finish
(df), data duration (dd) and timezone (tz)), data is concatenated for
the given period. When a timestamp (ts) is specified, a single record
(if any) is returned."""
    ds = self.request.get('ds')
    df = self.request.get('df')
    dd = ToInteger(self.request.get('dd'), duration)
    du = self.request.get('du')
    ts = ToInteger(self.request.get('ts'))
    tz = self.request.get('tz')

    # convert dd to minutes, unless already
    if du == 'hours':
      dd *= 60
    elif du == 'days':
      dd *= CONST['minsInDay']

    # when the HTTP header includes a 'Range', the offset is treated as a timestamp, not a byte offset
    rng = self.request.headers.get('Range')
    if rng:
      logging.debug("Range: %s" % rng)
      ss = rng.split('@')
      if len(ss) == 2:
        ts = ToInteger(ss[1])

    bd = []
    if ts:
      # get single record for given timestamp, if any
      b = db.GetBinaryDatum(dev.mac, pin, ts)
      if b:
        bd.append(b)
    else:
      # get data for given date/time range
      if tz:
        tz = ToFloat(tz)
      else:
        site = db.GetSite(dev.skey)
        tz = site.tz_offset
      start, finish, err = ToDateRange(ds, df, datetime.timedelta(minutes=dd), -tz)
      if err:
        return None, err
      bd = db.GetBinaryData(dev.mac, pin, start, finish)

    if len(bd) == 0:
      return None, 'NoData'
    return bd, None

class TextHandler(BinaryHandler):
  """Handles text serving requests."""
  def get(self, key, name):
    dev, pin, err = self.check(key, name, 'T', 'text')
    if not err:
      bd, err = self.data(dev, pin)

    if err:
      output = json.dumps({'er':err})
      self.response.headers['Content-Type'] = 'application/json'
      self.response.out.write(output)
      return

    output = ""
    fmt = None
    for b in bd:
      output += str(b.data)
      if not fmt:
        fmt = str(b.fmt)
    if not fmt:
      fmt = 'text/plain'
    self.response.headers['Content-Type'] = fmt
    self.response.out.write(output)

  def post(self, key, name):
    if not key:
      self.request.get('sk')
    if not name:
      name = self.request.get('di')
    return self.get(key, name)

class AudioHandler(BinaryHandler):
  """Handles audio serving requests."""
  def get(self, key, name):
    """Handles various audio requests. 1) When data ouput (do) is not
specified and a time period is specified, audio is concatenated for the
given period and returned in WAV format. 2) When data output is 'm3u',
an M3U playlist is returned listing individual URLs for each audio
segment. 3) With a timestamp (ts), we return an individual audio
segment in WAV format."""

    dev, pin, err = self.check(key, name, 'B', 'audio')
    if not err:
      bd, err = self.data(dev, pin, CONST['audioDuration'])

    do = self.request.get('do')
    if not err and do == '' and len(bd) * CONST['megaByte'] > CONST['maxHttpResponse']:
      err = 'TooMuchData'

    if err:
      output = json.dumps({'er':err})
      self.response.headers['Content-Type'] = 'application/json'
      self.response.out.write(output)
      return

    if do[:3] == 'm3u':
      # return a playlist in M3U format for HTTP live streaming (HLS)
      url = self.request.host_url + '/audio/' + key + '/' + name
      output = "#EXTM3U\n"
      if do == 'm3u.v4':
        # NB: experimental support for byte ranges.
        # We use the timestamp for the byte range "offset".
        output += "#EXT-X-VERSION:4\n"
        output += "#EXT-X-TARGETDURATION:5\n"
        output += "#EXT-X-MEDIA-SEQUENCE:0\n"
        for b in bd:
          ts = b.timestamp
          output += "#EXTINF:5.000, " + str(ts) + "\n"
          output += "#EXT-X-BYTERANGE: 882000@" + str(ts) + "\n"
          output += url + "\n"
      else:
        for b in bd:
          ts = b.timestamp
          output += "#EXTINF:5, " + str(ts) + "\n" + url + '?ts=' + str(ts) + "\n"
      output += "#EXT-X-ENDLIST\n"
      self.response.headers['Content-Type'] = 'audio/mpegurl'
      self.response.out.write(output)
      return

    # else return audio in WAV format
    # ToDo: remux audio for HLS (https://tools.ietf.org/html/draft-pantos-http-live-streaming-23)
    sz = len(bd[0].data)
    pcmdata = bytearray(len(bd) * sz)
    for ii in range(len(bd)):
      pcmdata[ii*sz:(ii+1)*sz] = bd[ii].data[0:sz]

    fmt = Dictify(bd[0].fmt, ';', '=')
    codec = fmt.get('codec')
    if codec != 'pcm':
      logging.warn("Treating binary data as PCM data")
    rate = ToInteger(fmt.get('rate'), 44100)
    channels = ToInteger(fmt.get('channels'), 2)
    bits = ToInteger(fmt.get('bits'), 16)
    buf = StringIO.StringIO()
    wav = wave.open(buf, 'wb')
    wav.setparams((channels, bits/8, rate, 0, 'NONE', 'NONE'))
    wav.writeframes(pcmdata)
    self.response.headers['Content-Type'] = 'audio/wav'
    self.response.headers['Access-Control-Allow-Origin'] = '*'
    self.response.out.write(buf.getvalue())
    wav.close()
    buf.close()

class DataHandler(BaseHandler):
  def get(self, key):
    """Handles data requests."""
    output, err = self.scalar(key)
    if err:
      output = {'er':err}
    self.response.headers['Access-Control-Allow-Origin'] = '*'
    self.response.headers['Content-Type'] = 'application/json'
    self.response.out.write(output)

  def scalar(self, key):
    """Returns various slices of scalar data in various formats."""
    ma = self.request.get('ma').upper()
    pn = self.request.get('pn')
    do = self.request.get('do')
    ds = self.request.get('ds')
    df = self.request.get('df')
    du = self.request.get('du')
    dd = ToInteger(self.request.get('dd'), CONST['dataDuration'])
    tz = self.request.get('tz')

    # convert dd to minutes, unless already
    if du == 'hours':
      dd *= 60
    elif du == 'days':
      dd *= CONST['minsInDay']

    err = None
    if key.isalpha():
      key, err = db.CheckAsp(key, 'data')
    if not err:
      site, err = db.CheckSite(key)
    if not err:
      dev, err = db.CheckMacPin(ma, pn)
    if err:
      return None, err

    if not do:
      do = 'raw'
    if do not in ('raw', 'json', 'gviz'):
      return None, 'InvalidFormat'

    start, finish, err = ToDateRange(ds, df, datetime.timedelta(minutes=dd), -site.tz_offset)
    if err:
      return None, err

    # query signal/scalar data
    signals = db.GetSignals(dev.mac, pn, start, finish)

    if tz:
      tz = ToFloat(tz)
    else:
      tz = site.tz_offset

    if do == 'raw':
      output = {'ma':ma, 'pn':pn, 'tz':tz, 'rd':[{'d':s.date.isoformat(), 'v':s.value } for s in signals]}
      return output, None

    # apply sensor, if any
    sensor = db.GetSensorByPin(site.skey, dev.did, pn)
    if not sensor:
      return None, 'SensorNotFound'

    if sensor.format:
      fmt = FMT_FUNCS[sensor.format]
    else:
      fmt = FMT_FUNCS['noop']

    data = [(s.date, fmt(sensor.transform(s.value))) for s in signals]

    if do == 'json':
      output = {'ma':ma, 'pn':pn, 'tz':tz, 'sd':[{'d':datum[0].isoformat(), 'v':datum[1]} for datum in data]}

    elif do == 'gviz':
      # We send UTC dates and times and let the client convert to their timezone.
      # See https://developers.google.com/chart/interactive/docs/datesandtimes.
      req_id = 0
      tqx = self.request.get('tqx')
      if tqx:
        props = Dictify(tqx, ';', ':')
        req_id = props.get('reqId', 0)
      if dd <= CONST['minsInDay']:
        desc = [('t', 'timeofday', 'Time'), ('v', 'number', sensor.sid)]
      else:
        desc = [('d', 'datetime', 'Date'), ('v', 'number', sensor.sid)]

      dt = gviz_api.DataTable(desc)
      dt.LoadData(data)
      output = dt.ToJSonResponse(req_id=req_id)

    return output, None

class ChartHandler(BaseHandler):
  """Handles chart requests."""
  def get(self, key):
    ma = self.request.get('ma').upper()
    pn = self.request.get('pn')
    ds = self.request.get('ds')
    df = self.request.get('df')
    dd = ToInteger(self.request.get('dd'), CONST['dataDuration'])
    tz = self.request.get('tz')
    si = ''

    err = None
    if key.isalpha():
      sk, err = db.CheckAsp(key, 'data')
    if not err:
      site, err = db.CheckSite(key)
    if not err:
      dev, err = db.CheckMacPin(ma, pn)
    if not err:
      sensor = db.GetSensorByPin(dev.skey, dev.did, pn)
      if not sensor:
        err = 'SensorNotFound'
    if not err:
      si = sensor.sid
      if tz:
        tz = ToFloat(tz)
      else:
        tz = site.tz_offset

    self.values = { 'key':key, 'tz':tz, 'si':si, 'ma':ma, 'pn':pn, 'ds':ds, 'df':df, 'dd':dd }
    self.WriteTemplate('chart.html', self.values, err)

###############################################################################
# NetSender request handlers
# NetSender requests typically require a device key which matches the device.
# For backwards compatibility though, a missing device key is treated as a device key of zero

class ConfigHandler(BaseHandler):
  """Handles config requests."""

  def get(self):
    return self.post()
  
  def post(self):
    ma = self.request.get('ma').upper()
    dk = ToInteger(self.request.get('dk'))
    ut = ToInteger(self.request.get('ut'))
    la = self.request.get('la') # device's LAN IP address
    ra = self.request.remote_addr # device's WAN IP address

    err = None
    if not IsMacAddress(ma):
      err = 'InvalidMac'
    else:
      dev = db.GetDevice(ma)
      if dev:
        if dev.status == DEV_STATUS['ok']:
          if dk != dev.dkey:
            logging.info("/config from device %s with invalid device key %d" % (ma, dk))
            err = 'InvalidDeviceKey'
          if not err and not db.GetRequest('config', ma, la, ra, 'OK'):
            db.CreateRequest('config', ma, la, ra, 'OK')
          # never reveal a device key to a device which is already configured, unless to reset it to zero
          if dk != 0:
            dk = None
        else:
          dk = dev.dkey
          dev.status = DEV_STATUS['ok']
          dev.updated = datetime.datetime.now()
          dev.put()
          db.CreateRequest('config', ma, la, ra, 'OK')
        db.UpdateOrCreateVar(dev.skey, '/dev.' + db.MacDecode(dev.mac), str(ut))
      else:
        logging.info("/config from unknown device %s" % ma)
        err = 'DeviceNotFound'

    if err:
      values = {'er':err}
      if not db.GetRequest('config', ma, la, ra, err):
        db.CreateRequest('config', ma, la, ra, err)

    else:
      values = {'ma':ma, 'wi':dev.wifi, 'ip':dev.inputs, 'op':dev.outputs, 'mp':dev.monitor_period, 'ap':dev.act_period, 'tg':dev.tag, 'vs':db.GetVarSum(dev.skey, dev.did)}
      if dk is not None:
        values.update({'dk': dk})

    output = json.dumps(values);
    self.response.headers['Access-Control-Allow-Origin'] = '*'
    self.response.headers['Content-Type'] = 'application/json'
    self.response.out.write(output)

class PollHandler(BaseHandler):
  """Handle poll requests from devices."""
  def get(self):
    self.post()

  def post(self):
    ma = self.request.get('ma').upper()
    dk = ToInteger(self.request.get('dk'))
    vn = ToInteger(self.request.get('vn'))

    dev, err = db.CheckDevice(ma, dk)
    if not err:
      response = { 'ma':ma }
      er = None
      trigger_count = 0
      body_offset = 0
      vs = True
      for pin in Each(dev.inputs):
        value = self.request.get(pin)
        if value:
          try:
            value = int(value)
            response[pin] = value
          except:
            er = 'InvalidValue'
            break

          if pin[0] in ['A', 'D', 'X']:
            self.scalar(ma, pin, value)

            # scalar signals can fire triggers
            sensor = db.GetSensorByPin(dev.skey, dev.did, pin)
            if not sensor:
              continue
            for trig in db.GetTriggersBySensor(sensor.skey, sensor.sid):
              if not trig.preconds():
                continue
              func = OP_FUNCS[trig.op]
              if func(sensor.transform(value), trig.cmp):
                if trig.execute(Notify):
                  trigger_count += 1

          elif pin[0] in ['T', 'B']:
            er = self.binary(ma, pin, value, body_offset)
            if er:
              break
            body_offset += value

          else:
            # video is now handled by VideoCaptureHandler
            logging.warn("Deprecated: %s attempting to POST video using /poll" % ma)
            continue

      if vn >= 138:
        # include output pin values in response
        for pin in Each(dev.outputs):
          act = db.GetActuatorByPin(dev.skey, dev.did, pin)
          if act:
            var = db.GetVar(dev.skey, act.var)
            if var:
              response[pin] = ToIntegerValue(var.value)

      if er:
        response['er'] = er
      if trigger_count > 0:
        response['tc'] = trigger_count
      if dev.status != DEV_STATUS['ok']:
        response['rc'] = dev.status
      if vs:
        response['vs'] = db.GetVarSum(dev.skey, dev.did)
        db.UpdateOrCreateVar(dev.skey, '/dev.' + ma)

    if err:
      output = json.dumps({'er':err})
    else:
      output = json.dumps(response)

    self.response.headers['Access-Control-Allow-Origin'] = '*'
    self.response.headers['Content-Type'] = 'application/json'
    self.response.out.write(output)

  def scalar(self, ma, pin, value):
    """Scalar signals encode (pin) data as query params, e.g., A0=100&D2=1."""
    db.CreateSignal(ma, pin, int(value))
    return True

  def binary(self, ma, pin, size, offset):
    """For binary signals, the query param indicates the size of the data
    and the data itself is in the POST body. We store the MIME type as the fmt."""

    body = self.request.body
    fmt = self.request.headers.get('Content-Type')
    bd = self.request.get('bd')
    bf = self.request.get('bf')
    if bd:
      body = str(bd)
    if bf:
      fmt = bf
    if size <= 0 or len(body) < offset + size or size > CONST['maxBlob']:
      logging.debug("Invalid binary data, size=%d, len(body)=%d" % (size, len(body)))
      return 'InvalidPayloadSize'
    if len(body) > 0:
      db.CreateBinaryData(ma, pin, fmt, body[offset:offset+size], ToTimestamp(datetime.datetime.now(utctz())))
    return None

class ActHandler(BaseHandler):
  """Handles device actuator requests."""
  def get(self):
    ma = self.request.get('ma').upper()
    dk = ToInteger(self.request.get('dk'))

    dev, err = db.CheckDevice(ma, dk)
    if not err:
      response = { 'ma':ma }
      if dev.status != DEV_STATUS['ok']:
        response['rc'] = dev.status
      else:
        response['vs'] = db.GetVarSum(dev.skey, dev.did)
      for pin in Each(dev.outputs):
        act = db.GetActuatorByPin(dev.skey, dev.did, pin)
        if act:
          var = db.GetVar(dev.skey, act.var)
          if var:
            response[pin] = ToIntegerValue(var.value)
      db.UpdateOrCreateVar(dev.skey, '/dev.' + ma)

    if err:
      output = json.dumps({'er':err})
    else:
      output = json.dumps(response)

    self.response.headers['Access-Control-Allow-Origin'] = '*'
    self.response.headers['Content-Type'] = 'application/json'
    self.response.out.write(output)

class VarsHandler(BaseHandler):
  """Returns vars for a device with the current var sum.
Since client version 136 we also include the varsum as a string.
Device-initiated mode changes are also handled here."""
  def get(self):
    ma = self.request.get('ma').upper()
    dk = ToInteger(self.request.get('dk'))
    vn = ToInteger(self.request.get('vn'))
    md = self.request.get('md')

    dev, err = db.CheckDevice(ma, dk)
    if err:
      output = json.dumps({'er':err})
    else:
      if md:
        db.UpdateOrCreateVar(dev.skey, dev.did+'.mode', md)
        logging.debug("Device %s updated mode=%s" % (dev.did, md))

      response = {'id':dev.did}
      if vn >= 136:
        response['vs'] = str(db.GetVarSum(dev.skey, dev.did))
      for var in db.GetVarsBySite(dev.skey, dev.did):
        response[var.name] = var.value
      output = json.dumps(response)

    self.response.headers['Access-Control-Allow-Origin'] = '*'
    self.response.headers['Content-Type'] = 'application/json'
    self.response.out.write(output)


class ApiHandler(BaseHandler):
  """API handler handles API requests."""
  def get(self, action):
    output = ''
    if action == 'migrate':
      count = 0
      # do something
      output = '{"migrated":' + str(count) + '}'

    self.response.headers['Access-Control-Allow-Origin'] = '*'
    self.response.headers['Content-Type'] = 'application/json'
    self.response.out.write(output)

###############################################################################
# The app

app = webapp2.WSGIApplication(
  [
   # requests that require being logged in
   ('/', MainHandler),
   ('/sites', SitesHandler),
   ('/settings/?([^/]*)/?(.*)', SettingsHandler),
   ('/monitor/?(.*)', MonitorHandler),
   ('/admin/?([^/]*)/?(.*)', AdminHandler),
   ('/help/?([^/]*)', HelpHandler),
   # requests that require a site key or ASP
   ('/var/([^/]*)/(.*)', VarHandler),
   ('/video/([^/]*)/(.*)', VideoHandler),
   ('/audio/([^/]*)/(.*)', AudioHandler),
   ('/text/([^/]*)/(.*)', TextHandler),
   ('/data/([^?]*)', DataHandler),
   ('/chart/([^?]*)', ChartHandler),
   # NetSender requests that require a device key
   ('/config', ConfigHandler),
   ('/poll', PollHandler),
   ('/recv', RecvVideoHandler),
   ('/act', ActHandler),
   ('/vars', VarsHandler),
   # other requests
   ('/api/?(.*)', ApiHandler),
   ('/clock/?(.*)', ClockHandler),
  ],
  debug=True)

def main():
    app.run()

if __name__ == '__main__':
    main()
